-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2021 at 03:28 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ngo`
--

-- --------------------------------------------------------

--
-- Table structure for table `calendars`
--

CREATE TABLE `calendars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_type` int(11) NOT NULL DEFAULT 1,
  `event_type` int(11) NOT NULL DEFAULT 1,
  `event_name` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(4) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 1,
  `bgcolor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#d8fed1',
  `brdcolor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#FF0000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `calendars`
--

INSERT INTO `calendars` (`id`, `user_id`, `title`, `calendar_type`, `event_type`, `event_name`, `description`, `staff`, `parent`, `is_public`, `status`, `start_date`, `end_date`, `priority`, `bgcolor`, `brdcolor`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'hi', 1, 2, 8, 'tet', '6,3', '2', 1, 1, '2021-06-02T07:55:00', '2021-07-02T15:50:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-25 09:44:34', '2021-06-25 09:44:34'),
(3, 1, 'testmeeting', 1, 1, 4, 'asasasas', NULL, NULL, 1, 1, '2021-06-30T12:00:00', '2021-06-30T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 09:22:57', '2021-06-30 09:22:57'),
(4, 1, 'asa', 1, 1, 4, 'sas', '6,3', NULL, 1, 1, '2021-06-30T12:00:00', '2021-06-30T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 09:32:08', '2021-06-30 09:32:08'),
(5, 1, 'meetings', 1, 1, 4, 'sasas', '6', NULL, 1, 1, '2021-07-01T12:00:00', '2021-07-04T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 23:18:53', '2021-06-30 23:18:53'),
(6, 1, 'meet', 1, 1, 1, 'as da as das', '6', '9,2', 1, 1, '2021-07-01T12:00:00', '2021-07-06T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 23:21:32', '2021-06-30 23:21:32'),
(7, 3, 'asasa', 1, 1, 1, 'aSAS', '6', '10', 1, 1, '2021-07-02T12:00:00', '2021-07-02T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-07-02 09:14:59', '2021-07-02 09:14:59'),
(8, 3, 'asasa', 1, 1, 1, 'aSAS', '6', '10', 1, 1, '2021-07-02T12:00:00', '2021-07-02T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-07-02 09:19:16', '2021-07-02 09:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `adresss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`id`, `name`, `email`, `gender`, `status`, `adresss`, `created_at`, `updated_at`) VALUES
(1, 'David', 'david@mailinator.com', '1', '1', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(2, 'Rosa', 'rosa@mailinator.com', '2', '1', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(3, 'Rosy', 'rosy@gmail.com', '2', '1', NULL, '2021-06-25 02:39:07', '2021-06-25 02:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `child_relations`
--

CREATE TABLE `child_relations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rel_from` int(11) NOT NULL,
  `rel_to` int(11) NOT NULL,
  `rel_type` enum('staff','parent') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'parent',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `child_relations`
--

INSERT INTO `child_relations` (`id`, `rel_from`, `rel_to`, `rel_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'parent', '1', '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(2, 2, 2, 'parent', '1', '2021-06-25 02:01:39', '2021-06-25 02:01:39'),
(3, 3, 2, 'staff', '1', '2021-06-25 02:02:52', '2021-06-25 02:02:52'),
(4, 3, 1, 'staff', '1', '2021-06-25 02:34:31', '2021-06-25 02:37:40'),
(5, 2, 3, 'parent', '1', '2021-06-25 02:39:07', '2021-06-25 02:39:07'),
(6, 6, 2, 'staff', '0', '2021-06-28 06:51:46', '2021-06-29 07:29:09'),
(7, 6, 1, 'staff', '1', '2021-06-28 06:52:07', '2021-06-28 06:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `code` varchar(155) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id`, `name`, `code`, `updated_at`, `created_at`) VALUES
(1, 'Disability', 'disability', '2021-06-24 14:06:08', '2021-06-21 21:42:52'),
(2, 'Education', 'education', '2021-06-24 14:06:19', '2021-06-21 21:45:01'),
(3, 'Health/Nutrition', 'health-nutrition', '2021-06-24 14:06:47', '2021-06-21 21:45:01'),
(4, 'Family services', 'family-services', '2021-06-24 14:07:01', '2021-06-21 21:45:52'),
(5, 'Mental Health', 'mental-health', '2021-06-24 14:07:18', '2021-06-21 21:45:52'),
(6, 'Internal Report', 'internal-report', '2021-06-24 14:08:10', '2021-06-24 14:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `component_resources`
--

CREATE TABLE `component_resources` (
  `id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `document_type` bigint(20) NOT NULL DEFAULT 1,
  `topic` mediumtext DEFAULT NULL,
  `additional_info` mediumtext DEFAULT NULL,
  `link` varchar(155) DEFAULT NULL,
  `file` text NOT NULL,
  `date` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Staff',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Disability', 1, NULL, NULL),
(2, 'Teaching', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` bigint(20) NOT NULL,
  `type` varchar(155) NOT NULL,
  `designation_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `type`, `designation_id`, `created_at`, `updated_at`) VALUES
(1, 'disability-resource', 1, '2021-06-29 05:04:58', '2021-06-29 05:04:58'),
(2, 'education-docs', 2, '2021-06-29 05:04:58', '2021-06-29 05:04:58'),
(3, 'education-result-rediness', 2, '2021-06-30 18:30:29', '2021-06-30 18:30:29'),
(4, 'health-ehs', 3, '2021-07-01 00:48:46', '2021-07-01 00:48:46'),
(5, 'health-hs', 3, '2021-07-01 00:48:46', '2021-07-01 00:48:46'),
(6, 'health-nutrition', 3, '2021-07-01 01:31:34', '2021-07-01 01:31:34'),
(7, 'mental-resource', 5, '2021-07-01 18:58:09', '2021-07-01 18:58:09'),
(8, 'education-resource', 2, '2021-07-01 18:58:09', '2021-07-01 18:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `name`, `subject`, `email_type`, `email_body`, `created_at`, `updated_at`) VALUES
(1, 'newsletter', 'NEWSLETTER FROM LE JARDIN', 'newsletter', '<p>Hi TO_NAME;</p>\r\n\r\n<p>POSTED_BY has sent a newsletter.</p>\r\n<div class=\"card-body\">\r\n<div class=\"card-header\">TITLE</div>\r\n<div class=\"card-body\">DESCRIPTION</div>\r\n</div>\r\n\r\n<p>Thanks</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_types`
--

CREATE TABLE `email_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `constants` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_types`
--

INSERT INTO `email_types` (`id`, `type`, `constants`, `created_at`, `updated_at`) VALUES
(1, 'newsletter', 'TO_EMAIL,TO_NAME,DESCRIPTION,TITLE,POSTED_BY', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_types`
--

CREATE TABLE `event_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_category` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_types`
--

INSERT INTO `event_types` (`id`, `name`, `event_category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Teacher Planning Day', '0', '1', NULL, NULL),
(2, 'Holiday', '0', '1', NULL, NULL),
(3, 'Emergency Closed', '0', '1', NULL, NULL),
(4, 'Parents Meeting', '0', '1', NULL, NULL),
(5, 'Mobile Bus', '1', '1', NULL, NULL),
(6, 'Home Visit', '1', '1', NULL, NULL),
(7, 'MH Intern Counselling services', '1', '1', NULL, NULL),
(8, 'Disabilities', '1', '1', NULL, NULL),
(9, 'Home Visit', '1', '1', NULL, NULL),
(10, 'Conference', '1', '1', NULL, NULL),
(11, 'Techhelp', '0', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

CREATE TABLE `feeds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_by` int(11) NOT NULL,
  `privacy` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `parent` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feeds`
--

INSERT INTO `feeds` (`id`, `title`, `desc`, `link`, `posted_by`, `privacy`, `parent`, `staff`, `created_at`, `updated_at`) VALUES
(2, 'test', 'test  Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans.', '580769187.jpg', 1, '0', '2', '6', '2021-06-30 08:13:39', '2021-06-30 08:13:39'),
(3, 'rrr', 'ewrwer', NULL, 1, '0', '', NULL, '2021-07-01 03:32:34', '2021-07-01 03:32:34'),
(4, 'sasa', 'sasas', '270336916.jpg', 1, '0', NULL, '3,6', '2021-07-01 04:46:46', '2021-07-01 04:46:46'),
(5, 'aAAsdsds', 'dsdsdsdsd', '1499758866.png', 1, '0', NULL, '3,6', '2021-07-01 04:50:32', '2021-07-01 04:50:32'),
(6, 'ugugug', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.sdasd', '17213157.jpg', 1, '1', '', '', '2021-07-01 08:28:37', '2021-07-01 08:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` decimal(10,7) DEFAULT NULL,
  `long` float(10,7) DEFAULT NULL,
  `place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `lat`, `long`, `place`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Center A', '26.8398509', 75.8009338, 'Jawahar Circle Garden, Jawahar Circle, Malviya Nagar, Jaipur, Rajasthan, India', '1', NULL, '2021-06-29 07:59:52'),
(2, 'Center B', '22.7195687', 75.8577271, 'Indorė, Madhya Pradesh, India', '1', NULL, '2021-06-29 08:00:05'),
(4, 'tetw', '-33.8892658', 151.1242371, 'Woollahra NSW, Australia', '1', '2021-06-29 07:59:33', '2021-06-29 20:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2019_08_19_000000_create_failed_jobs_table', 1),
(12, '2021_02_20_090712_create_designations_table', 1),
(13, '2021_02_20_104440_create_user_details_table', 1),
(14, '2021_02_20_104900_add_columns_to_users_table', 1),
(15, '2021_02_27_115642_create_calendars_table', 1),
(16, '2021_06_23_130354_create_children_table', 2),
(18, '2021_06_24_111930_create_child_relations_table', 3),
(19, '2021_06_25_111424_create_event_types_table', 4),
(21, '2021_06_25_121405_create_locations_table', 5),
(22, '2021_06_30_100229_create_feeds_table', 6),
(23, '2021_07_02_075332_create_newsletters_table', 7),
(24, '2021_07_02_115443_create_email_types_table', 8),
(25, '2021_07_02_115455_create_email_templates_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `title`, `desc`, `link`, `staff`, `parent`, `posted_by`, `created_at`, `updated_at`) VALUES
(1, 'developer', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', NULL, '3,6', '2,10', 1, '2021-07-02 04:52:07', '2021-07-02 04:52:07'),
(2, 'newnewletter', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '156205368.docx', '3,6', '2,10', 1, '2021-07-02 04:55:46', '2021-07-02 04:55:46'),
(3, 'newnewletter', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1733453577.docx', '3,6', '2,10', 1, '2021-07-02 04:57:39', '2021-07-02 04:57:39'),
(4, 'newnewletter', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1810174026.docx', '3,6', '2,10', 1, '2021-07-02 05:02:34', '2021-07-02 05:02:35'),
(5, 'testtt', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.sdasd', '375665022.docx', '6', '2', 1, '2021-07-02 05:08:17', '2021-07-02 05:23:44'),
(6, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '615300874.docx', '6', '10', 1, '2021-07-02 06:46:36', '2021-07-02 06:46:36'),
(7, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1366285548.docx', '6', '10', 1, '2021-07-02 06:47:17', '2021-07-02 06:47:17'),
(8, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1830345099.docx', '6', '10', 1, '2021-07-02 06:47:48', '2021-07-02 06:47:48'),
(9, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '2062321121.docx', '6', '10', 1, '2021-07-02 06:48:15', '2021-07-02 06:48:15'),
(10, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '599650134.docx', '6', '10', 1, '2021-07-02 06:48:26', '2021-07-02 06:48:26'),
(11, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1417012095.jpg', '', '', 1, '2021-07-02 06:49:08', '2021-07-02 06:51:35'),
(12, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '826083631.docx', '6', '10', 1, '2021-07-02 06:50:06', '2021-07-02 06:50:06'),
(13, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '684750219.docx', '6', '10', 1, '2021-07-02 06:50:59', '2021-07-02 06:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `designation` tinyint(1) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `user_role` tinyint(4) NOT NULL DEFAULT 1,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `designation`, `phone`, `is_admin`, `user_role`, `profile`, `profile_photo_path`, `deleted_at`) VALUES
(1, 'Admin', 'admin@lejardin.com', NULL, '$2y$10$pmNXO9EDgdmzXur4ZZTdPeL3bzoYKG7Dogurit/mSVnYCgj3l/HJq', NULL, '2021-06-25 01:55:25', '2021-06-25 01:59:05', NULL, '(184) 431-4111', 1, 1, '2127091500.jpg', NULL, NULL),
(2, 'Richel Watt', 'richel@mailinator.com', NULL, '$2y$10$lHjkR0Qw/Vg9YaM/.c8JQugkaQ.4xWWBsacIlnQnFiFU/iFZ0u8k.', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38', NULL, '(185) 542-5845', 0, 3, '543097252.jpg', NULL, NULL),
(3, 'Brosley', 'Brosley@mailinator.com', NULL, '$2y$10$IeL9T0x4f875xbI3EsgT7u0/mkJ7FeVebymDLUSQHnyQF7iWuZciW', NULL, '2021-06-25 02:02:51', '2021-06-28 05:20:40', 2, '(186) 622-7889', 0, 2, '476662887.jpg', NULL, NULL),
(4, 'test', 'test@mailinator.com', NULL, '$2y$10$m8Zh1sXjBRTWnej8GrHHSunCrbRzSAxw6ydknJQRpoMwvHzl0fYHi', NULL, '2021-06-25 02:03:41', '2021-06-25 02:07:35', NULL, '(666) 666-6666', 0, 1, '1969020905.png', NULL, '2021-06-25 02:07:35'),
(5, 'Maria', 'maria@gmail.com', NULL, '$2y$10$lalk4ajb1wuNXfc8nW6VJeGpQ.Xt.NzOWGFb7W2SLcHyRBFlC26A2', NULL, '2021-06-28 06:43:54', '2021-06-28 06:49:05', NULL, '(999) 999-9999', 0, 1, '550398394.jpg', NULL, NULL),
(6, 'Pujara', 'pujara@gmail.com', NULL, '$2y$10$KRh66Swzww9DG/3V2uhQK.oKuuXvnNSuI.NgWBDkY2kxzdc2lOQdy', NULL, '2021-06-28 06:51:46', '2021-06-29 07:29:08', 1, '(437) 879-7097', 0, 2, '2123340209.jpg', NULL, NULL),
(7, 'tst', 'testparent@lejardin.com', NULL, '$2y$10$MFmae2E2t6zAjA34GCsnfO0/GbBX6xAWyx/SSumse/sg7TsAztLdO', NULL, '2021-06-30 06:03:03', '2021-06-30 06:39:40', NULL, '(999) 999-9999', 0, 3, '1967764789.png', NULL, NULL),
(8, 'tst', 'testparent2@lejardin.com', NULL, '$2y$10$mVdqIkmZqtxT94h2Co8xJ.YMlbY.Pa7IeBcU8bV/voW80A7UT2FiW', NULL, '2021-06-30 06:04:07', '2021-06-30 06:04:08', NULL, '(999) 999-9999', 0, 3, '408814525.PNG', NULL, NULL),
(9, 'tstre', 'testparent1@lejardin.com', NULL, '$2y$10$AJj2pDVBiPnh0djdxQJaXOJzMf2TohnFCU49MooiTtD72i2k8qwNu', NULL, '2021-06-30 06:05:01', '2021-06-30 06:05:01', NULL, '(999) 999-9999', 0, 3, '1070897148.jpg', NULL, NULL),
(10, 'tstre', 'testparent4@lejardin.com', NULL, '$2y$10$gZj2bNJUSD7qXohWrpgIh.f/DyxSbLyOorW9kxNUEnHhBIU9doNKO', NULL, '2021-06-30 06:09:21', '2021-06-30 06:09:22', NULL, '(999) 999-9999', 0, 3, '88885210.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `phone_home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `address1` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `git_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outlook_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slack_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `my_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skill` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `status`, `phone_home`, `city`, `state`, `country`, `zip`, `address1`, `address2`, `git_acc`, `twitter_acc`, `linkedin_acc`, `facebook_acc`, `outlook_acc`, `instagram_acc`, `slack_acc`, `my_acc`, `other_acc`, `aadhar_no`, `website`, `pan_no`, `work`, `education`, `skill`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '(184) 453-5286', NULL, NULL, NULL, NULL, '9820 Leatherwood Dr.\r\nAbsecon, NJ 08205', '7842 West Saxton Ave.\r\nWest Haven, CT 06516', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 0, '(184) 480-8104', NULL, NULL, NULL, NULL, '37 College Ave.\r\nMinot, ND 58701', '37 College Ave.\r\nMinot, ND 58701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, '(186) 644-7840', NULL, NULL, NULL, NULL, '68 E. Constitution Dr.\r\nFairport, NY 14450', '68 E. Constitution Dr.\r\nFairport, NY 14450', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, '(777) 777-7777', NULL, NULL, NULL, NULL, 'dfdfd', 'fsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, '(087) 912-6435', NULL, NULL, NULL, NULL, 'Tst t e', 'permanent lives here', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 1, '(122) 222-2211', NULL, NULL, NULL, NULL, 'saS jk', 'ASa ok', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf', 'af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf', 'af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 9, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf', 'af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf ffsafas', 'afasf  safsa a sa a a f dsdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calendars`
--
ALTER TABLE `calendars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `children_email_unique` (`email`);

--
-- Indexes for table `child_relations`
--
ALTER TABLE `child_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_resources`
--
ALTER TABLE `component_resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_types`
--
ALTER TABLE `email_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_types`
--
ALTER TABLE `event_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `feeds`
--
ALTER TABLE `feeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calendars`
--
ALTER TABLE `calendars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `child_relations`
--
ALTER TABLE `child_relations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `component_resources`
--
ALTER TABLE `component_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email_types`
--
ALTER TABLE `email_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event_types`
--
ALTER TABLE `event_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feeds`
--
ALTER TABLE `feeds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
