-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2021 at 11:29 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ngo2`
--

-- --------------------------------------------------------

--
-- Table structure for table `calendars`
--

CREATE TABLE `calendars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_type` int(11) NOT NULL DEFAULT 1,
  `event_type` int(11) NOT NULL DEFAULT 1,
  `event_name` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(4) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 1,
  `bgcolor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#d8fed1',
  `brdcolor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#FF0000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `calendars`
--

INSERT INTO `calendars` (`id`, `user_id`, `title`, `calendar_type`, `event_type`, `event_name`, `description`, `is_public`, `status`, `start_date`, `end_date`, `priority`, `bgcolor`, `brdcolor`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'rrerr', 1, 1, 4, 're', 1, 1, '2021-06-25T12:00:00', '2021-06-25T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-25 06:18:14', '2021-06-25 06:18:14');

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `adresss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`id`, `name`, `email`, `gender`, `status`, `adresss`, `created_at`, `updated_at`) VALUES
(1, 'David', 'david@mailinator.com', '1', '1', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(2, 'Rosa', 'rosa@mailinator.com', '2', '1', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(3, 'Rosy', 'rosy@gmail.com', '2', '1', NULL, '2021-06-25 02:39:07', '2021-06-25 02:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `child_relations`
--

CREATE TABLE `child_relations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rel_from` int(11) NOT NULL,
  `rel_to` int(11) NOT NULL,
  `rel_type` enum('staff','parent') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'parent',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `child_relations`
--

INSERT INTO `child_relations` (`id`, `rel_from`, `rel_to`, `rel_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'parent', '1', '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(2, 2, 2, 'parent', '1', '2021-06-25 02:01:39', '2021-06-25 02:01:39'),
(3, 3, 2, 'staff', '1', '2021-06-25 02:02:52', '2021-06-25 02:02:52'),
(4, 3, 1, 'staff', '1', '2021-06-25 02:34:31', '2021-06-25 02:37:40'),
(5, 2, 3, 'parent', '1', '2021-06-25 02:39:07', '2021-06-25 02:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `code` varchar(155) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id`, `name`, `code`, `updated_at`, `created_at`) VALUES
(1, 'Disability', 'disability', '2021-06-25 01:06:08', '2021-06-22 08:42:52'),
(2, 'Education', 'education', '2021-06-25 01:06:19', '2021-06-22 08:45:01'),
(3, 'Health/Nutrition', 'health-nutrition', '2021-06-25 01:06:47', '2021-06-22 08:45:01'),
(4, 'Family services', 'family-services', '2021-06-25 01:07:01', '2021-06-22 08:45:52'),
(5, 'Mental Health', 'mental-health', '2021-06-25 01:07:18', '2021-06-22 08:45:52'),
(6, 'Internal Report', 'internal-report', '2021-06-25 01:08:10', '2021-06-25 01:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `component_resources`
--

CREATE TABLE `component_resources` (
  `id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `document_type` bigint(20) NOT NULL DEFAULT 1,
  `topic` mediumtext DEFAULT NULL,
  `additional_info` mediumtext DEFAULT NULL,
  `link` varchar(155) DEFAULT NULL,
  `file` text NOT NULL,
  `date` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `component_resources`
--

INSERT INTO `component_resources` (`id`, `component_id`, `document_type`, `topic`, `additional_info`, `link`, `file`, `date`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'community partners', '', '', '', NULL, '2021-06-30 04:15:44', '2021-06-23 02:51:55'),
(2, 1, 1, 'FDLRS referral and related', '', '', '', NULL, '2021-06-30 04:15:56', '2021-06-23 02:51:55'),
(4, 2, 1, 'Testing', '', 'http://56.octaldevs.com/philatelic/admin/product-manager/library', '', NULL, '2021-06-30 09:34:00', '2021-06-24 22:01:42'),
(6, 2, 2, 'test ed', NULL, NULL, '1625053750.pdf', NULL, '2021-06-30 06:19:10', '2021-06-30 06:19:10'),
(7, 2, 2, 'Testingggggg', NULL, NULL, '1625053597.txt', NULL, '2021-06-30 06:34:21', '2021-06-30 06:34:21'),
(10, 2, 3, 'testing for education section', NULL, NULL, '1625117435.jpg', NULL, '2021-07-01 00:00:35', '2021-07-01 00:00:35');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Staff',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Disability', 1, NULL, NULL),
(2, 'Teaching', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` bigint(20) NOT NULL,
  `type` varchar(155) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'resource', '2021-06-29 16:04:58', '2021-06-29 16:04:58'),
(2, 'education', '2021-06-29 16:04:58', '2021-06-29 16:04:58'),
(3, 'school Readiness/Assessmet Result ', '2021-07-01 05:30:29', '2021-07-01 05:30:29');

-- --------------------------------------------------------

--
-- Table structure for table `event_types`
--

CREATE TABLE `event_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_category` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_types`
--

INSERT INTO `event_types` (`id`, `name`, `event_category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Teacher Planning Day', '0', '1', NULL, NULL),
(2, 'Holiday', '0', '1', NULL, NULL),
(3, 'Emergency Closed', '0', '1', NULL, NULL),
(4, 'Parents Meeting', '0', '1', NULL, NULL),
(5, 'Mobile Bus', '1', '1', NULL, NULL),
(6, 'Home Visit', '1', '1', NULL, NULL),
(7, 'MH Intern Counselling services', '1', '1', NULL, NULL),
(8, 'Disabilities', '1', '1', NULL, NULL),
(9, 'Home Visit', '1', '1', NULL, NULL),
(10, 'Conference', '1', '1', NULL, NULL),
(11, 'Techhelp', '0', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2019_08_19_000000_create_failed_jobs_table', 1),
(12, '2021_02_20_090712_create_designations_table', 1),
(13, '2021_02_20_104440_create_user_details_table', 1),
(14, '2021_02_20_104900_add_columns_to_users_table', 1),
(15, '2021_02_27_115642_create_calendars_table', 1),
(16, '2021_06_23_130354_create_children_table', 2),
(18, '2021_06_24_111930_create_child_relations_table', 3),
(19, '2021_06_25_111424_create_event_types_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `designation` bigint(20) NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `user_role` tinyint(4) NOT NULL DEFAULT 1,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `designation`, `phone`, `is_admin`, `user_role`, `profile`, `profile_photo_path`, `deleted_at`) VALUES
(1, 'Admin', 'admin@lejardin.com', NULL, '$2y$10$pmNXO9EDgdmzXur4ZZTdPeL3bzoYKG7Dogurit/mSVnYCgj3l/HJq', NULL, '2021-06-25 01:55:25', '2021-06-25 01:59:05', 0, '(184) 431-4111', 1, 1, '2127091500.jpg', NULL, NULL),
(2, 'Richel Watt', 'richel@mailinator.com', NULL, '$2y$10$lHjkR0Qw/Vg9YaM/.c8JQugkaQ.4xWWBsacIlnQnFiFU/iFZ0u8k.', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38', 0, '(185) 542-5845', 0, 3, '543097252.jpg', NULL, NULL),
(3, 'Brosley', 'Brosley@mailinator.com', NULL, '$2y$10$IeL9T0x4f875xbI3EsgT7u0/mkJ7FeVebymDLUSQHnyQF7iWuZciW', NULL, '2021-06-25 02:02:51', '2021-06-25 02:02:51', 2, '(186) 622-7889', 0, 2, '476662887.jpg', NULL, NULL),
(4, 'test', 'test@mailinator.com', NULL, '$2y$10$m8Zh1sXjBRTWnej8GrHHSunCrbRzSAxw6ydknJQRpoMwvHzl0fYHi', NULL, '2021-06-25 02:03:41', '2021-06-25 02:07:35', 0, '(666) 666-6666', 0, 1, '1969020905.png', NULL, '2021-06-25 02:07:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `phone_home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `address1` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `git_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outlook_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slack_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `my_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skill` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `status`, `phone_home`, `city`, `state`, `country`, `zip`, `address1`, `address2`, `git_acc`, `twitter_acc`, `linkedin_acc`, `facebook_acc`, `outlook_acc`, `instagram_acc`, `slack_acc`, `my_acc`, `other_acc`, `aadhar_no`, `website`, `pan_no`, `work`, `education`, `skill`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '(184) 453-5286', NULL, NULL, NULL, NULL, '9820 Leatherwood Dr.\r\nAbsecon, NJ 08205', '7842 West Saxton Ave.\r\nWest Haven, CT 06516', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 1, '(184) 480-8104', NULL, NULL, NULL, NULL, '37 College Ave.\r\nMinot, ND 58701', '37 College Ave.\r\nMinot, ND 58701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, '(186) 644-7840', NULL, NULL, NULL, NULL, '68 E. Constitution Dr.\r\nFairport, NY 14450', '68 E. Constitution Dr.\r\nFairport, NY 14450', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, '(777) 777-7777', NULL, NULL, NULL, NULL, 'dfdfd', 'fsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calendars`
--
ALTER TABLE `calendars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `children_email_unique` (`email`);

--
-- Indexes for table `child_relations`
--
ALTER TABLE `child_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_resources`
--
ALTER TABLE `component_resources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `component_id` (`component_id`),
  ADD KEY `document_type` (`document_type`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_types`
--
ALTER TABLE `event_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calendars`
--
ALTER TABLE `calendars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `child_relations`
--
ALTER TABLE `child_relations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `component_resources`
--
ALTER TABLE `component_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `event_types`
--
ALTER TABLE `event_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `component_resources`
--
ALTER TABLE `component_resources`
  ADD CONSTRAINT `component_resources_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `components` (`id`),
  ADD CONSTRAINT `component_resources_ibfk_2` FOREIGN KEY (`document_type`) REFERENCES `document_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
