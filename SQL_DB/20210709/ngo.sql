-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2021 at 04:08 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ngo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `timestamp` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `user_id`, `description`, `timestamp`, `created_at`, `updated_at`) VALUES
(1, 1, 'Update the disability staff Resources.', 1625569229, '2021-07-06 11:00:29', '2021-07-06 05:30:29'),
(2, 5, 'Update the disability staff Resources.', 1625573627, '2021-07-06 12:13:47', '2021-07-06 06:43:47');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `children` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marked_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `timestamp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `children`, `marked_by`, `description`, `date`, `day`, `month`, `year`, `timestamp`, `created_at`, `updated_at`) VALUES
(1, '[{\"child\":\"1\",\"attendance\":1},{\"child\":\"2\",\"attendance\":0},{\"child\":\"3\",\"attendance\":0}]', '1', 'test Desc', '2021-07-27', 27, 7, 2021, '1625668248', '2021-07-07 03:13:23', '2021-07-07 09:00:48'),
(2, '[{\"child\":\"1\",\"attendance\":0},{\"child\":\"2\",\"attendance\":1},{\"child\":\"3\",\"attendance\":0}]', '1', 'sssadsad', '2020-07-11', 11, 7, 2020, '1625647782', '2021-07-07 03:19:42', '2021-07-07 03:19:42'),
(3, '[{\"child\":\"1\",\"attendance\":1},{\"child\":\"2\",\"attendance\":1},{\"child\":\"3\",\"attendance\":1}]', '1', '', '2021-07-15', 15, 7, 2021, '1625733935', '2021-07-08 03:15:35', '2021-07-08 03:15:35'),
(4, '[{\"child\":\"1\",\"attendance\":0},{\"child\":\"2\",\"attendance\":1},{\"child\":\"3\",\"attendance\":0}]', '3', '', '2021-06-16', 16, 6, 2021, '1625754901', '2021-07-08 09:03:05', '2021-07-08 09:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `calendars`
--

CREATE TABLE `calendars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_type` int(11) NOT NULL DEFAULT 1,
  `event_type` int(11) NOT NULL DEFAULT 1,
  `event_name` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(4) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 1,
  `bgcolor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#d8fed1',
  `brdcolor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#FF0000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `calendars`
--

INSERT INTO `calendars` (`id`, `user_id`, `title`, `calendar_type`, `event_type`, `event_name`, `description`, `staff`, `parent`, `is_public`, `status`, `start_date`, `end_date`, `priority`, `bgcolor`, `brdcolor`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'hi', 1, 2, 8, 'tet', '6,3', '2', 1, 1, '2021-06-02T07:55:00', '2021-07-02T15:50:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-25 09:44:34', '2021-06-25 09:44:34'),
(3, 1, 'testmeeting', 1, 1, 4, 'asasasas', NULL, NULL, 1, 1, '2021-06-30T12:00:00', '2021-06-30T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 09:22:57', '2021-06-30 09:22:57'),
(4, 1, 'asa', 1, 1, 4, 'sas', '6,3', NULL, 1, 1, '2021-06-30T12:00:00', '2021-06-30T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 09:32:08', '2021-06-30 09:32:08'),
(5, 1, 'meetings', 1, 1, 4, 'sasas', '6', NULL, 1, 1, '2021-07-01T12:00:00', '2021-07-04T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 23:18:53', '2021-06-30 23:18:53'),
(6, 1, 'meet', 1, 1, 1, 'as da as das', '6', '9,2', 1, 1, '2021-07-01T12:00:00', '2021-07-06T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-06-30 23:21:32', '2021-06-30 23:21:32'),
(7, 3, 'asasa', 1, 1, 1, 'aSAS', '6', '10', 1, 1, '2021-07-02T12:00:00', '2021-07-02T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-07-02 09:14:59', '2021-07-02 09:14:59'),
(8, 3, 'asasa', 1, 1, 1, 'aSAS', '6', '10', 1, 1, '2021-07-02T12:00:00', '2021-07-02T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-07-02 09:19:16', '2021-07-02 09:19:16'),
(9, 1, 'test event', 1, 1, 1, NULL, '6', '9', 1, 1, '2021-07-05T12:00:00', '2021-07-05T12:00:00', 1, '#d8fed1', '#FF0000', NULL, '2021-07-05 08:45:12', '2021-07-05 08:45:12');

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `adresss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`id`, `name`, `email`, `gender`, `status`, `adresss`, `created_at`, `updated_at`) VALUES
(1, 'David', 'david@mailinator.com', '1', '1', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(2, 'Rosa', 'rosa@mailinator.com', '2', '1', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(3, 'Rosy', 'rosy@gmail.com', '2', '1', NULL, '2021-06-25 02:39:07', '2021-06-25 02:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `child_relations`
--

CREATE TABLE `child_relations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rel_from` int(11) NOT NULL,
  `rel_to` int(11) NOT NULL,
  `rel_type` enum('staff','parent') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'parent',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `child_relations`
--

INSERT INTO `child_relations` (`id`, `rel_from`, `rel_to`, `rel_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'parent', '1', '2021-06-25 02:01:38', '2021-06-25 02:01:38'),
(2, 2, 2, 'parent', '1', '2021-06-25 02:01:39', '2021-06-25 02:01:39'),
(3, 3, 2, 'staff', '1', '2021-06-25 02:02:52', '2021-06-25 02:02:52'),
(4, 3, 1, 'staff', '1', '2021-06-25 02:34:31', '2021-06-25 02:37:40'),
(5, 2, 3, 'parent', '1', '2021-06-25 02:39:07', '2021-06-25 02:39:07'),
(6, 6, 2, 'staff', '0', '2021-06-28 06:51:46', '2021-06-29 07:29:09'),
(7, 6, 1, 'staff', '1', '2021-06-28 06:52:07', '2021-06-28 06:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `component_resources`
--

CREATE TABLE `component_resources` (
  `id` int(11) UNSIGNED NOT NULL,
  `designation_id` bigint(20) UNSIGNED NOT NULL,
  `document_type` bigint(20) NOT NULL DEFAULT 1,
  `topic` mediumtext DEFAULT NULL,
  `additional_info` mediumtext DEFAULT NULL,
  `link` varchar(155) DEFAULT NULL,
  `file` text NOT NULL,
  `date` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `component_resources`
--

INSERT INTO `component_resources` (`id`, `designation_id`, `document_type`, `topic`, `additional_info`, `link`, `file`, `date`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'community partners', '', 'http://73.octaldevs.com/leJardin/lejardinweb/public/login', '', NULL, '2021-06-30 22:31:39', '2021-06-22 21:21:55'),
(2, 1, 1, 'FDLRS referral and related', '', '', '1625133160.jpg', NULL, '2021-06-30 22:52:40', '2021-06-22 21:21:55'),
(4, 2, 8, 'Testing', '', 'http://56.octaldevs.com/philatelic/admin/product-manager/library', '', NULL, '2021-07-02 01:22:47', '2021-06-24 16:31:42'),
(6, 2, 2, 'test ed', NULL, NULL, '1625053750.pdf', NULL, '2021-06-30 00:49:10', '2021-06-30 00:49:10'),
(10, 2, 3, 'testing for education section', NULL, NULL, '1625117435.jpg', NULL, '2021-06-30 18:30:35', '2021-06-30 18:30:35'),
(11, 2, 2, 'Testingggg', NULL, NULL, '1625133839.jpg', NULL, '2021-06-30 23:04:15', '2021-06-30 23:01:34'),
(12, 3, 6, 'Lead result', '', '', '1625137728.txt', NULL, '2021-07-01 07:21:48', '2021-07-01 00:08:48'),
(13, 3, 4, 'Health plan', NULL, NULL, '1625140340.jpg', NULL, '2021-07-01 00:52:20', '2021-07-01 00:52:20'),
(14, 1, 1, 'Types of therapy', '', '', '1625202331.jpg', NULL, '2021-07-01 18:06:50', '2021-07-01 18:05:31'),
(15, 3, 5, 'Physical exam', '', '', '1625204636.jpg', NULL, '2021-07-01 18:43:56', '2021-07-01 18:43:56'),
(16, 5, 7, 'community partners', '', '', '1625215612.jpg', NULL, '2021-07-02 03:16:52', '2021-07-02 03:16:52'),
(17, 3, 6, 'heamoglobin result', '', '', '1625229450.jpg', NULL, '2021-07-02 07:07:30', '2021-07-02 07:07:30'),
(18, 3, 4, 'testt', '', '', '1625232639.jpg', NULL, '2021-07-05 02:19:59', '2021-07-02 08:00:39'),
(19, 3, 6, 'Types of therapy', '', '', '1625232661.jpg', NULL, '2021-07-02 08:01:01', '2021-07-02 08:01:01'),
(20, 5, 7, 'Testingggggg', '', '', '1625232744.jpg', NULL, '2021-07-02 08:02:24', '2021-07-02 08:02:24'),
(21, 1, 1, 'test ed', '', 'http://56.octaldevs.com/philatelic/admin/product-manager/library', '', NULL, '2021-07-05 01:06:01', '2021-07-05 01:03:37'),
(22, 2, 8, 'Flow chart', '', '', '1625475989.html', NULL, '2021-07-05 03:36:29', '2021-07-05 03:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Staff',
  `code` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Disability', 'disability', 1, NULL, NULL),
(2, 'Education', 'education', 1, NULL, NULL),
(3, 'Health/Nutrition', 'health-nutrition', 1, NULL, NULL),
(4, 'Family services', 'family-services', 1, NULL, NULL),
(5, 'Mental Health', 'mental-health', 1, NULL, NULL),
(6, 'Internal Report', 'internal-report', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` bigint(20) NOT NULL,
  `type` varchar(155) NOT NULL,
  `designation_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `type`, `designation_id`, `created_at`, `updated_at`) VALUES
(1, 'disability-resource', 1, '2021-06-29 05:04:58', '2021-06-29 05:04:58'),
(2, 'education-docs', 2, '2021-06-29 05:04:58', '2021-06-29 05:04:58'),
(3, 'education-result-rediness', 2, '2021-06-30 18:30:29', '2021-06-30 18:30:29'),
(4, 'health-ehs', 3, '2021-07-01 00:48:46', '2021-07-01 00:48:46'),
(5, 'health-hs', 3, '2021-07-01 00:48:46', '2021-07-01 00:48:46'),
(6, 'health-nutrition', 3, '2021-07-01 01:31:34', '2021-07-01 01:31:34'),
(7, 'mental-resource', 5, '2021-07-01 18:58:09', '2021-07-01 18:58:09'),
(8, 'education-resource', 2, '2021-07-01 18:58:09', '2021-07-01 18:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `name`, `subject`, `email_type`, `email_body`, `created_at`, `updated_at`) VALUES
(1, 'newsletter', 'NEWSLETTER FROM LE JARDIN', 'newsletter', '<p>Hi TO_NAME;</p>\r\n\r\n<p>POSTED_BY has sent a newsletter.</p>\r\n<div class=\"card-body\">\r\n<div class=\"card-header\">TITLE</div>\r\n<div class=\"card-body\">DESCRIPTION</div>\r\n</div>\r\n\r\n<p>Thanks</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_types`
--

CREATE TABLE `email_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `constants` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_types`
--

INSERT INTO `email_types` (`id`, `type`, `constants`, `created_at`, `updated_at`) VALUES
(1, 'newsletter', 'TO_NAME,DESCRIPTION,TITLE,POSTED_BY', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_types`
--

CREATE TABLE `event_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_category` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_types`
--

INSERT INTO `event_types` (`id`, `name`, `event_category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Teacher Planning Day', '0', '1', NULL, NULL),
(2, 'Holiday', '0', '1', NULL, NULL),
(3, 'Emergency Closed', '0', '1', NULL, NULL),
(4, 'Parents Meeting', '0', '1', NULL, NULL),
(5, 'Mobile Bus', '1', '1', NULL, NULL),
(6, 'Home Visit', '1', '1', NULL, NULL),
(7, 'MH Intern Counselling services', '1', '1', NULL, NULL),
(8, 'Disabilities', '1', '1', NULL, NULL),
(9, 'Home Visit', '1', '1', NULL, NULL),
(10, 'Conference', '1', '1', NULL, NULL),
(11, 'Techhelp', '0', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

CREATE TABLE `feeds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_by` int(11) NOT NULL,
  `privacy` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `parent` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feeds`
--

INSERT INTO `feeds` (`id`, `title`, `desc`, `link`, `posted_by`, `privacy`, `parent`, `staff`, `created_at`, `updated_at`) VALUES
(2, 'test', 'test  Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans.', '580769187.jpg', 1, '0', '10', '', '2021-06-30 08:13:39', '2021-07-08 09:01:20'),
(3, 'rrr', 'ewrwer', NULL, 1, '0', '', NULL, '2021-07-01 03:32:34', '2021-07-01 03:32:34'),
(4, 'sasa', 'sasas', '270336916.jpg', 1, '0', NULL, '3,6', '2021-07-01 04:46:46', '2021-07-01 04:46:46'),
(5, 'aAAsdsds', 'dsdsdsdsd', '1499758866.png', 1, '0', NULL, '3,6', '2021-07-01 04:50:32', '2021-07-01 04:50:32'),
(6, 'ugugug', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.sdasd', '17213157.jpg', 1, '1', '', '', '2021-07-01 08:28:37', '2021-07-01 08:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` decimal(10,7) DEFAULT NULL,
  `long` float(10,7) DEFAULT NULL,
  `place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `lat`, `long`, `place`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Center A', '26.8398509', 75.8009338, 'Jawahar Circle Garden, Jawahar Circle, Malviya Nagar, Jaipur, Rajasthan, India', '1', NULL, '2021-06-29 07:59:52'),
(2, 'Center B', '22.7195687', 75.8577271, 'Indorė, Madhya Pradesh, India', '1', NULL, '2021-06-29 08:00:05'),
(4, 'tetw', '-33.8892658', 151.1242371, 'Woollahra NSW, Australia', '1', '2021-06-29 07:59:33', '2021-06-29 20:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2019_08_19_000000_create_failed_jobs_table', 1),
(12, '2021_02_20_090712_create_designations_table', 1),
(13, '2021_02_20_104440_create_user_details_table', 1),
(14, '2021_02_20_104900_add_columns_to_users_table', 1),
(15, '2021_02_27_115642_create_calendars_table', 1),
(16, '2021_06_23_130354_create_children_table', 2),
(18, '2021_06_24_111930_create_child_relations_table', 3),
(19, '2021_06_25_111424_create_event_types_table', 4),
(21, '2021_06_25_121405_create_locations_table', 5),
(22, '2021_06_30_100229_create_feeds_table', 6),
(23, '2021_07_02_075332_create_newsletters_table', 7),
(24, '2021_07_02_115443_create_email_types_table', 8),
(25, '2021_07_02_115455_create_email_templates_table', 8),
(26, '2021_07_06_074728_create_attendance_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `title`, `desc`, `link`, `staff`, `parent`, `posted_by`, `created_at`, `updated_at`) VALUES
(1, 'developer', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', NULL, '3,6', '2,10', 1, '2021-07-02 04:52:07', '2021-07-02 04:52:07'),
(2, 'newnewletter', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '156205368.docx', '3,6', '2,10', 1, '2021-07-02 04:55:46', '2021-07-02 04:55:46'),
(3, 'newnewletter', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1733453577.docx', '3,6', '2,10', 1, '2021-07-02 04:57:39', '2021-07-02 04:57:39'),
(4, 'newnewletter', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1810174026.docx', '3,6', '2,10', 1, '2021-07-02 05:02:34', '2021-07-02 05:02:35'),
(5, 'testtt', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.sdasd', '375665022.docx', '6', '2', 1, '2021-07-02 05:08:17', '2021-07-02 05:23:44'),
(6, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '615300874.docx', '6', '10', 1, '2021-07-02 06:46:36', '2021-07-02 06:46:36'),
(7, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1366285548.docx', '6', '10', 1, '2021-07-02 06:47:17', '2021-07-02 06:47:17'),
(8, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1830345099.docx', '6', '10', 1, '2021-07-02 06:47:48', '2021-07-02 06:47:48'),
(9, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '2062321121.docx', '6', '10', 1, '2021-07-02 06:48:15', '2021-07-02 06:48:15'),
(10, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '599650134.docx', '6', '10', 1, '2021-07-02 06:48:26', '2021-07-02 06:48:26'),
(11, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '1417012095.jpg', '', '', 1, '2021-07-02 06:49:08', '2021-07-02 06:51:35'),
(12, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '826083631.docx', '6', '10', 1, '2021-07-02 06:50:06', '2021-07-02 06:50:06'),
(13, 'sadasd', 'Lorem ipsum is a dummy text used to replace text in some areas just for the purpose of an example. It can be used in publishing and graphic design.', '684750219.docx', '6', '10', 1, '2021-07-02 06:50:59', '2021-07-02 06:50:59'),
(14, 'nininin', 'jiuigg', '572323589.docx', '3,6', '7,10', 1, '2021-07-05 03:00:57', '2021-07-05 03:00:57'),
(15, 'dfwefew', 'ewfwe', NULL, '', '', 1, '2021-07-05 03:03:25', '2021-07-05 03:03:25'),
(16, 'ndf', 'gdfg', NULL, '', '', 1, '2021-07-05 03:32:33', '2021-07-05 03:32:33'),
(17, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:33:10', '2021-07-05 03:33:10'),
(18, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:33:35', '2021-07-05 03:33:35'),
(19, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:33:48', '2021-07-05 03:33:48'),
(20, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:34:04', '2021-07-05 03:34:04'),
(21, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:34:12', '2021-07-05 03:34:12'),
(22, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:34:21', '2021-07-05 03:34:21'),
(23, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:34:35', '2021-07-05 03:34:35'),
(24, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:35:18', '2021-07-05 03:35:18'),
(25, 'dsd', 'dsad', NULL, '', '', 1, '2021-07-05 03:37:13', '2021-07-05 03:37:13'),
(26, 'test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', NULL, '', '', 1, '2021-07-05 03:39:40', '2021-07-05 03:39:40'),
(27, 'test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', NULL, '', '', 1, '2021-07-05 03:40:57', '2021-07-05 03:40:57'),
(28, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:09:20', '2021-07-05 05:09:20'),
(29, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:09:35', '2021-07-05 05:09:35'),
(30, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:09:46', '2021-07-05 05:09:46'),
(31, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:10:35', '2021-07-05 05:10:35'),
(32, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:10:58', '2021-07-05 05:10:58'),
(33, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:12:09', '2021-07-05 05:12:09'),
(34, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:12:22', '2021-07-05 05:12:22'),
(35, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:13:52', '2021-07-05 05:13:52'),
(36, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:14:02', '2021-07-05 05:14:02'),
(37, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:14:33', '2021-07-05 05:14:33'),
(38, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:14:39', '2021-07-05 05:14:39'),
(39, 'rewr', 'rewre', NULL, '6', '9', 1, '2021-07-05 05:15:05', '2021-07-05 05:15:05'),
(40, 'test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', NULL, '3', '2', 1, '2021-07-05 05:17:46', '2021-07-05 05:17:46'),
(41, 'withatta', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', '333625128.docx', '3', '2', 1, '2021-07-05 05:18:40', '2021-07-05 05:18:40'),
(42, 'dsadsa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', '834354335.docx', '3', '2', 1, '2021-07-05 05:26:24', '2021-07-05 05:26:24'),
(43, 'sas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', NULL, '3', '2', 1, '2021-07-05 05:27:26', '2021-07-05 05:27:26'),
(44, 'dsadsa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum vel risus vitae pretium. Ut ac metus a elit euismod eleifend sed non lacus. Nullam varius ante iaculis euismod accumsan. Duis varius maximus ipsum, ac feugiat enim mollis nec. Cras nec bibendum leo. In sagittis sollicitudin lectus at pharetra. Sed at lorem fringilla, vulputate nibh in, blandit mauris. Aenean hendrerit tristique eros id aliquam. Donec tincidunt eget nisl non tincidunt. Integer tortor mi, hendrerit at diam vitae, posuere tempus libero. Vivamus pretium pellentesque enim et lobortis. Vestibulum in elit iaculis, vehicula enim et, maximus risus. Vestibulum dictum non purus eu dictum. Sed id felis ut ex elementum luctus. Praesent vestibulum luctus rutrum. Duis consectetur cursus est.', '914038385.docx', '3', '2', 1, '2021-07-05 05:35:45', '2021-07-05 05:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@lejardin.com', '$2y$10$acKEUurdkEPtORf5oRGcfO4WcMVt43sNEcPqQcWBBk9ONyHYoL21.', '2021-07-05 00:38:04'),
('admin@mailinator.com', '$2y$10$fgC0nr7Vq/D2DzznqCxIGee4i1de4hGZcfLM8WZ4qfcWJR7BdNDpe', '2021-07-05 01:22:21'),
('nik@mailinator.com', '$2y$10$/no2Ig5RCJ4Y6t6vHp7VpuR/aVnDjFbgTALAtmfyVqU22rMKDzXMu', '2021-07-05 01:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `designation` tinyint(1) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `user_role` tinyint(4) NOT NULL DEFAULT 1,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `designation`, `phone`, `is_admin`, `user_role`, `profile`, `profile_photo_path`, `deleted_at`) VALUES
(1, 'Admin', 'admin@lejardin.com', NULL, '$2y$10$pmNXO9EDgdmzXur4ZZTdPeL3bzoYKG7Dogurit/mSVnYCgj3l/HJq', NULL, '2021-06-25 01:55:25', '2021-06-25 01:59:05', NULL, '(184) 431-4111', 1, 1, '2127091500.jpg', NULL, NULL),
(2, 'Richel Watt', 'richel@mailinator.com', NULL, '$2y$10$lHjkR0Qw/Vg9YaM/.c8JQugkaQ.4xWWBsacIlnQnFiFU/iFZ0u8k.', NULL, '2021-06-25 02:01:38', '2021-06-25 02:01:38', NULL, '(185) 542-5845', 0, 3, '543097252.jpg', NULL, NULL),
(3, 'Brosley', 'Brosley@mailinator.com', NULL, '$2y$10$IeL9T0x4f875xbI3EsgT7u0/mkJ7FeVebymDLUSQHnyQF7iWuZciW', NULL, '2021-06-25 02:02:51', '2021-06-28 05:20:40', 2, '(186) 622-7889', 0, 2, '476662887.jpg', NULL, NULL),
(4, 'test', 'test@mailinator.com', NULL, '$2y$10$m8Zh1sXjBRTWnej8GrHHSunCrbRzSAxw6ydknJQRpoMwvHzl0fYHi', NULL, '2021-06-25 02:03:41', '2021-06-25 02:07:35', NULL, '(666) 666-6666', 0, 1, '1969020905.png', NULL, '2021-06-25 02:07:35'),
(5, 'Maria', 'maria@gmail.com', NULL, '$2y$10$lalk4ajb1wuNXfc8nW6VJeGpQ.Xt.NzOWGFb7W2SLcHyRBFlC26A2', NULL, '2021-06-28 06:43:54', '2021-06-28 06:49:05', NULL, '(999) 999-9999', 0, 1, '550398394.jpg', NULL, NULL),
(6, 'Pujara', 'pujara@gmail.com', NULL, '$2y$10$KRh66Swzww9DG/3V2uhQK.oKuuXvnNSuI.NgWBDkY2kxzdc2lOQdy', NULL, '2021-06-28 06:51:46', '2021-06-29 07:29:08', 1, '(437) 879-7097', 0, 2, '2123340209.jpg', NULL, NULL),
(7, 'tst', 'testparent@lejardin.com', NULL, '$2y$10$MFmae2E2t6zAjA34GCsnfO0/GbBX6xAWyx/SSumse/sg7TsAztLdO', NULL, '2021-06-30 06:03:03', '2021-06-30 06:39:40', NULL, '(999) 999-9999', 0, 3, '1967764789.png', NULL, NULL),
(8, 'tst', 'testparent2@lejardin.com', NULL, '$2y$10$mVdqIkmZqtxT94h2Co8xJ.YMlbY.Pa7IeBcU8bV/voW80A7UT2FiW', NULL, '2021-06-30 06:04:07', '2021-06-30 06:04:08', NULL, '(999) 999-9999', 0, 3, '408814525.PNG', NULL, NULL),
(9, 'tstre', 'testparent1@lejardin.com', NULL, '$2y$10$AJj2pDVBiPnh0djdxQJaXOJzMf2TohnFCU49MooiTtD72i2k8qwNu', NULL, '2021-06-30 06:05:01', '2021-06-30 06:05:01', NULL, '(999) 999-9999', 0, 3, '1070897148.jpg', NULL, NULL),
(10, 'tstre', 'testparent4@lejardin.com', NULL, '$2y$10$gZj2bNJUSD7qXohWrpgIh.f/DyxSbLyOorW9kxNUEnHhBIU9doNKO', NULL, '2021-06-30 06:09:21', '2021-06-30 06:09:22', NULL, '(999) 999-9999', 0, 3, '88885210.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `phone_home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `address1` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `git_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outlook_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slack_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `my_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skill` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `status`, `phone_home`, `city`, `state`, `country`, `zip`, `address1`, `address2`, `git_acc`, `twitter_acc`, `linkedin_acc`, `facebook_acc`, `outlook_acc`, `instagram_acc`, `slack_acc`, `my_acc`, `other_acc`, `aadhar_no`, `website`, `pan_no`, `work`, `education`, `skill`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '(184) 453-5286', NULL, NULL, NULL, NULL, '9820 Leatherwood Dr.\r\nAbsecon, NJ 08205', '7842 West Saxton Ave.\r\nWest Haven, CT 06516', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 0, '(184) 480-8104', NULL, NULL, NULL, NULL, '37 College Ave.\r\nMinot, ND 58701', '37 College Ave.\r\nMinot, ND 58701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, '(186) 644-7840', NULL, NULL, NULL, NULL, '68 E. Constitution Dr.\r\nFairport, NY 14450', '68 E. Constitution Dr.\r\nFairport, NY 14450', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, '(777) 777-7777', NULL, NULL, NULL, NULL, 'dfdfd', 'fsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, '(087) 912-6435', NULL, NULL, NULL, NULL, 'Tst t e', 'permanent lives here', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 1, '(122) 222-2211', NULL, NULL, NULL, NULL, 'saS jk', 'ASa ok', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf', 'af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf', 'af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 9, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf', 'af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, 1, '(787) 858-4124', NULL, NULL, NULL, NULL, 'fsaf ffsafas', 'afasf  safsa a sa a a f dsdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calendars`
--
ALTER TABLE `calendars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `children_email_unique` (`email`);

--
-- Indexes for table `child_relations`
--
ALTER TABLE `child_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_resources`
--
ALTER TABLE `component_resources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `component_id` (`designation_id`),
  ADD KEY `document_type` (`document_type`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_types`
--
ALTER TABLE `email_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_types`
--
ALTER TABLE `event_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `feeds`
--
ALTER TABLE `feeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `calendars`
--
ALTER TABLE `calendars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `child_relations`
--
ALTER TABLE `child_relations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `component_resources`
--
ALTER TABLE `component_resources`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email_types`
--
ALTER TABLE `email_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event_types`
--
ALTER TABLE `event_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feeds`
--
ALTER TABLE `feeds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD CONSTRAINT `activity_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `component_resources`
--
ALTER TABLE `component_resources`
  ADD CONSTRAINT `component_resources_ibfk_1` FOREIGN KEY (`document_type`) REFERENCES `document_types` (`id`),
  ADD CONSTRAINT `component_resources_ibfk_2` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
