<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cache-clear', function() {
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cleared!</h1>';
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\admin\HomeController::class, 'index'])->name('home');



Route::group(['middleware'=>['auth']],
	function(){

		Route::view('calendar', 'calendar/calendar');
		Route::get('messaging', 'App\Http\Controllers\admin\CommuniqueController@chat');
		Route::get('/load-latest-messages', 'App\Http\Controllers\admin\CommuniqueController@getLoadLatestMessages');
		Route::post('/send', 'App\Http\Controllers\admin\CommuniqueController@postSendMessage');
		Route::get('/fetch-old-messages', 'App\Http\Controllers\admin\CommuniqueController@getOldMessages');

		Route::view('app/mail', 'communique/mail');

		Route::get('staff', 'App\Http\Controllers\admin\UsersController@staff');
		Route::get('parent', 'App\Http\Controllers\admin\UsersController@parent');
		Route::get('users/{id?}', 'App\Http\Controllers\admin\UsersController@index');
		Route::get('editUser/{id?}', 'App\Http\Controllers\admin\UsersController@saveUserForm');
		Route::get('viewUser/{id?}', 'App\Http\Controllers\admin\UsersController@viewUser');
		Route::get('staff_details/{id?}', 'App\View\Components\staffdetails@render');
		Route::post('saveUserDetails/{id?}', 'App\Http\Controllers\admin\UsersController@saveUserDetails');
		Route::post('updateUserDetails/{id?}', 'App\Http\Controllers\admin\UsersController@updateUserDetails');
		Route::post('updatePassword', 'App\Http\Controllers\admin\UsersController@updatePassword');
		Route::get('deleteUser/{id?}', 'App\Http\Controllers\admin\UsersController@deleteUser');
		Route::get('chstatusUser/{id?}', 'App\Http\Controllers\admin\UsersController@chstatusUser');
		Route::post('uploadImageUser/{id?}','App\Http\Controllers\admin\UsersController@uploadImageUser');
		Route::any('editAttendance','App\Http\Controllers\admin\CalendarController@editAttendance');
		Route::any('attendance','App\Http\Controllers\admin\CalendarController@attendance');

		Route::get('locations/','App\Http\Controllers\admin\LocationController@index');
		Route::any('editLocation/{id?}','App\Http\Controllers\admin\LocationController@edit');
		Route::any('deleteLocation/{id?}','App\Http\Controllers\admin\LocationController@delete');

		Route::get('feeds/','App\Http\Controllers\admin\FeedsController@index');
		Route::any('editFeed/{id?}','App\Http\Controllers\admin\FeedsController@edit');
		Route::any('deleteFeed/{id?}','App\Http\Controllers\admin\FeedsController@delete');
		Route::get('likeFeed','App\Http\Controllers\admin\FeedsController@likeFeed');
		Route::post('addComment','App\Http\Controllers\admin\FeedsController@addComment');
		Route::post('removeComment','App\Http\Controllers\admin\FeedsController@removeComment');
		
		Route::get('newsletters/','App\Http\Controllers\admin\NewslettersController@index');
		Route::any('editNewsletter/{id?}','App\Http\Controllers\admin\NewslettersController@edit');
		Route::any('deleteNewsletter/{id?}','App\Http\Controllers\admin\NewslettersController@delete');
		
		Route::get('profile/{id?}', 'App\Http\Controllers\admin\ProfileController@index');
		Route::get('profileform/{id?}', 'App\Http\Controllers\admin\ProfileController@profileForm');
		Route::get('profileskillsform/{id?}', 'App\Http\Controllers\admin\ProfileController@profileSkillsForm');
		Route::post('updateUserProfile/{id?}', 'App\Http\Controllers\admin\ProfileController@edit');
		Route::post('updateUserSkills/{id?}', 'App\Http\Controllers\ProfileController@updateUserSkills');
		
		Route::post('addCalDetails/{id?}', 'App\Http\Controllers\admin\CalendarController@addCalDetails');
		Route::get('deleteCalendar/{id?}', 'App\Http\Controllers\admin\CalendarController@deleteCalDetails');

		Route::get('notifications', ['as' =>'notifications', 'uses' =>  'App\Http\Controllers\admin\NotificationsController@index']);

		Route::get('{type?}',['as' => 'component.view', 'uses' => 'App\Http\Controllers\admin\ComponentController@index']);
		Route::get('{type?}/{document_id?}/add', ['as' => 'document.type.add', 'uses' =>  'App\Http\Controllers\admin\ComponentController@addDocuments']);
		Route::get('{type?}/{document_id?}/edit/{id?}', ['as' => 'document.type.edit', 'uses' =>  'App\Http\Controllers\admin\ComponentController@editDocuments']);
		Route::get('delete/{type?}/resources/{id?}',['as' => 'resource.delete', 'uses' =>  'App\Http\Controllers\admin\ComponentController@deleteResource']);
		Route::post('component/documents/update', ['as' => 'education.documents.save', 'uses' =>  'App\Http\Controllers\admin\ComponentController@uploadDocuments']);
		// Route::post('ajax/set_current_time_zone', array('as' => 'ajaxsetcurrenttimezone','uses' => 'App\Http\Controllers\admin\ComponentController@setCurrentTimeZone'));

	}
);
Route::post('api/login', [App\Http\Controllers\api\AuthController::class, 'authenticate']);
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('api/logout', [App\Http\Controllers\api\AuthController::class, 'logout']);
		Route::get('api/feeds/','App\Http\Controllers\api\FeedsController@index');
		Route::any('api/editFeed/{id?}','App\Http\Controllers\api\FeedsController@edit');
		Route::post('api/likeFeed','App\Http\Controllers\api\FeedsController@likeFeed');
		Route::post('api/addComment','App\Http\Controllers\api\FeedsController@addComment');
		Route::post('api/removeComment','App\Http\Controllers\api\FeedsController@removeComment');

		Route::get('api/locations/','App\Http\Controllers\api\LocationController@index');
		Route::any('api/editLocation/{id?}','App\Http\Controllers\api\LocationController@edit');

		Route::get('api/newsletters/','App\Http\Controllers\api\NewslettersController@index');
		Route::any('api/editNewsletter/{id?}','App\Http\Controllers\api\NewslettersController@edit');

		Route::any('api/attendance','App\Http\Controllers\api\CalendarController@attendance');
		Route::any('api/addevent','App\Http\Controllers\api\CalendarController@addCalDetails');
		Route::any('api/markAttendance{id?}','App\Http\Controllers\api\CalendarController@markAttendance');
		Route::get('api/calData','App\Http\Controllers\api\CalendarController@calData');

		Route::any('api/components/{type}','App\Http\Controllers\api\ComponentController@index');

		Route::get('api/chats/{search_keyword?}','App\Http\Controllers\api\CommuniqueController@chats');
		Route::get('api/getLatestMessages/{group_id}','App\Http\Controllers\api\CommuniqueController@getLatestMessages');
		Route::post('api/sendMessage','App\Http\Controllers\api\CommuniqueController@sendMessage');
		Route::get('api/profile','App\Http\Controllers\api\UsersController@index');
		Route::post('api/editProfile','App\Http\Controllers\api\UsersController@edit');

});