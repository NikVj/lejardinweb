<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\DB;
use App\Models\Calendars;
use App\Repositories\UserRepository;
use App\Repositories\CalendarsRepository;

class Calendar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user_repository,CalendarsRepository $repository)
    {
        $this->user_repository = $user_repository;
        $this->repository = $repository;
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {   $data = $this->repository->getEvents(auth()->user());

        return view('components.calendar', $data);
    }
}
