<?php

namespace App\Lib;


use Pusher\Pusher;

class PusherFactory
{
    public static function make()
    {
        return new Pusher(
            env("PUSHER_APP_KEY")!='' ? env("PUSHER_APP_KEY"):"9cfa4d42c4a33e35be86", // public key
            env("PUSHER_APP_SECRET") ? env("PUSHER_APP_SECRET") : "039c9a1d04dde49f685d", // Secret
            env("PUSHER_APP_ID") ? env("PUSHER_APP_ID") :"1287005", // App_id
            array(
                'cluster' => env("PUSHER_APP_CLUSTER")!=NULL ? env("PUSHER_APP_CLUSTER") : "ap2", // Cluster
                'encrypted' => true,
            )
        );
    }
}