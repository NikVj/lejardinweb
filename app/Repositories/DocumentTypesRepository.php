<?php

namespace App\Repositories;

use App\Models\DocumentsType;


class DocumentTypesRepository extends BaseRepository
{
    public function __construct(DocumentsType $types)
    {
        parent::__construct($types);
    }
}
