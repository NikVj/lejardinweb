<?php

namespace App\Repositories;

use App\Models\Attendance;

use Illuminate\Support\Facades\DB;

class AttendanceRepository extends BaseRepository
{
    public function __construct(Attendance $attendance)
    {
        parent::__construct($attendance);
    }
    public function saveAttendance($condition,$attendanceData)
    {
        if(!empty($condition)){
            $addAttendance = $this->updateOrCreate($condition,$attendanceData);
        }else{
            $addAttendance = $this->create($attendanceData);
        }
        return $addAttendance;
    }
    public function getAttendance($condition = array())
    {
        $data = array();
        if(isset($condition['id']) || isset($condition['date'])){
            $data['attendanceData'] = $this->fetchOne($condition);
        }else{
            $data['attendanceData'] = $this->fetchGrouBy('date',$condition);
        }
        if(!empty($data['attendanceData'])){
            if(!(isset($condition['id'])) && !(isset($condition['date']))){
                foreach($data['attendanceData'] as $attendanceDate => $attendanceArray){
                    foreach($attendanceArray as $attendanceKey => $attendanceVal){
                        $childrenData = (json_decode($attendanceVal['children'] ,true));
                        $child_array = array();
                        foreach ($childrenData as $key => $value) {
                            $child_array['child_'.$value['child']] = $value['attendance'];
                        }
                        $data['attendanceData'][$attendanceDate][$attendanceKey]['childrenData'] = $child_array;
                    }
                }
                $data['years'] = $this->getDistinct('year');
                $data['filters'] = $condition;
                $data['children'] = children();
            }else{
                $childrenData = (json_decode($data['attendanceData']['children'] ,true));
                $child_array = array();
                foreach ($childrenData as $key => $value) {
                    $child_array['child_'.$value['child']] = $value['attendance'];
                }
                $data['attendanceData']['childrenData'] = $child_array;
            }
        }
        // pr($data['attendanceData']->toArray());die;
        return $data;
    }
}
