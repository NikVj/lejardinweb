<?php

namespace App\Repositories;

use App\Models\ComponentResources;


class ComponentResourcesRepository extends BaseRepository
{
    public function __construct(ComponentResources $resources)
    {
        parent::__construct($resources);
    }


    public function getResources($id)
    {
        return $this->getModel()
            ->where('designation_id', $id)
            ->get();
    }
}
