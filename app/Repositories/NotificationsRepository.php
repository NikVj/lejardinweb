<?php

namespace App\Repositories;

use App\Models\Newsletter;
use App\Models\Notification;
use App\Mail\Newsletters;

use Illuminate\Support\Facades\Mail;

class NotificationsRepository extends BaseRepository
{
    public function __construct(Newsletter $newsletter)
    {
        parent::__construct($newsletter);
    }
    public function addNewsletter($authUser,$condition,$Data,$file='')
    {
    	$return = false;
        $newsletterSaved = $this->updateOrCreate($condition,$Data);
        if($newsletterSaved){
        	$return = true;
            if($file){
                $return = $this->uploadImageUser($authUser,$file, $newsletterSaved->id,'newsletter','link',false);
            }
            $postedby=$authUser['firstname'];
            $recievers = array();
            $recievers =array_merge(explode(',',$Data['parent']),$recievers);
            $recievers =array_merge(explode(',',$Data['staff']),$recievers);
            $notificationData['from_user']=$authUser['id'];
            $notificationData['content']=$postedby. ' sent a newsletter to you';
            $notificationData['url_component']='newsletter';
            $notificationData['section_id']=$newsletterSaved->id;
            foreach($recievers as $eachrec){
                $notificationData['to_user']=$eachrec;
                $notificationSaved = Notification::Create($notificationData);
                $recData = userData($eachrec);
                if($file != ''){
                    $data = ['TO_NAME' => $recData->firstname.' '.$recData->lastname,'TITLE' => $Data['title'],'DESCRIPTION'=> $Data['desc'],'POSTED_BY'=> $postedby,"link"=>$file];
                }else{
                    $data = ['TO_NAME' => $recData->firstname.' '.$recData->lastname,'TITLE' => $Data['title'],'DESCRIPTION'=> $Data['desc'],'POSTED_BY'=> $postedby];
                }
                Mail::to($recData->email)->send(new Newsletters($data));
            }
        }
            return $return;
    }
    public function getNewsletters($authUser)
    {
        $allNewsletter = array();
        if (!$authUser['is_admin'] == 1) {
            if ($authUser['user_role'] == 2) {
                $allNewsletter = $this->findInSet($authUser['id'],'staff')->withModel('posted_by');
            }else{
                $allNewsletter = $this->findInSet($authUser['id'],'parent')->withModel('posted_by');
            }
        }else{            
            $allNewsletter = $this->withModel('posted_by');
        }
        $newsletterCount  =  0;
        foreach($allNewsletter as $eachnewsletter){
            $recipients = array();
            if($eachnewsletter['staff']!='' && $eachnewsletter['staff']!=NULL){
                $recipients = array_merge($recipients,explode(',',$eachnewsletter['staff']));
            }
            if($eachnewsletter['parent']!='' && $eachnewsletter['parent']!=NULL){
                $recipients = array_merge($recipients,explode(',',$eachnewsletter['parent']));
            }
            if(count($recipients) != 0){
                foreach ($recipients as $recKey => $recVal) {
                    $recData = userData($recVal);
                    $recipients[$recKey] = array();
                    $recipients[$recKey]['name'] = $recData->firstname.' '.$recData->lastname;
                    $recipients[$recKey]['profile'] = $recData->profile;
                    $recipients[$recKey]['id'] = $recData->id;
                }
            }
            $allNewsletter[$newsletterCount]['recipients'] = $recipients;
            // $allNewsletter[$newsletterCount]['reciepientData'] = $reciepientData;
            $newsletterCount++;
        }
        return $allNewsletter;
    }
    public function getNewsletter($condition)
    {
        $newsletterDetails = array();
        // echo '<pre>';
        // print_r($condition);die;
        $newsletterDetails = $this->withModel('posted_by',$condition,true);
        return $newsletterDetails;
    }
}
