<?php

namespace App\Repositories;

use App\Models\Feed;
use App\Models\FeedComment;

use Illuminate\Support\Facades\DB;

class FeedsRepository extends BaseRepository
{
    public function __construct(Feed $feed)
    {
        parent::__construct($feed);
    }
    public function addFeed($authUser,$condition,$Data,$file='')
    {
        $return = false;
        $feedSaved = $this->updateOrCreate($condition,$Data);
        if($feedSaved){
            $return = true;
            if($file){
                $return = $this->uploadImageUser($authUser,$file, $feedSaved->id,'feeds','link');
            }
        }
            return $return;
    }
    public function getFeeds($authUser)
    {
        $allFeeds = array();
        if (!$authUser['is_admin'] == 1) {
            if ($authUser['user_role'] == 2) {
                $allFeeds = $this->findInSet($authUser['id'],'staff')->conditionOr('privacy','1')->conditionOr('posted_by',$authUser['id'])->sortBy('id','desc')->withModel('comments');
            }else{
                $allFeeds = $this->findInSet($authUser['id'],'parent')->conditionOr('privacy','1')->conditionOr('posted_by',$authUser['id'])->sortBy('id','desc')->withModel('comments');
            }
        }else{            
            $allFeeds = $this->sortBy('id','desc')->withModel('comments');
        }
        if(!empty($allFeeds)){
            $i=0;
            foreach($allFeeds as $feed){
                $allFeeds[$i]->posted_on = date('d F Y H:i a', strtotime($feed->created_at));
                $allFeeds[$i]->liked = in_array($authUser['id'], explode(',',$feed->liked_by)) ? 1 : 0;
                $allFeeds[$i]->likeCount = ($feed->liked_by !='' ? count(explode(',',$feed->liked_by)) : 0);
                // $allFeeds[$i]->feedComments = ->with('comments')->get();;
                $i++;
            }
        }
        return $allFeeds;
    }
    public function likeFeed($id,$authUser)
    {
        $return = false;
        $feed = Feed::find($id);
        if($feed){
            $liked_by_array = explode(',', $feed->liked_by);
            if(in_array($authUser['id'], $liked_by_array)){
                unset($liked_by_array[array_search($authUser['id'],$liked_by_array)]);//for unset like array, to unlike
                $liked_by = '';
                if(!empty($liked_by_array))
                    $liked_by = implode(',',$liked_by_array);
                $done = Feed:: where('id', $id)->update(array('liked_by'=> $liked_by));
                if($done){
                    $return = 1;
                }
            }else{
                if($feed->liked_by == ''){
                    $to_be_concat = $authUser['id'];
                }
                else{
                    $to_be_concat = ",".$authUser['id'];
                }
                $data = array(
                    'liked_by' => DB::raw('CONCAT(liked_by, "'.$to_be_concat.'")')
                );
                $done = Feed:: where('id', $id)->whereRaw(' FIND_IN_SET("'.auth()->user()['id'].'",liked_by) = 0 ')->update($data);
                if($done){
                    $return = 2;
                }
            }
            save_activities('feed', $id, 'react', $authUser['firstname'].' '.$authUser['lastname'].' '.($return==2 ? 'liked' : 'unliked').' a post',$authUser);
        }
        return $return;
    }
    public function addComment($id, $comment, $authUser)
    {
        $return = false;
        $feed = Feed::find($id);
        if($feed){
            $data = array(
                        'user_id' => $authUser['id'],
                        'feed_id' => $id,
                        'comment' => $comment,
                        'timestamp' => strtotime('now'),
                    );
            $return = FeedComment::create($data);
            save_activities('feed', $return->id, 'add', $authUser['firstname'].' '.$authUser['lastname'].'added a comment',$authUser);
            // print_r($return->toArray());die;
        }
        return $return;
    }
    public function removeComment($id, $authUser)
    {
        $return = false;
        $feedComment = FeedComment::find($id);
        if($feedComment){
            if($feedComment->user_id == $authUser['id'] || $authUser['user_role'] == 1){
                $return = FeedComment::where('id',$id)->delete();
                $return =  json_encode(['comment' => $feedComment]);
                save_activities('feed', $return->id, 'delete', $authUser['firstname'].$authUser['lastname'].'removed a comment',$authUser);
            }else{
                return 2;
            }
        }
        return $return;
    }
}
