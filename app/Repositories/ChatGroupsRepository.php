<?php

namespace App\Repositories;

use App\Models\Chat;
use App\Models\ChatGroup;

use Illuminate\Support\Facades\DB;
use App\Lib\PusherFactory;

class ChatGroupsRepository extends BaseRepository
{
    public function __construct( ChatGroup $chatgroup)
    {
        parent::__construct($chatgroup);
    }
}
