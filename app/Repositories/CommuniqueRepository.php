<?php

namespace App\Repositories;

use App\Models\Chat;
use App\Models\ChatGroup;
use App\Repositories\ChatGroupsRepository;

use Illuminate\Support\Facades\DB;
use App\Lib\PusherFactory;

class CommuniqueRepository extends BaseRepository
{
    public function __construct(Chat $chat, ChatGroupsRepository $chatgroup)
    {
        parent::__construct($chat);
        $this->chatgroup = $chatgroup;
    }
    public function saveChat($authUser,$chatData,$groupnamepost='')
    {
        $usersArray = array();
        array_push($usersArray, $chatData['from_user']);
        array_push($usersArray, $chatData['to_user']);
        $usersArray = implode(',',$usersArray);
        if($groupnamepost && $groupnamepost != ''){
            $groupName = $groupnamepost;
        }else{
            if($chatData['to_user']  > $chatData['from_user']){
                $groupName = $chatData['from_user'].'_'. $chatData['to_user'] ;
            }else{
                $groupName = $chatData['to_user'] .'_'.$chatData['from_user'];
            }
        }
        $groupData = array(
            'users'     => $usersArray,
            'group_name'       => $groupName,
            'timestamp'       => strtotime('now'),
        );
        $condition = array(
            'id'=> $chatData['group_id']
        );

        $groupSaved = $this->chatgroup->updateOrCreate($condition,$groupData);
        $chatData['group_id'] = $groupSaved['id'];


        $message = new Chat();

        $message->from_user = $chatData['from_user'];

        $message->to_user = $chatData['to_user'];

        $message->group_id = $chatData['group_id'];

        $message->content = $chatData['content'];

        $message->type = isset($chatData['type']) ? $chatData['type'] :'text';

        $message->read_by = $chatData['from_user'];

        $message->timestamp = strtotime('now');

        $message->save();

        if($chatData['file']){
            $this->uploadImageUser($authUser,$chatData['file'], $message->id, 'chats','content',false, $chatData['group_id'],true);
        }


   
        // prepare some data to send with the response
        $message->dateTimeStr = date("Y-m-dTH:i", strtotime($message->created_at->toDateTimeString()));

        $message->dateHumanReadable = $message->created_at->diffForHumans();

        $userDatafrom = userData($message->from_user);

        $userDatato=userData($message->to_user);

        $message->fromUserName = $userDatafrom->firstname.' '.$userDatafrom->lastname;

        $message->receiverName = $userDatato->firstname.' '.$userDatato->lastname;

        $message->fromUserImage = $userDatafrom->profile;

        // $message->from_user_id = $authUser['id'];

        $message->receiverImage = $userDatato->profile;

        // $message->to_user_id = (int)$chatData['to_user'];

        $message->to_user = (int)$chatData['to_user'];

        $message->group_id = $chatData['group_id'];

        $message->type = isset($message->type)? $message->type :'text';
        $NewMessage = Chat:: find($message->id)->toArray();
        $message->content = $NewMessage['content'];

        PusherFactory::make()->trigger('chat', 'send', ['data' => $message]);

        return $message;
    }
    public function getLastMessages($authUser,$search_keyword = '')
    {
        $condition = array(
            $authUser['id']
        );

        $all_groups = $this->chatgroup->fetchfindInSet($authUser['id'],'users');
        $groups = array();
        foreach($all_groups as $eachGroup ){
            array_push($groups, $eachGroup['id']);
        }
        if($search_keyword == ''){
            $data =  Chat::select('chats.*',DB::raw("CONCAT(receiver_data.firstname,receiver_data.lastname) AS receiver_name"),DB::raw("CONCAT('".url('/images/users/')."/', receiver_data.profile) as receiver_profile"),DB::raw("CONCAT(sender_data.firstname,sender_data.lastname) AS sender_name"),DB::raw("CONCAT('".url('/images/users/')."/', sender_data.profile) as sender_profile"))
                        ->whereIn('group_id',$groups)
                        ->join('users as sender_data', 'chats.from_user','sender_data.id')
                        ->join('users as receiver_data', 'chats.to_user','receiver_data.id')
                         ->orderBy('created_at', 'DESC')
                        ->get()
                        ->unique('group_id');
        }else{
            $data =  Chat::select('chats.*',DB::raw("CONCAT(receiver_data.firstname,receiver_data.lastname) AS receiver_name"),DB::raw("CONCAT('".url('/images/users/')."/', receiver_data.profile) as receiver_profile"),DB::raw("CONCAT(sender_data.firstname,sender_data.lastname) AS sender_name"),DB::raw("CONCAT('".url('/images/users/')."/', sender_data.profile) as sender_profile"))
                        ->whereIn('group_id',$groups)
                        ->join('users as sender_data', 'chats.from_user','sender_data.id')
                        ->join('users as receiver_data', 'chats.to_user','receiver_data.id')
                        ->where('receiver_data.name', 'like', '%' . $search_keyword . '%')
                        ->orderBy('created_at', 'DESC')
                        ->get()
                        ->unique('group_id');
        }
        return $data;
    }
}
