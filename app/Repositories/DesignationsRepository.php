<?php

namespace App\Repositories;

use App\Models\Designations;


class DesignationsRepository extends BaseRepository
{
    public function __construct(Designations $designation)
    {
        parent::__construct($designation);
    }
}
