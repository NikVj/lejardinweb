<?php

namespace App\Repositories;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;
use Image;
use File;

/**
 * Class EloquentBaseRepository.
 */
abstract class BaseRepository
{
    /**
     *
     */
    const ID = 'id';
    /**
     *
     */
    const SORT_ASC = 'asc';
    /**
     *
     */
    const SORT_DESC = 'desc';
    /**
     *
     */
    const PAGINATE_ITEM = 10;
    /**
     * @var Model
     */
    private $model;

    /**
     * @var Model
     */
    private $queryBuilder;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        //echo $model;die;
        $this->setModel($model);
        $this->setQueryBuilder($this->getModel());
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Model $model
     *
     * @return $this
     *
     */
    protected function setModel(Model $model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function fetchOne(array $conditions = [])
    {
        $result = $this->getQueryBuilder()->where($conditions)->first();
        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function fetch(array $conditions = [])
    {
        $result = $this->getQueryBuilder()->where($conditions)->get();
        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function fetchWhereIn($field, $conditions = [])
    {
        $result = $this->getQueryBuilder()->whereIn($field, $conditions)->get();

        $this->resetQueryBuilder();

        return $result;
    }
    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function fetchGrouBy($field,array $conditions = [])
    {
        $result = $this->getQueryBuilder()->where($conditions)->get()->groupBy($field);
        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param array $parameters
     *
     * @return static
     *
     */
    public function create(array $parameters)
    {
        return $this->getModel()->create($parameters);
    }

    /**
     * @param array $items
     *
     * @return static
     *
     */
    public function createMultiple(array $items)
    {
        return $this->getModel()->insert($items);
    }

    /**
     * [delete description].
     *
     * @method delete
     *Agarwal
     *
     * @param array $conditions [description]
     * @return mixed [type] [description]
     */
    public function delete(array $conditions = [])
    {
        $result = $this->getQueryBuilder()->where($conditions)->delete();
        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param array $conditions
     * @param array $parameters
     *
     * @return static
     *
     */
    public function updateOrCreate(array $conditions, array $parameters)
    {
        return $this->getModel()->updateOrCreate($conditions, $parameters);
    }

    /**
     * @param array $items
     * @return bool
     */
    public function updateOrCreateMultipleOnUniqueKey(array $items)
    {
        if (!empty($items)) {
            array_walk($items, function (&$val) {
                array_walk($val,function(&$val){
                    $val="'".$val."'";
                });
                return ksort($val);
            });
            $columns = implode(',', array_keys(reset($items)));

            $values=implode(',',array_map(function ($val){
                return '(' . implode(', ', $val) . ')';
            },$items));

            $updateData=implode(', ',array_map(function($val){
                return "$val =  VALUES($val)";
            },array_keys(reset($items))))   ;

            $temp = DB::statement('INSERT INTO ' . $this->getModel()->getTable() . '(' . $columns . ') VALUES '.$values.' ON DUPLICATE KEY UPDATE '.$updateData);
        }
        return true;
    }

    /**
     * @param array $conditions
     * @param array $parameters
     *
     * @return BaseRepository
     *
     */
    public function getOrCreate(array $conditions, array $parameters)
    {
        if (!is_null($instance = $this->getModel()->where($conditions)->first())) {
            return $instance;
        }

        return $this->create($parameters);
    }

    /**
     * @param array $parameters
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function update(array $parameters, array $conditions = [])
    {
        $model = $this->getQueryBuilder()->where($conditions)->update($parameters);
        $this->resetQueryBuilder();

        return $model;
    }

    /**
     * @param array $parameters
     * @param array $conditions
     * @return mixed
     */
    public function updateMultiple(array $parameters, array $conditions = [])
    {
        $model = $this->getQueryBuilder();
        if (!empty($conditions)) {
            foreach ($conditions as $field => $condition) {
                $model = $model->whereIn($field, $condition);
            }
        }
        $model = $model->update($parameters);
        $this->resetQueryBuilder();

        return $model;
    }

    /**
     * @param $id
     * @param       $relation
     * @param array $values
     * @return mixed
     * @internal param Model $model
     */
    public function syncWithRelation($id, $relation, array $values)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();

        if (empty($model)) {
            return;
        }

        return $model->$relation()->sync($values);
    }

    /**
     * @param $id
     * @param $relation
     * @param array $values
     */
    public function syncWithRelationWithoutDetaching($id, $relation, array $values)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();
        if (empty($model)) {
            return;
        }

        return $model->$relation()->syncWithoutDetaching($values);
    }


    /**
     * @param $id
     * @param $relation
     * @param array $values
     */
    public function updateExistingPivot($id, $relation, array $values , $pivotKey = null ){

        if (!empty($pivotKey))
        {
            $model = $this->getModel()->where( 'id', $pivotKey )->first();
        }else{
            $model = $this->getModel()->first();
        }

        if (empty($model)) {
            return;
        }

        return $model->$relation()->updateExistingPivot($id,$values);
    }

    /**
     * @param $id
     * @param       $relation
     * @param array $data
     * @return mixed
     * @internal param Model $model
     */
    public function createRelation($id, $relation, array $data)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();
        if (empty($model)) {
            return;
        }

        return $model->$relation()->create($data);
    }

    /**
     * @param $id
     * @param       $relation
     * @param array $data
     * @return mixed
     * @internal param Model $model
     */
    public function attachRelation($id, $relation, array $data)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();
        if (empty($model)) {
            return;
        }

        return $model->$relation()->attach($data);
    }

    /**
     * @param $id
     * @param       $relation
     * @param array $data
     * @return mixed
     */
    public function detachRelation($id, $relation, array $data)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();
        if (empty($model)) {
            return;
        }

        return $model->$relation()->detach($data);
    }

    public function associateRelation($id, $relation, Model $relationModel)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();
        if (empty($model)) {
            return;
        }

        return $model->$relation()->associate($relationModel);
    }

    public function dissociateRelation($id, $relation, Model $relationModel)
    {
        $model = $this->getModel()->where($this->model->getKeyName(), $id)->first();
        if (empty($model)) {
            return;
        }

        return $model->$relation()->dissociate($relationModel);
    }

    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function getCount(array $conditions = [])
    {
        return $this->getQueryBuilder()->where($conditions)->count();
    }


    /**
     * @return mixed
     */
    public function getQueryBuilder()
    {
            return $this->queryBuilder;
    }

    /**
     * @param $queryBuilder
     *
     * @return mixed
     *
     */
    public function setQueryBuilder($queryBuilder)
    {
        return $this->queryBuilder = $queryBuilder;
    }

    /**
     */
    private function resetQueryBuilder()
    {
        $this->setQueryBuilder($this->getModel());
    }

    /**
     * @param       $field
     * @param array $conditions
     *
     * @return $this
     *
     */
    public function conditionIn($field, array $conditions)
    {
        $this->setQueryBuilder($this->getQueryBuilder()
            ->whereIn($field, $conditions));

        return $this;
    }

    /**
     * @param       $field
     * @param array $conditions
     *
     * @return $this
     *
     */
    public function findInSet($val,$colname)
    {
        $this->setQueryBuilder($this->getQueryBuilder()
            ->whereRaw("FIND_IN_SET('".$val."',".$colname.")"));

        return $this;
    }

    /**
     * @param       $field
     * @param array $conditions
     *
     * @return $this
     *
     */
    public function withModel($set,$condition=array(),$fetchone=false)
    {
        // echo '<pre>';
        // $model = $this->getModel()->first();
        // print_r($model);die;
        if($fetchone == true){
                    $result = $this->setQueryBuilder($this->getQueryBuilder())
                        ->with($set)->where($condition)->first();
        }else{
                    $result = $this->setQueryBuilder($this->getQueryBuilder())
                        ->with($set)->get();
        }
        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function fetchfindInSet($val,$colname)
    {
        $result = $this->getQueryBuilder()->whereRaw("FIND_IN_SET('".$val."',".$colname.")")->get();

        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param        $field
     * @param string $order
     *
     * @return $this
     *a Kundu
     */
    public function sortBy($field, $order = 'asc')
    {
        $this->setQueryBuilder($this->getQueryBuilder()
            ->orderBy($field, $order));

        return $this;
    }

    public function conditionOr($column, $operator = null, $value = null)
    {
        return $this->condition($column, $operator, $value, 'or');
    }

    /**
     * @param        $column
     * @param null   $operator
     * @param null   $value
     * @param string $boolean
     *
     * @return $this
     *
     */
    public function condition($column, $operator = null, $value = null, $boolean = 'and')
    {
        $this->setQueryBuilder($this->getQueryBuilder()
            ->where($column, $operator, $value, $boolean));

        return $this;
    }

    /**
     * @param array $conditions
     *
     * @return mixed
     *
     */
    public function countRows($conditions = [])
    {
        return $this->getModel()->where($conditions)->count();
    }

    /**
     * This function is used for returning the Query
     * Model used in generating Grid in our application.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * @param $field
     *
     * @return array
     *
     */
    public function getEnum($field)
    {
        $table = $this->getModel()->getTable();
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$field'"))[0]->Type;
        preg_match('/^enum\(\'(.*)\'\)$/', $type, $matches);
        $enum = explode("','", $matches[1]);
        $enum = array_map('ucwords', array_combine($enum, $enum));

        return $enum;
    }
    public function getDistinct($field,$condition=array())
    {
        $table = $this->getModel()->getTable();
        if(empty($condition)){
            $result = DB::select(DB::raw("SELECT DISTINCT($field) as year FROM $table"));
        }else{
            $cond_str = '';
            foreach ($condition as $key => $value){
                $cond_str.=  "$field = $value OR ";
            }
            $cond_str = rtrim($cond_str, 'OR ');
            $result = DB::select(DB::raw("SELECT DISTINCT($field) FROM $table WHERE $cond_str"));
        }

        return $result;
    }

    public function increment($field, $value = 1)
    {
        $result = $this->getQueryBuilder()->increment($field, $value);
        $this->resetQueryBuilder();

        return $result;
    }

    public function decrement($field, $value = 1)
    {
        $result = $this->getQueryBuilder()->decrement($field, $value);
        $this->resetQueryBuilder();

        return $result;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function limit($value)
    {
        $this->setQueryBuilder($this->getQueryBuilder()
            ->limit($value));
        return $this;
    }

    /**
     * @return mixed
     */
    public function forceDelete()
    {
        $result = $this->getQueryBuilder()->forceDelete();
        $this->resetQueryBuilder();
        return $result;
    }

    /**
     * @return mixed
     */
    public function restore()
    {
        $result = $this->getQueryBuilder()->restore();
        $this->resetQueryBuilder();
        return $result;
    }

    /**
     * @param $relation
     * @param Closure|null $callback
     * @return $this
     */
    public function withRelation($relation)
    {
        $this->getQueryBuilder()->with($relation);
        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function columns(array $columns){
        $this->setQueryBuilder($this->getModel()->addSelect($columns));
        return $this;
    }

    /**
     * @param $relation
     * @param Closure|null $callback
     * @return $this
     */
    public function hasRelation($relation, Closure $callback = null)
    {
        $this->setQueryBuilder($this->getQueryBuilder()
            ->whereHas($relation, $callback));
        return $this;
    }

    public function multipleCondition($conditionArray)
    {
        $this->
        // setQueryBuilder($this->getQueryBuilder()
            getModel()->where(function ($query) use ($conditionArray){
                foreach($conditionArray as $condKey=>$condVal){
                    $funcName = $condVal['condition'];
                    $this->$funcName($condVal['attr1'],$condVal['attr2']);
                    // $query->where('UPPER(V_CODIGO)', 'LIKE', '%'.Str::upper($texto).'%')
                    // ->orWhere('UPPER(V_NOMBRE)', 'LIKE', '%'.Str::upper($texto).'%');
                }
            });
        // );
        return $this;
    }

    public function uploadImageUser($authUser,$file, $id, $type='users', $col_name='profile', $img_validation=true,$id_folder=0, $full_path = false)
    {
        if ( $type=='users' && ($authUser['is_admin'] != 1 && $id != $authUser['id'])) {
            return response()->json([
                'class_name'  => 'alert-danger',
                'message'     => 'Something went wrong!',
                'current'     => '',
                'first'       => ''
            ]);
        }else{

            $directoryPath = public_path().'/images/'.$type.'/';
            if ($directoryPath) {
                File::makeDirectory($directoryPath, $mode = 0777, true, true);
            }

            $findProfile = $this->getModel()->find($id)->profile;
            if (!empty($findProfile && file_exists($directoryPath.$findProfile))) {
                unlink($directoryPath.$findProfile);
            }

            $validation_passed = true;
            if($img_validation == true){
                $validation = Validator::make(array('profile'=>$file), [
                    'profile' => 'required|image|mimes:jpeg,png,jpg,gif|max:20480'
                ]);
                $validation_passed = $validation->passes();
            }

            if($validation_passed){
                $image = $file;
                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $updateField = $this->getModel()->where('id', $id)->update([$col_name => $new_name]);

                if ($updateField) {
                    if($img_validation){
                        $resize_image = Image::make($image->getRealPath())->fit(300, 300);
                        if($id_folder > 0){
                            $folderPath = 'images/'.$type.'/'.$id_folder.'/'.$new_name;
                        }
                        else{
                            $folderPath = 'images/'.$type.'/'.$new_name;
                        }
                        $uploadPath = public_path($folderPath);
                        $resize_image->save($uploadPath);
                    }else{
                        if($id_folder > 0){
                            $folderPath = 'files/'.$type.'/'.$id_folder.'/';
                        }
                        else{
                            $folderPath = 'files/'.$type.'/';
                        }
                        $uploadPath = public_path($folderPath);
                        $file->move($uploadPath,$new_name);
                    }
                    if($full_path == true){
                        $finalUploadPath = $folderPath;
                        if(!$img_validation){
                            $finalUploadPath = $finalUploadPath.$new_name;
                        }
                        $updateField = $this->getModel()->where('id', $id)->update([$col_name => $finalUploadPath]);
                    }
                    return true;
                }
            }else{
                return false;
            }
        }
    }

}
