<?php

namespace App\Repositories;

use App\Models\Calendars;
use App\Repositories\UserRepository;


class CalendarsRepository extends BaseRepository
{
    public function __construct(Calendars $calendar,UserRepository $user_repository)
    {
        $this->user_repository = $user_repository;
        parent::__construct($calendar);
    }
    public function getEvents($authUser,$userIdgiven='')
    {
        if ($userIdgiven == '' && $authUser['is_admin'] == 1) {
        	$calendarData = Calendars::join('users', 'calendars.user_id', '=', 'users.id')
        					->get(['calendars.id','calendars.event_type','calendars.start_date as start','calendars.end_date as end','calendars.title','calendars.description','calendars.parent','calendars.staff','calendars.event_type'])
        					->groupBy('event_type')
        					->toArray();
        }else{
        	if($userIdgiven == '')
        		// $userId = $authUser->id;
                $userId = $authUser['id'];
            else
                $userId = $userIdgiven;
        	if(userData($userId)->user_role == 2){
        		$colname='staff';
        	}else{
        		$colname='parent';
        	}
        	$calendarData = Calendars::join('users', 'calendars.user_id', '=', 'users.id')
        					// ->where('calendars.user_id', $userId)
        					->where(function ($query) use($userId,$colname) {
							  $query->where('calendars.user_id', $userId)
							    ->orwhereRaw("FIND_IN_SET('".$userId."',".$colname.")");
							})
        					->get(['calendars.id','calendars.event_type','calendars.start_date as start','calendars.end_date as end','calendars.title','calendars.description','calendars.parent','calendars.staff','calendars.event_type'])
        					->groupBy('event_type')
        					->toArray();
        }
        if($userIdgiven != ''){
            return $calendarData;
        }

        $data['calEventsData']=$data['appointmentsEventsData']='{}';
        foreach($calendarData as $pereventtype => $pereventData){
            foreach($pereventData as $eventKey => $eventData){
                if($eventData['parent'] != ''){
                    $Eachparent = array();
                    $AllParent = explode(',',$eventData['parent']);
                    foreach($AllParent as $keyP=>$valP){
                        $userData = userData($valP);
                        $Eachparent[$keyP]['image'] = $userData->profile;
                        $Eachparent[$keyP]['full_image_url'] = userProfile($userData->id);
                        $Eachparent[$keyP]['name'] = $userData->firstname.' '.$userData->lastname;
                        $Eachparent[$keyP]['id'] =  $userData->id;
                    }
                    $calendarData[$pereventtype][$eventKey]['parent'] = $Eachparent;
                }
                if($eventData['staff'] != ''){
                    $Eachstaff = array();
                    $AllStaff = explode(',',$eventData['staff']);
                    foreach($AllStaff as $keyS=>$valS){
                        $userData = userData($valS);
                        $Eachstaff[$keyS]['image'] = $userData->profile;
                        $Eachstaff[$keyS]['full_image_url'] = userProfile($userData->id);
                        $Eachstaff[$keyS]['name'] = $userData->firstname.' '.$userData->lastname;
                        $Eachstaff[$keyS]['id'] =  $userData->id;
                    }
                    $calendarData[$pereventtype][$eventKey]['staff'] = $Eachstaff;
                }
            }
        }
        if (!empty($calendarData[1])) { $data['calEventsData']   = json_encode($calendarData[1]); }
        if (!empty($calendarData[2])) { $data['appointmentsEventsData']  = json_encode($calendarData[2]); }
        $data['allparents'] = $this->user_repository->getUsers('','3');
        $data['allstaff'] = $this->user_repository->getUsers('','2');
        $data['children'] = children();
        return $data;
    }
}
