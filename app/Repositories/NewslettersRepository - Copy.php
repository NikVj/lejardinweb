<?php

namespace App\Repositories;

use App\Models\Newsletter;
use App\Mail\Newsletters;

use Illuminate\Support\Facades\Mail;

class NewslettersRepository extends BaseRepository
{
    public function __construct(Newsletter $newsletter)
    {
        parent::__construct($newsletter);
    }
    public function addNewsletter($authUser,$condition,$Data,$file='')
    {
    	$return = false;
        $newsletterSaved = $this->updateOrCreate($condition,$Data);
        if($newsletterSaved){
        	$return = true;
            if($file){
                $return = $this->uploadImageUser($authUser,$file, $newsletterSaved->id,'newsletter','link',false);
            }
            $postedby=$authUser['name'];
            $recievers = array();
            $recievers =array_merge(explode(',',$Data['parent']),$recievers);
            $recievers =array_merge(explode(',',$Data['staff']),$recievers);
            foreach($recievers as $eachrec){
                $recData = userData($eachrec);
                if($file != ''){
                    $data = ['TO_NAME' => $recData->firstname.' '.$recData->latname,'TITLE' => $Data['title'],'DESCRIPTION'=> $Data['desc'],'POSTED_BY'=> $postedby,"link"=>$file];
                }else{
                    $data = ['TO_NAME' => $recData->firstname.' '.$recData->lastname,'TITLE' => $Data['title'],'DESCRIPTION'=> $Data['desc'],'POSTED_BY'=> $postedby];
                }
                Mail::to($recData->email)->send(new Newsletters($data));
            }
        }
            return $return;
    }
    public function getNewsletters($authUser)
    {
        $allNewsletter = array();
        if (!$authUser['is_admin'] == 1) {
            if ($authUser['user_role'] == 2) {
                $allNewsletter = $this->findInSet($authUser['id'],'staff')->fetch();
            }else{
                $allNewsletter = $this->findInSet($authUser['id'],'parent')->fetch();
            }
        }else{            
            $allNewsletter = $this->fetch();
        }
        $newsletterCount  =  0;
        foreach($allNewsletter as $eachnewsletter){
            $recipients = array();
            if($eachnewsletter['staff']!='' && $eachnewsletter['staff']!=NULL){
                $recipients = array_merge($recipients,explode(',',$eachnewsletter['staff']));
            }
            if($eachnewsletter['parent']!='' && $eachnewsletter['parent']!=NULL){
                $recipients = array_merge($recipients,explode(',',$eachnewsletter['parent']));
            }
            if(count($recipients) != 0){
                foreach ($recipients as $recKey => $recVal) {
                    $recData = userData($recVal);
                    $recipients[$recKey] = array();
                    $recipients[$recKey]['name'] = $recData->firstname.' '.$recData->lastname;
                    $recipients[$recKey]['profile'] = $recData->profile;
                    $recipients[$recKey]['id'] = $recData->id;
                }
            }
            $allNewsletter[$newsletterCount]['recipients'] = $recipients;
            // $allNewsletter[$newsletterCount]['reciepientData'] = $reciepientData;
            $newsletterCount++;
        }
        return $allNewsletter;
    }
}
