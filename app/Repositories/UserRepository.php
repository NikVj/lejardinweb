<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Profile;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\DB;
use Validator;
use Image;
use File;

class UserRepository extends BaseRepository
{
    public function __construct(User $User)
    {
        parent::__construct($User);
    }

    public function getUsers($des_id='',$type='',$condition=[],$search_keyword='')
    {
        $getQuery = DB::table('users')
        ->select('users.*',DB::raw("(CASE WHEN users.profile IS NOT NULL THEN CONCAT('".url('/images/users/')."/', users.profile) ELSE '".url('/images/user.png')."' END) AS profile_image"),
            // "CONCAT('".url('/images/user.png')."/', users.profile) as profile_image"),
        'user_details.status','user_details.phone_home','user_details.city','user_details.state','user_details.country','user_details.zip','user_details.address1','user_details.address2','user_details.twitter_acc','user_details.linkedin_acc','user_details.facebook_acc','user_details.outlook_acc','user_details.instagram_acc','user_details.slack_acc','user_details.education','user_details.work','user_details.website','user_details.skill','designations.designation_name as designation_name')
        ->join('user_details','user_details.user_id','=','users.id')
        ->leftjoin('designations','designations.id','=','users.designation')
        ->wherenull('users.deleted_at');
        if($des_id != ''){
            $getQuery->where('users.designation',$des_id); // to get data according to the designation id
        }
        if($type != ''){
            $getQuery->where('users.user_role',$type); // to get data according to the designation id
        }
        if($search_keyword != ''){
            $getQuery->where('name', 'like', '%' . $search_keyword . '%'); // to get data according to the designation id
        }
        if(!empty($condition)){
            $getQuery->whereNotIn('users.id',$condition);
            $getQuery->orderBy('users.created_at');
        }
        $allUsers = $getQuery->get()->toArray();
        return $allUsers;
    }

    public function saveusers($authUser,$request)
    {
          
        if (!$authUser['is_admin'] == 1) {
            $message = 'Access Denied!';
            return redirect('staff')->withErrors($message);
        }

        $request->validate([
            'user_role' => 'required',
            'firstname'        => 'required',
            'lastname'        => 'required',
            'email'       => 'required|unique:users,email',
            'phone'       => 'required',
            'address1'    => 'required',
            'password'    => 'min:8|required_with:confirm_password|same:confirm_password',
        ]);

        $hashedPassword = Hash::make($request->post('password'));
        unset($request['password']);
        $request['password'] = $hashedPassword;

        $addUser = User::create($request->all());

        if ($addUser) {
            $userDetailsData = array(
                'user_id'     => $addUser->id,
                'other_acc'   => request('other_acc'),
                'phone_home'  => request('phone_home'),
                'address1'    => request('address1'),
                'address2'    => request('address2'),
                'status'      => 1,
            );;
            DB::table('user_details')->insert($userDetailsData);
            if($request->file('profile')){
                $this->uploadImageUser($authUser,$request->file('profile'), $addUser->id);
            }
            if(request('user_role') != 1){
                if(request('user_role') == 3){
                    $childarray = request('child');
                }else{
                    $childarray = request('staffchild');
                }
                if(is_array($childarray)){
                    foreach ($childarray as $key => $value) { 
                        if(request('user_role') == 3){
                            if($value['firstname'] == '' || $value['email'] == ''){
                                continue;
                            }
                            $rel_type = 'parent';
                            $addchild = Children::create($value); 
                            $rel_id = $addchild->id;
                        }else{
                            $rel_id = $value;
                            $rel_type = 'staff';
                        }

                        $childData = array(
                            'rel_to'     => $rel_id,
                            'rel_type'   => $rel_type,
                            'rel_from'  => $addUser->id,
                        );
                        ChildRelation::create($childData);  
                    } 
                }else{
                    $msg_childarray_error = 'You need to enter correct child infomation!!';
                        return redirect('users')->withErrors($msg_childarray_error);
                }
            }
        }else{
            return false;
        }

        return true;
    }

    public function updateusers($authUser,$request,$api=false)
    {
          
        if($api == true){
            $userId = $authUser['id'];
            $userroleentered = $authUser['user_role'];
        }else{
            $userId = $request->all()['user_id'];
            $userroleentered = $request->post('role');
        }
        if ($api == false && $authUser['is_admin'] != 1 && $authUser['id'] != $userId) {
            $message = 'Access Denied!';
            return redirect('staff')->withErrors($message);
        }
        if($request->file('profile')){
            $this->uploadImageUser($authUser,$request->file('profile'), $userId);
        }
        if($api == false){
            $request->validate([
                'firstname'        => 'required',
                'lastname'        => 'required',
                'email'       => 'required|email|max:225|'. Rule::unique('users')->ignore($userId),
                'phone'       => 'required',
                'address1'    => 'required',
                'role'    => 'required',
            ]);
        }

        if (!empty($request->post('password'))) {
            $data = array(
                'password' => $request->post('password'),
                'confirm_password' => $request->post('confirm_password'),
            );
            $validator = Validator::make($data, [
                'password'    => 'min:8|required_with:confirm_password|same:confirm_password',
            ]);

            if ($validator->fails()) {
                if($api == true){
                    return json_encode(['error' => $validator->errors()->first()]);
                } else{
                    return redirect('users')->withErrors($validator->messages());
                }

            }

            $hashedPassword = Hash::make($request->post('password'));
        }else{
            $hashedPassword = User::where('id',$userId)->first()->password;
        }

        $data = array(
            'designation' => $request->post('designation'),
            'firstname'        => $request->post('firstname') ? $request->post('firstname') : $authUser['firstname'],
            'lastname'        => $request->post('lastname') ? $request->post('lastname') : $authUser['lastname'],
            'email'       => $request->post('email') ? $request->post('email') : $authUser['email'],
            'password'    => $hashedPassword,
            'phone'       => $request->post('phone') ? $request->post('phone') : $authUser['phone']
        );

        $update = User::wherenull('deleted_at')->where('id',$userId)->first();
        $update->update($data);

        if ($update) {
            $userDetailsData = array(
                'other_acc'   => $request->post('other_acc'),
                'phone_home'  => $request->post('phone_home'),
                'address1'    => $request->post('address1'),
                'address2'    => $request->post('address2')
            );

            DB::table('user_details')->where('id',$update->id)->update($userDetailsData);
            $chilids = array();
            if($userroleentered != 1){
                if($userroleentered == 3){
                    if($api == true){
                        $childarray = json_decode($request->post('child'),true);
                        $totalChild = count($childarray);
                        for($i = $totalChild; $i > 0;  $i--){
                            $childarray[$i] =$childarray[$i-1];
                        }
                        unset($childarray[0]);
                    }
                    else{
                        $childarray = $request->post('child');
                    }
                }else{
                    if($api == true){
                        $child_string = str_replace('"', '', $request->post('staffchild'));
                        $childarray = $chilids = explode(',', $child_string);
                    }
                    else{
                        $childarray = $chilids = $request->post('staffchild');
                    }
                }
                if(is_array($childarray)){
                    foreach ($childarray as $key => $value) { 
                        if($userroleentered == 3){
                            $rel_type = 'parent';
                            if($value['firstname'] == '' || $value['email'] == ''){
                                continue;
                            }
                            if(isset($value['id']) && (($value['id']) == '' || ($value['id']) == 0)){
                                unset($value['id']);
                                $addchild = Children::create($value);
                            }else{
                                $addchild = Children::where('id',$value['id'])->first();
                                $addchild->update($value);
                            }
                            $rel_id = $addchild->id;
                            array_push($chilids,$rel_id);
                        }else{
                            $rel_id = $value;
                            $rel_type = 'staff';
                        }

                        $childData = array(
                            'rel_to'     => $rel_id,
                            'rel_type'   => $rel_type,
                            'rel_from'  => $update->id,
                        );
                        $childreltblget = ChildRelation::where('rel_to', $rel_id)->where('rel_from', $update->id)->where('rel_type', $rel_type)->first(); 
                        if ($childreltblget == null) {
                            $addchild = ChildRelation::create($childData);
                        }else{
                            if($childreltblget->status == '0'){
                                $childreltblget->update(array('status'=>'1'));
                            }
                        }
                    } 

                    ChildRelation::whereNotIn('rel_to', $chilids)->where('rel_from', $update->id)->update(array('status'=>'0'));
                }else{
                    $msg_childarray_error = 'You need to enter correct child infomation!!';
                    if($api == true){
                        return json_encode(['error' => $msg_childarray_error]);
                    } else{
                        return redirect('users')->withErrors($msg_childarray_error);
                    }
                }
            }
        }else{
            return false;
        }

        return true;
    }

    public function updatePassword($authUser,$request,$api=false)
    {

        if (!empty($request->post('password'))) {
            $data = array(
                'password' => $request->post('password'),
                'confirm_password' => $request->post('confirm_password'),
            );
            $validator = Validator::make($data, [
                'password'    => 'min:8|required_with:confirm_password|same:confirm_password',
            ]);

            if ($validator->fails()) {
                return json_encode(['error' => $validator->errors()->first()]);
            }

            $hashedPassword = Hash::make($request->post('password'));
        }else{
            $hashedPassword = User::where('id',$authUser['id'])->first()->password;
        }

        $data = array(
            'password'    => $hashedPassword,
        );

        $update = User::wherenull('deleted_at')->where('id',$authUser['id'])->first();
        $update->update($data);

        if ($update) {
            return true;
        }else{
            return false;
        }

    }


    public function getUserDetails($id)
    {
            $userDetails = User::select('users.*','user_details.*')
            ->join('user_details','user_details.user_id','=','users.id')
            ->where('users.id', $id)
            ->get()->first();
            return $userDetails;
    }
/*
    function uploadImageUser($authUser,$file, $id)
    {
        if ($authUser['is_admin'] != 1 && $id != $authUser['id']) {
            return response()->json([
                'class_name'  => 'alert-danger',
                'message'     => 'Something went wrong!',
                'current'     => '',
                'first'       => ''
            ]);
        }else{

            $directoryPath = public_path().'/images/users/';
            if ($directoryPath) {
                File::makeDirectory($directoryPath, $mode = 0777, true, true);
            }

            $findProfile = User::find($id)->profile;
            if (!empty($findProfile && file_exists($directoryPath.$findProfile))) {
                unlink($directoryPath.$findProfile);
            }

            $validation = Validator::make(array('profile'=>$file), [
                'profile' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
            ]);

            if($validation->passes()){
                $image = $file;
                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $updateField = User::where('id', $id)->update(['profile' => $new_name]);

                if ($updateField) {
                    $resize_image = Image::make($image->getRealPath())->fit(300, 300);
                    $resize_image->save(public_path('images/users/'.$new_name));

                    return response()->json([
                        'class_name'  => 'alert-success',
                        'message'     => 'Image Upload Successfully!',
                        'imagePath'   => '/images/users/'.$new_name
                    ]);
                }
            }else{
                return response()->json([
                    'class_name'  => 'alert-danger',
                    'message'     => $validation->errors()->all(),
                    'imagePath'   => ''
                ]);
            }
        }
    }
*/    
}
