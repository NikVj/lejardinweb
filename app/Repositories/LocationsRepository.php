<?php

namespace App\Repositories;

use App\Models\Location;


class LocationsRepository extends BaseRepository
{
    public function __construct(Location $location)
    {
        parent::__construct($location);
    }
}
