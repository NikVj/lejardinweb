<?php

namespace App\Repositories;

use App\Models\ActivityLogs;


class ActivityLogsRepository extends BaseRepository
{
    public function __construct(ActivityLogs $logs)
    {
        parent::__construct($logs);
    }

    public function getLogs()
    {
        return $this->getModel()
            ->with('user')
            ->orderBy('created_at','desc')
            // ->get()
            ->paginate(20);
    }

    public function AddLogs($desc)
    {
        $data = $this->setLogData($desc);
        $this->create($data);
        return true;
    }

    private function setLogData($desc)
    {
        $data = [
            'user_id' => auth()->user()['id'],
            'description' => $desc,
            'timestamp' => strtotime("now")
        ];
        return $data; 

    } 
}
