<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Image;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.show');
    }

    function profileForm($id)
    {
        return view('profile.profile_form');
    }

    function profileSkillsForm($id)
    {
        return view('profile.skills_form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $userId = $request->all()['userid'];

        if (auth()->user()['is_admin'] != 1 && auth()->user()['id'] != $userId) {
            $message = 'Access Denied!';
            return redirect('profile')->withErrors($message);
        }

        $request->validate([
            'designation' => 'required',
            'name'        => 'required',
            'phone'       => 'required',
            'address1'    => 'required'
        ]);

        $data = array(
            'git_acc'       => request('git_acc'),
            'twitter_acc'   => request('twitter_acc'),
            'linkedin_acc'  => request('linkedin_acc'),
            'facebook_acc'  => request('facebook_acc'),
            'outlook_acc'   => request('outlook_acc'),
            'instagram_acc' => request('instagram_acc'),
            'slack_acc'     => request('slack_acc'),
            'my_acc'        => request('my_acc'),
            'aadhar_no'     => request('aadhar_no'),
            'pan_no'        => request('pan_no'),
            'website'       => request('website'),
            'other_acc'     => request('other_acc'),
            'phone_home'    => request('phone_home'),
            'city'          => request('city'),
            'state'         => request('state'),
            'country'       => request('country'),
            'zip'           => request('zip'),
            'address1'      => request('address1'),
            'address2'      => request('address2'),
        );

        $update = Profile::where('user_id', $userId)->first();
        $update->update($data);

        if ($update) {
            $usersData = array(
                'designation' => $request->post('designation'),
                'name'        => $request->post('name'),
                'phone'       => $request->post('phone')
            );

            DB::table('users')->where('id',$update->id)->update($usersData);
        }else{
            $message = 'Something went wrong!';
            return redirect('profile')->withErrors($message);
        }

        $message = 'Staff as "'.$request->post('designation').'" updated successfully!';
        return redirect('profile')->withSuccess($message);
    }

    public function updateUserSkills(Request $request)
    {
        $userId = $request->all()['userid'];

        if (auth()->user()['is_admin'] != 1 && auth()->user()['id'] != $userId) {
            $message = 'Access Denied!';
            return redirect('profile')->withErrors($message);
        }

        $finalSkillJson = NULL;
        $finalEduJson = NULL;
        $finalWorkJson = NULL;

        $inputSkills      = $request->all()['skill_name'];
        $inputSkillsnum   = $request->all()['skill_number'];
        $inputSkillsbg    = $request->all()['skill_bgcolor'];

        foreach($inputSkills as $key => $link) 
        { 
            if(empty($link)){ 
                unset($inputSkills[$key]);
                unset($inputSkillsnum[$key]);
                unset($inputSkillsbg[$key]);
            } 
        }

        $arrSkillCount = count($inputSkills);

        if ($arrSkillCount >= 1) {
            for ($i=0; $i < $arrSkillCount; $i++) { 

                $skillNameInd = array_values($inputSkills)[$i];
                $skillNoInd   = array_values($inputSkillsnum)[$i];
                $skillBgInd   = array_values($inputSkillsbg)[$i];

                $finalSkillArr[] = array(
                    'name' => $skillNameInd,
                    'number' => $skillNoInd,
                    'bgcolor' => $skillBgInd,
                );
            }
            $finalSkillJson = json_encode($finalSkillArr);
        }
        // pr($finalSkillJson);

        $inputEdu         = $request->all()['edu_name'];
        $inputEduUni      = $request->all()['edu_university'];
        $inputEduFrom     = $request->all()['edu_from'];
        $inputEduTo       = $request->all()['edu_to'];

        foreach($inputEdu as $key => $link) 
        { 
            if(empty($link)){ 
                unset($inputEdu[$key]);
                unset($inputEduUni[$key]);
                unset($inputEduFrom[$key]);
                unset($inputEduTo[$key]);
            } 
        }

        $arrEduCount = count($inputEdu);
        if ($arrEduCount >= 1) {
            for ($i=0; $i < $arrEduCount; $i++) { 
                $eduNameInd   = array_values($inputEdu)[$i];
                $eduUniInd    = array_values($inputEduUni)[$i];
                $eduFromInd   = array_values($inputEduFrom)[$i];
                $eduToInd     = array_values($inputEduTo)[$i];

                $finalEduArr[] = array(
                    'name' => $eduNameInd,
                    'university' => $eduUniInd,
                    'from' => $eduFromInd,
                    'to' => $eduToInd,
                );
            }
            $finalEduJson = json_encode($finalEduArr);
        }
        // pr($finalEduJson);
        // die;


        $inputWork         = $request->all()['work_organization'];
        $inputWorkDes      = $request->all()['work_designation'];
        $inputWorkFrom     = $request->all()['work_from'];
        $inputWorkTo       = $request->all()['work_to'];
        $inputWorkWeb      = $request->all()['work_website'];

        foreach($inputWork as $key => $link) 
        { 
            if(empty($link)){ 
                unset($inputWork[$key]);
                unset($inputWorkDes[$key]);
                unset($inputWorkFrom[$key]);
                unset($inputWorkTo[$key]);
                unset($inputWorkWeb[$key]);
            } 
        }

        $arrWorkCount = count($inputWork);
        if ($arrWorkCount > 1) {
            for ($i=0; $i < $arrWorkCount; $i++) { 
                $workOrgInd     = array_values($inputWork)[$i];
                $workDesigInd   = array_values($inputWorkDes)[$i];
                $workFromInd    = array_values($inputWorkFrom)[$i];
                $workToInd      = array_values($inputWorkTo)[$i];
                $workWebInd     = array_values($inputWorkWeb)[$i];

                $finalWorkArr[] = array(
                    'organization'    => $workOrgInd,
                    'designation'  => $workDesigInd,
                    'from' => $workFromInd,
                    'to' => $workToInd,
                    'website' => $workWebInd,
                );
            }
            $finalWorkJson = json_encode($finalWorkArr);
        }
        // pr($finalWorkJson);

        // die;


        $finalData = array(
            'work'      => $finalWorkJson,
            'education' => $finalEduJson,
            'skill'     => $finalSkillJson,
        );


        $update = Profile::where('user_id', $userId)->first();
        $update->update($finalData);

        if ($update) {
            $message = 'Skill Sets, Education and Work experience for "'.$request->post('designation').'" updated successfully!';
            return redirect('profile')->withSuccess($message);
        }else{
            $message = 'Something went wrong!';
            return redirect('profile')->withErrors($message);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
