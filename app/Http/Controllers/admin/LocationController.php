<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Location;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\LocationsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Validator;
use Image;
use File;
use Illuminate\Support\Facades\View;

class LocationController extends Controller
{
    public function __construct(LocationsRepository $repository)
    {
        $this->repository = $repository;
        View::share('locations.index', ['allLocation']);
    }

    public function index()
    {
        $allLocation = $this->repository->fetch();
        return view('locations.index', compact('allLocation'));
    }

    public function test()
    {
        $UserRepo = App::make(LocationsRepository::class);
        $user = $UserRepo->condition('id', 1)->fetchOne();
        // dd($user);

    }

    public function edit($id='', Request $request)
    {
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'name' => $request->post('name'),
                'place' => $request->post('place'),
                'lat' => $request->post('lat'),
                'long' => $request->post('long'),
            );
            $locSaved = $this->repository->updateOrCreate($condition,$Data);
            if($locSaved){
                $message = 'Location Saved Succesfully!!';
                return redirect('locations')->withSuccess($message);
            }else{
                return redirect()->back()->withErrors($message);
            }
        }
        $locDetails = array();
        if($id != '')
            $locDetails = $this->repository->fetchOne($condition);
        return view('locations.location_save', compact('locDetails'));
        // dd($user);

    }

    public function delete($id='')
    {
        if($id==''){
            $message = 'Please enter ID of the item to be deleted';
            return redirect('locations')->withErrors($message);
        }
        $message = 'Something went wrong!!';
        $condition = array('id'=>$id);
        $deleted = $this->repository->delete($condition);
        if($deleted){
            $message = 'Location deleted Succesfully!!';
            return redirect('locations')->withSuccess($message);
        }
        return redirect('locations')->withErrors($message);
    }
}