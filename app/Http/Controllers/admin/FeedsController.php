<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Feed;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\FeedsRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Validator;
use Image;
use File;
use Illuminate\Support\Facades\View;

class FeedsController extends Controller
{
    public function __construct(FeedsRepository $repository, UserRepository $user_repository)
    {
        $this->repository = $repository;
        $this->user_repository = $user_repository;
        View::share('feeds.index', ['allFeed']);
    }

    public function index()
    {
        $allFeeds = $this->repository->getFeeds(auth()->user());
        return view('feeds.index', compact('allFeeds'));
    }

    public function test()
    {
        $UserRepo = App::make(FeedsRepository::class);
        $user = $UserRepo->condition('id', 1)->fetchOne();
        // dd($user);

    }

    public function edit($id='', Request $request)
    {
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'title' => $request->post('title'),
                'desc' => $request->post('desc'),
                'privacy' => $request->post('privacy'),
                'posted_by' => auth()->user()['id'],
                'staff' => ($request->post('privacy') == 0) ? (($request->post('staff') != '')?implode(',',$request->post('staff')):'') :'',
                'parent' => ($request->post('privacy') == 0) ? (($request->post('parent') != '')?implode(',',$request->post('parent')):'') :'',
            );

            $feedSaved = $this->repository->addFeed(auth()->user(),$condition,$Data,$request->file('link'));
            if($feedSaved){
                $message = 'Feed Saved Succesfully!!';
                return redirect('feeds')->withSuccess($message);
            }else{
                return redirect()->back()->withErrors($message);
            }
        }
        $feedDetails = array();
        if($id != '')
            $feedDetails = $this->repository->fetchOne($condition);

            $allparents = $this->user_repository->getUsers('','3');
            $allstaff = $this->user_repository->getUsers('','2');
        
        return view('feeds.feed_save', compact('feedDetails','allparents','allstaff'));
        // dd($user);

    }

    public function delete($id='')
    {
        if($id==''){
            $message = 'Please enter ID of the item to be deleted';
            return redirect('feeds')->withErrors($message);
        }
        $message = 'Something went wrong!!';
        $condition = array('id'=>$id);
        $deleted = $this->repository->delete($condition);
        if($deleted){
            $message = 'Feed deleted Succesfully!!';
            return redirect('feeds')->withSuccess($message);
        }
        return redirect('feeds')->withErrors($message);
    }

    public function likeFeed(Request $request)
    {
        $status = $liked = false;
        $message = 'Something went wrong!!';
        if($request->id != ''){
            $liked = $this->repository->likeFeed($request->id, auth()->user());
            if($liked){
                $message = 'Feed '. (($liked == 1) ? 'unliked' : 'liked').' Succesfully!!';
                $status = true;
            }
        }
        return json_encode(
            array(
                'status' => $status,
                'message' => $message,
                'like' => $liked,
            )
        );
    }
    public function addComment(Request $request)
    {
        $status = $added = false;
        $message = 'Something went wrong!!';
        if($request->id != ''){
            $added = $this->repository->addComment($request->id, $request->comment, auth()->user());
            if($added){
                $message = 'Comment Added Succesfully!!';
                $status = true;
            }
        }
        return json_encode(
            array(
                'status' => $status,
                'message' => $message,
                'comment' => $added,
            )
        );
    }
    public function removeComment(Request $request)
    {
        $status = $added = false;
        $message = 'Something went wrong!!';
        if($request->id != ''){
            $removed = $this->repository->removeComment($request->id, auth()->user());
            if($removed){
                $message = 'Comment removed Succesfully!!';
                $status = true;
            }
        }
        return json_encode(
            array(
                'status' => $status,
                'message' => $message,
                'comment' => $added,
            )
        );
    }
}