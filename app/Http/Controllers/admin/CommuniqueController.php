<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat;
use App\Models\ChatGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Repositories\CommuniqueRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\View;

use App\Lib\PusherFactory;
class CommuniqueController extends Controller
{
    public function __construct( CommuniqueRepository $repository, UserRepository $user_repository)
    {
        $this->repository = $repository;
        $this->user_repository = $user_repository;
    }

	// 2021-03-08T08:30:00
	// 2021-03-08T13:00:00

	public function chat(Request $request)
	{
        $userIDs = array();
		$users = $this->repository->getLastMessages(auth()->user());
        foreach($users as $key => $user){
            $users[$key]['unreadCount'] = Chat::select(DB::raw("COUNT('read_by') as unreadCount") )->whereRaw(' FIND_IN_SET("'.auth()->user()['id'].'",read_by) = 0 ')->where('group_id',$users[$key]->group_id)->get()[0]['unreadCount'];
            if($user['from_user'] == auth()->user()['id']){
                array_push($userIDs,$user['to_user']);
            }
            else{
                array_push($userIDs,$user['from_user']);
            }
        }
            array_push($userIDs,auth()->user()['id']);
        $allusers = $this->user_repository->getUsers('','',$userIDs);
		return view('communique.chat',compact('users','allusers'));
	}

    public function getLoadLatestMessages(Request $request)
    {
		$users = $this->user_repository->getUsers();
        if(!$request->user_id) {
            return;
        }
        if($request->group_id != '' && $request->group_id != 0){
            $data = array(
                'read_by' => DB::raw('CONCAT(read_by, ",'.auth()->user()['id'].'")')
            );
            Chat:: where('group_id', $request->group_id)->whereRaw(' FIND_IN_SET("'.auth()->user()['id'].'",read_by) = 0 ')->update($data);
        }

        $chatMessage = Chat::select('chats.*',DB::raw("CONCAT(sender.firstname,sender.lastname) AS fromUserName"),DB::raw("CONCAT(receiver.firstname,receiver.lastname) AS receiverName"),'sender.profile as fromUserImage','receiver.profile as receiverImage')->where(function($query) use ($request) {
            $query->where('from_user', auth()->user()['id'])->where('to_user', $request->user_id);
        })->orWhere(function ($query) use ($request) {
            $query->where('from_user', $request->user_id)->where('to_user', auth()->user()['id']);
        })->join('users as sender','sender.id','from_user')->join('users as receiver','receiver.id','to_user')->orderBy('id','DESC')->latest()->take(10)->get();
        $messages = $chatMessage->reverse()->values();
        $return = [];
        foreach ($messages as $message) {
            $return[] = view('communique/message-line')->with(array('message'=> $message , 'users'=> $users))->render();
        }
        return response()->json(['state' => 1, 'messages' => $return]);
    }
/**
     * postSendMessage
     *
     * @param Request $request
     */
    public function postSendMessage(Request $request)
    {
        if(!$request->to_user || (!$request->message && !$request->file('file'))) {
            return;
        }
        $chatData = array(
            'from_user'     => auth()->user()['id'],
            'to_user'       => $request->to_user,
            'group_id'       => $request->group_id,
            'content'       => $request->message ? $request->message : '',
            'file'       => $request->file('file'),
            'type'       => (($request->type && $request->type != '') ? $request->type : 'text') ,
            'timestamp'       => strtotime('now'),
        );
        $message = $this->repository->saveChat(auth()->user(),$chatData, $request->post('group_name'));

        

        return response()->json(['state' => 1, 'data' => $message]);
    }
    /**
     * getOldMessages
     *
     * we will fetch the old messages using the last sent id from the request
     * by querying the created at date
     *
     * @param Request $request
     */
    public function getOldMessages(Request $request)
    {
        if(!$request->old_message_id || !$request->to_user)
            return;

        $message = Chat::find($request->old_message_id);

        $lastMessages = Chat::select('chats.*',DB::raw("CONCAT(sender.firstname,sender.lastname) AS fromUserName"),DB::raw("CONCAT(receiver.firstname,receiver.lastname) AS receiverName"),'sender.profile as fromUserImage','receiver.profile as receiverImage')->where(function($query) use ($request, $message) {
            $query->where('from_user', auth()->user()['id'])
                ->where('to_user', $request->to_user)
                ->where('timestamp', '<', $message->timestamp);
        })
            ->orWhere(function ($query) use ($request, $message) {
            $query->where('from_user', $request->to_user)
                ->where('to_user', auth()->user()['id'])
                ->where('timestamp', '<', $message->timestamp);
        })
            ->join('users as sender','sender.id','from_user')->join('users as receiver','receiver.id','to_user')->orderBy('id','DESC')->latest()->take(10)->get();
        // $lastMessages = $chatlastMessages->reverse()->values();

        $return = [];

        if($lastMessages->count() > 0) {

            foreach ($lastMessages as $message) {

                $return[] = view('communique/message-line')->with('message', $message)->render();
            }

            // PusherFactory::make()->trigger('chat', 'oldMsgs', ['to_user' => $request->to_user, 'data' => $return]);
        }

        return response()->json(['state' => 1, 'data' => $return]);
    }
}