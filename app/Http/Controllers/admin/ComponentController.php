<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\DesignationsRepository;
use App\Repositories\ComponentResourcesRepository;
use App\Repositories\DocumentTypesRepository;
use App\Repositories\ActivityLogsRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Image;
use File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;
use App\Models\ActivityLogs;
use App\Models\ChildRelation;
use App\Models\Calendars;

class ComponentController extends Controller
{

    public function __construct(DocumentTypesRepository $documentTypes, DesignationsRepository $component , ComponentResourcesRepository $resources , ActivityLogsRepository $logs)
    {
        $this->documentTypes = $documentTypes;
        $this->component = $component;
        $this->resources = $resources;
        $this->logs = $logs;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /*
     component represent designation 
     */
    public function index(Request $request,$type)
    {
       
        $allUsers = [];
        
        $logs = [];
        
        $conferences = [];
        
        $paginate = 10;

        $component = $this->component->fetchOne(['code'=>$type]);
       
        if(empty($component))
        {
            $message = 'Component Not Found';
            return redirect(route('home'))->withErrors($message);
        }
        
        $UserRepo = App::make(UserRepository::class);
        $allUsers = $UserRepo->getUsers($component->id);
        
        
        $documentTypes = $this->documentTypes->fetch();
       
        
        $resources = $this->resources->getResources( $component->id);

        if($type == 'education'){
            $conferencesQuery = Calendars::where('event_name','=','10');
            if(auth()->user()['user_role'] == 3){
                // $children = ChildRelation::where('rel_type','parent')->where('rel_from',auth()->user()['id'])->where('status',1)->get('rel_to');
                // echo '<pre>';print_r($childrenArray);die;
               $conferencesQuery->whereRaw("find_in_set(".auth()->user()['id'].",parent)");
            }else if(auth()->user()['user_role'] == 2){
                $conferencesQuery->whereRaw("find_in_set(".auth()->user()['id'].",staff)");
            }
            $conferences = $conferencesQuery->get()->toArray();
            $conferenceCount  =  0;
            foreach($conferences as $conference){
                $recipients = array();
                if($conference['staff']!='' && $conference['staff']!=NULL){
                    $recipients = explode(',',$conference['staff']);
                    foreach ($recipients as $recKey => $recVal) {
                        $recData = userData($recVal);
                        $thisStaff = array();
                        $thisStaff['name'] = $recData->firstname.' '.$recData->lastname;
                        $thisStaff['profile'] = $recData->profile;
                        $thisStaff['id'] = $recData->id;
                        $conferences[$conferenceCount]['staff']  = [];
                        // array_push($conference['staff'],$thisStaff);
                        // echo '<pre>';
                        // print_r($conference);
                        $conferences[$conferenceCount]['staff'][$recKey] = $thisStaff;
                    }
                }
                if($conference['parent']!='' && $conference['parent']!=NULL){
                    $recipients = explode(',',$conference['parent']);
                    foreach ($recipients as $recKey => $recVal) {
                        $recData = userData($recVal);
                        $thisParent = array();
                        $thisParent['name'] = $recData->firstname.' '.$recData->lastname;
                        $thisParent['profile'] = $recData->profile;
                        $thisParent['id'] = $recData->id;
                        $conferences[$conferenceCount]['parent']  = [];
                        $conferences[$conferenceCount]['parent'][$recKey] = $thisParent;
                    }
                }
                $conferenceCount++;
            }
        }

        if($type == 'internal-report'){
            $col = 'id';
            $sort = 'desc';
            if($request->col){
                $col = $request->col;
            }
            if($request->sort){
                $sort = $request->sort;
            }

            if($request->limit){
                $paginate = $request->limit;
            }
            if($request->search_keyword){
                $search_keyword = $request->search_keyword;
            }
            // $logs = $this->logs->getLogs()
            $logs = ActivityLogs::orderBy($col,$sort)
            ->paginate($paginate);;
        
            return view('compo.'.$type, compact('allUsers','resources','component','documentTypes','logs','paginate'))->with('PSJ', ($request->input('page', 1) - 1) * $paginate);;
        }

        
        return view('compo.'.$type, compact('allUsers','resources','component','documentTypes','logs','paginate','conferences'));
    }

    public function addDocuments($type,$document_id)
    {
        $component = $this->component->condition('code',$type)->fetchOne();
        $documentType = $this->documentTypes->condition('type',$document_id)->fetchOne();
        if( empty($documentType) )
        {
            $message = 'Invalid Document Type Pass!';
            return redirect(route('component.view',['type' => $type]))->withErrors($message);

        }
        return view('compo.resource_form', compact('component','documentType'));

    }

    function editDocuments($type,$document_id,$id)
    {
        $resource = '';
        if(!empty($id))
        $resource = $this->resources->condition('id',$id)->fetchOne();
        $documentType = $this->documentTypes->condition('type',$document_id)->fetchOne();
        $component = $this->component->condition('code',$type)->fetchOne();

        if( empty($documentType) )
        {
            $message = 'Invalid Document Type Pass!';
            return redirect(route('component.view',['type' => $type]))->withErrors($message);

        }
        if($component->id != $resource->designation_id){
            $message = 'Invalid type pass for this resource id : '.$id;
            return redirect(route('component.view',['type' => $type]))->withErrors($message);
        }
        return view('compo.resource_form', compact('component','resource','documentType'));
    }

    public function uploadDocuments(Request $request)
    {
        
        if (!auth()->user()['is_admin'] == 1) {
            $message = 'Access Denied!';
            return redirect(route('component.view',['type' => $request->type]))->withErrors($message);
        }
       

        $request->validate([
            'topic'        => 'required',
            'link'        => 'required_if:resource_type,link',
        ]);
        if(empty($request->uploaded_file)){
            $request->validate([
                'document'    => 'required_if:resource_type,document'
            ]);
    }
        
        if(!empty($request->id)){
        $resource = $this->resources->condition('id',$request->id)->fetchOne();
        $rsourceFile = $resource->file;
        }
        

        if($request->file()){
           
            $directoryPath = public_path().'/resources/'.$request->type.'/';
            
            if ($directoryPath) {
                File::makeDirectory($directoryPath, $mode = 0777, true, true);
            }
            if (!empty($rsourceFile) && file_exists($directoryPath.$rsourceFile) ) {
                unlink($directoryPath.$rsourceFile);
            }
            $fileName = time().'.'.$request->file('document')->getClientOriginalExtension();
            $request->document->move(
                $directoryPath, $fileName
            );
    
        }
        else{
            if(!empty($request->uploaded_file))
            $fileName = $request->uploaded_file;
        }
        
        $component = $this->component->fetchOne(['code'=>$request->type]);
        
        $data = [
            'designation_id' => $component->id,
            'topic'     => $request->topic,
            'document_type' => $request->document_id,
            'additional_info'   => !empty($request->additional_info) ? $request->additional_info : '',
            'link'  => (!empty($request->link) && $request->resource_type == 'link') ? $request->link : '',
            'file'    => (!empty($fileName)&& $request->resource_type == 'document') ? $fileName : '',
            ];

        
        if (empty($request->id)){
           $r = $this->resources->create($data);
           $des = "New Resource added in ".$request->type . " Component with id - " . $r->id;
           $this->logs->AddLogs($des);
        }
        else{

            $this->resources->update($data,['id' => $request->id]);
            $des = "Resource Update of ".$request->type . " Component with id - " . $request->id;
            $this->logs->AddLogs($des);
        }

            

        $message = $request->component_type.'Data uploaded successfully!';
        return redirect(route('component.view',['type' => $request->type]))->withSuccess($message);

    }

    function deleteResource($type,$id)
    {
        if (auth()->user()['is_admin'] != 1 || $id == auth()->user()['id']) {
                $message = 'Access Denied!';
                return  redirect(route('component.view',['type' => $type]))->withError($message);
        }else{
            
            $resource =  $this->resources->condition('id',$id)->fetchOne();
            if(!empty($resource->file))
            {
                $directoryPath = public_path().'/resources/'.$type.'/';
            
            if (file_exists($directoryPath.$resource->file) ) 
                unlink($directoryPath.$resource->file);

            }
            if( $this->resources->condition('id',$id)->delete()){
                $message = 'Resource deleted successfully!';
                return redirect(route('component.view',['type' => $type]))->withSuccess($message);
            }else{   
                $message = 'Resource went wrong!';
                return redirect(route('component.view',['type' => $type]))->withError($message);
            }
        }
    }

    // public function setCurrentTimeZone(Request $request){ //To set the current timezone offset in session
        
    //     if(!empty($request->curent_zone)){
    //         $current_time_zone = $request->curent_zone;
            
    //         Session::put('current_time_zone',  $current_time_zone);
    //     }
    // }

    
}

/*
------- static text code uses to distinguish different type of Documents/Links for component sections ------   
disability-resource
education-docs
education-result-rediness
health-ehs
health-hs
health-nutrition
mental-resource
education-resource
*/