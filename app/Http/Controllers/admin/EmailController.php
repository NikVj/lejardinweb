<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Mail;
class EmailController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function sendEmail($to_email, $to_name, $title, $subject)
    {
        $data['title'] = $title;
 
        Mail::send('emails.email', $data, function($message) {
 
            $message->to($to_email, $to_name)
 
                    ->subject($subject);
        });
 
        if (Mail::failures()) {
           return response()->Fail('Sorry! Please try again latter');
         }else{
           return response()->success('Great! Successfully send in your mail');
         }
    }
}