<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\UserRepository;
use App\Repositories\CalendarsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Validator;
use Image;
use File;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function __construct(UserRepository $repository, CalendarsRepository $cal_repository)
    {
        $this->repository = $repository;
        $this->cal_repository = $cal_repository;
        View::share('users.staff', ['allUsers']);
    }

    public function index($des_id='')
    {
        $allUsers = $this->repository->getUsers($des_id); // calling repository functions
        return view('users.staff', compact('allUsers'));
    }

    public function staff()
    {
        $allUsers = $this->repository->getUsers('','2'); // calling repository functions
        return view('users.staff', compact('allUsers'));
    }

    public function parent()
    {
        $allUsers = $this->repository->getUsers('','3'); // calling repository functions
        return view('users.staff', compact('allUsers'));
    }

    function saveUserForm($id=NULL)
    {
        if ($id==0) {
            return view('users.staff_save');
        }else{
            $userDetails = $this->repository->getUserDetails($id);
            return view('users.staff_save', compact('userDetails'));
        }
    }
    function viewUser($id)
    {
        if ($id==0) {
            return view('users.staff');
        }else{
            $events = $this->cal_repository->getEvents(auth()->user(),$id);
            $userDetails = $this->repository->getUserDetails($id);
            return view('users.staff_view', compact('userDetails','events'));
        }
    }

    function saveUserDetails(Request $request)
    {   
        $saved = $this->repository->saveusers(auth()->user(),$request); // calling repository functions

        if ($saved != true) {
            $message = 'Something went wrong!';
            return redirect('users')->withErrors($message);
        }

        $message = 'User added successfully!';
        return redirect('users')->withSuccess($message);
    }

    function updateUserDetails(Request $request)
    {
        $saved = $this->repository->updateusers(auth()->user(),$request); // calling repository functions
        if($saved != true){
            $message = 'Something went wrong!';
            return redirect('users')->withErrors($message);
        }

        $message = 'User updated successfully!';
        if(auth()->user()['is_admin'] == 1)
            return redirect('users')->withSuccess($message);
        else
            return redirect('viewUser/'.auth()->user()['id'])->withSuccess($message);
    }

    function deleteUser($id)
    {
        if (auth()->user()['is_admin'] != 1 || $id == auth()->user()['id']) {
                $message = 'Access Denied!';
                return redirect('users')->withErrors($message);
        }else{
            $findId = User::find( $id );
            if ($findId ->delete()){
                $message = 'Staff deleted successfully!';
                return redirect('users')->withSuccess($message);
            }else{   
                $message = 'Something went wrong!';
                return redirect('users')->withErrors($message);
            }
        }
    }

    function chstatusUser($id)
    {
        if (auth()->user()['is_admin'] != 1 || $id == auth()->user()['id']) {
                $message = 'Access Denied!';
        }else{
            $findId = Profile::where('user_id', $id)->first();
            if($findId){
                if(($findId->status) == 0){
                    $status = '1';
                }else{
                    $status = '0';
                }
                $data   =  array(
                                'status' => $status
                            );
                if (DB::table('user_details')->where('id',$findId->id)->update($data)){
                    $message = 'Status Changed successfully!';
                    return redirect('users')->withSuccess($message);
                }else{   
                    $message = 'Something went wrong!';
                }
            }else{
                 $message = 'User does not exist!';
            }
        }
        return redirect('users')->withErrors($message);
    }

    function updatePassword(Request $request)
    {
        $saved = $this->repository->updatePassword(auth()->user(),$request); // calling repository functions
        if($saved != true){
            $message = 'Something went wrong!';
        }else{
            if(is_object(json_decode($saved))){
                $message = json_decode($saved)->error;
            }else{
                $message = 'Password updated successfully!';
                $success = true;
                return redirect()->back()->withSuccess($message);
            }
        }
        return redirect()->back()->withErrors($message);
    }
}