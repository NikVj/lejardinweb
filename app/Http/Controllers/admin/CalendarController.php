<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Calendars;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Repositories\AttendanceRepository;
use Illuminate\Support\Facades\View;
use App\Models\Notification;

class CalendarController extends Controller
{
    public function __construct( AttendanceRepository $attendance_repository)
    {
        $this->attendance_repository = $attendance_repository;
        View::share('users.staff', ['allUsers']);
    }

	// 2021-03-08T08:30:00
	// 2021-03-08T13:00:00

	function addCalDetails(Request $request)
	{

		// if (!auth()->user()['is_admin'] == 1) {
		// 	$message = 'Access Denied!';
		// 	return redirect('staff')->withErrors($message);
		// }
		$request->validate([
			'title'       => 'required',
			'etype'       => 'required',
			'start_date'  => 'required',
			'end_date'    => 'required',
			'event_name'    => 'required_without_all:appointment_name',
			'appointment_name'    => 'required_without_all:event_name'
		]);

		$startDate = date('Y-m-d'.'\T'.'H:i:s', strtotime($request->all()['start_date']));
		$end_date  = date('Y-m-d'.'\T'.'H:i:s', strtotime($request->all()['end_date']));
		
		// pr($startDate);

        if($startDate < $end_date){DB::enableQueryLog();
        	if($request->post('staff') && count($request->post('staff')) > 0){
	        	foreach($request->post('staff') as $perstaff){
	        		$userBusy = Calendars::whereRaw("find_in_set(".$perstaff.",staff)")
		            ->where(function($query) use ($startDate, $end_date) {
			            $query->where(function($queryStart) use ($end_date) {
		        			$queryStart->where('start_date','<',$end_date);
		        			$queryStart->where('end_date','>',$end_date);
			            });
			            $query->orWhere(function($queryEnd) use ($startDate) {
		        			$queryEnd->where('start_date','<',$startDate);
		        			$queryEnd->where('end_date','>',$startDate);
			            });
			        })->get();
	        		if($userBusy->count() > 0){
						$message = 'Staff is busy in this time period!';
						return redirect('calendar')->withErrors($message);
	        			return false;
	        		}
	        	}
	        }
        	if($request->post('parent') && count($request->post('parent')) > 0){
	        	foreach($request->post('parent') as $perparent){
	        		$userBusy = Calendars::whereRaw("find_in_set(".$perparent.",parent)")
		            ->where(function($query) use ($startDate, $end_date) {
			            $query->where(function($queryStart) use ($end_date) {
		        			$queryStart->where('start_date','<',$end_date);
		        			$queryStart->where('end_date','>',$end_date);
			            });
			            $query->orWhere(function($queryEnd) use ($startDate) {
		        			$queryEnd->where('start_date','<',$startDate);
		        			$queryEnd->where('end_date','>',$startDate);
			            });
			        })->get();
	        		if($userBusy->count() > 0){
						$message = 'Parent is busy in this time period!';
						return redirect('calendar')->withErrors($message);
	        			return false;
	        		}
	        	}
	        }
			// pr($request->all());
			$calData = array(
				'user_id'     => auth()->user()['id'],
				'title'       => $request->all()['title'],
				'event_type'       => $request->all()['etype'],
				'event_name'       => ($request->all()['etype'] == 2) ? $request->all()['appointment_name'] : $request->all()['event_name'],
				'start_date'  => $startDate,
				'end_date'    => $end_date,
				'staff'    => ($request->post('staff') != '')?implode(',',$request->post('staff')):'',
				'parent'    => ($request->post('parent') != '')?implode(',',$request->post('parent')):'',
				'description' => $request->all()['description'],
			);

			// pr($calData);die;

			$addEvent = Calendars::create($calData);
            $notificationData['from_user']=auth()->user()['id'];
            $postedby=auth()->user()['firstname'];
            $notificationData['content']=$postedby. ' invited you in an event';
            $notificationData['url_component']='calendar';
            $notificationData['section_id']=$addEvent->id;
            $recievers = array();
            if($request->post('parent') != '')
            	$recievers =array_merge($request->post('parent'),$recievers);
            if($request->post('staff') != '');
            	$recievers =array_merge($request->post('staff'),$recievers);
            foreach($recievers as $eachrec){
                $notificationData['to_user']=$eachrec;
                $notificationSaved = Notification::Create($notificationData);
            }

			if ($addEvent) {
				$message = 'Event added successfully!';
				return redirect('calendar')->withSuccess($message);
			}else{
				$message = 'Something went wrong!';
				return redirect('calendar')->withErrors($message);
			}
		}else{
			$message = 'Start date should be prior to end date!';
			return redirect('calendar')->withErrors($message);
		}
	}

	function attendance(Request $request){
		if (!(auth()->user()['user_role'] == 1 || auth()->user()['user_role'] == 2)) {
			$message = 'Access Denied!';
			return redirect('calendar')->withErrors($message);
		}
		$filters = array();
		if($request->post('year')){
			$filters = array_merge($filters,array('year'=>$request->post('year')));
		}
		if($request->post('month')){
			$filters = array_merge($filters,array('month'=>$request->post('month')));
		}
		$data = $this->attendance_repository->getAttendance($filters);
		return view('calendar.attendance', compact('data'));
	}
	function editAttendance($id='',Request $request)
	{
		if (!(auth()->user()['user_role'] == 1 || auth()->user()['user_role'] == 2)) {
			$message = 'Access Denied!';
			return redirect('calendar')->withErrors($message);
		}

		$condition = array();
		if($id != ''){
        	$condition = array('id'=>$id);
		}else if($request->get('date') != ''){
        	$condition = array('date'=>$request->get('date'),'marked_by'=>auth()->user()['id']);			
		}
        if($request->isMethod('post')){
			$request->validate([
				'date'  => 'required',
				'children'    => 'required'
			]);

			$date = $request->post('date');

			// pr($date);
			// pr($request->all());
			$attendanceData = array(
				'marked_by'     => auth()->user()['id'],
				'date'  => $date,
				'day'  => date('d', strtotime($request->all()['date'])),
				'month'  => date('m', strtotime($request->all()['date'])),
				'year'  => date('Y', strtotime($request->all()['date'])),
				'children'    => $request->post('children'),
				'description' => isset($request->all()['description']) ? $request->all()['description'] : '',
				'timestamp' => strtotime('now'),
			);
			$addAttendance = $this->attendance_repository->saveAttendance($condition,$attendanceData);

			if ($addAttendance) {
				$message = 'attendance saved successfully!';
				$type = 'Success';
            	if(!$request->ajax()){
					return redirect('attendance')->withSuccess($message);
				}
			}else{
				$message = 'Something went wrong!';
				$type = 'Success';
            	if(!$request->ajax()){
					return redirect('attendance')->withErrors($message);
				}
			}
			echo json_encode(array('message' => $message,'type' => $type));die;
		}
        if($id != '' || $request->get('date') != ''){
            $data['attendanceData'] = $this->attendance_repository->getAttendance($condition)['attendanceData'];
            $data['date'] = $request->get('date');
        }

        $data['children'] = children();
    	if(!$request->ajax()){
			return view('calendar.mark_attendance', compact('data'));
		}
		else{
			return view('calendar.mark_calattendance', compact('data'));
		}

	}

	function deleteCalDetails($id)
	{
		$checkCal = Calendars::get()->where('id', $id)->where('user_id', auth()->user()['id'])->first();

        if (auth()->user()['is_admin'] != 1 || empty($checkCal)) {
            return response()->json([
                'class_name'  => 'alert-danger',
                'message'     => 'Something went wrong!'
            ]);
        }else{
            $findId = Calendars::find( $id );
            if ($findId ->delete()){	
                return response()->json([
                    'class_name'  => 'alert-success',
                    'message'     => 'Calendar event deleted successfully!'
                ]);
            }else{   
                return response()->json([
                    'class_name'  => 'alert-danger',
                    'message'     => 'Something went wrong!'
                ]);
            }
        }
	}
}