<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Feed;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\NewslettersRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Validator;
use Image;
use File;
use Illuminate\Support\Facades\View;

class NewslettersController extends Controller
{
    public function __construct(NewslettersRepository $repository, UserRepository $user_repository)
    {
        $this->repository = $repository;
        $this->user_repository = $user_repository;
        View::share('newsletters.index', ['allNewsletter']);
    }

    public function index()
    {
        $allNewsletter = $this->repository->getNewsletters(auth()->user());
        return view('newsletters.index', compact('allNewsletter'));
    }

    public function test()
    {
        $UserRepo = App::make(NewslettersRepository::class);
        $user = $UserRepo->condition('id', 1)->fetchOne();
        // dd($user);

    }

    public function edit($id='', Request $request)
    {
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'title' => $request->post('title'),
                'desc' => $request->post('desc'),
                'posted_by' => auth()->user()['id'],
                'staff' => ($request->post('staff') != '')?implode(',',$request->post('staff')):'',
                'parent' => ($request->post('parent') != '')?implode(',',$request->post('parent')):'',
            );

            $newsletterSaved = $this->repository->addNewsletter(auth()->user(),$condition,$Data,$request->file('link'));
            if($newsletterSaved){
                $message = 'Newsletters Sent Succesfully!!';
                return redirect('newsletters')->withSuccess($message);
            }else{
                return redirect()->back()->withErrors($message);
            }
        }
        $newsletterDetails = array();
        if($id != '')
            $newsletterDetails = $this->repository->fetchOne($condition);

            $allparents = $this->user_repository->getUsers('','3');
            $allstaff = $this->user_repository->getUsers('','2');
        return view('newsletters.newsletter_save', compact('newsletterDetails','allparents','allstaff'));
        // dd($user);

    }

    public function delete($id='')
    {
        if($id==''){
            $message = 'Please enter ID of the item to be deleted';
            return redirect('newsletters')->withErrors($message);
        }
        $message = 'Something went wrong!!';
        $condition = array('id'=>$id);
        $deleted = $this->repository->delete($condition);
        if($deleted){
            $message = 'Newsletters deleted Succesfully!!';
            return redirect('newsletters')->withSuccess($message);
        }
        return redirect('newsletters')->withErrors($message);
    }
}