<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Calendars;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Repositories\NotificationsRepository;
use Illuminate\Support\Facades\View;
use App\Models\Notification;

class NotificationsController extends Controller
{
    public function __construct( NotificationsRepository $notification_repository)
    {
        $this->repository = $notification_repository;
        View::share('users.staff', ['allUsers']);
    }

	// 2021-03-08T08:30:00
	// 2021-03-08T13:00:00

	function index(Request $request){
        if($request->ajax()){
    		$data = Notification::where('to_user',auth()->user()['id'])->limit(30)->orderBy('id', 'DESC')->get();
    		$count=Notification::where('to_user',auth()->user()['id'])->where('is_read',0)->count();
			echo json_encode(array('data' => $data,'count'=>$count));die;
    	}else{
        
	        $paginate = 10;

	        if($request->limit){
	            $paginate = $request->limit;
	        }
	        $col = 'id';
	        $sort = 'desc';
	        if($request->col){
	            $col = $request->col;
	        }
	        if($request->sort){
	            $sort = $request->sort;
	        }
	        $data = Notification::where('to_user',auth()->user()['id'])->orderBy($col,$sort)
            ->paginate($paginate);
	        return view('notifications.index', compact('data','paginate'))->with('PSJ', ($request->input('page', 1) - 1) * $paginate);
	    }
	}
}