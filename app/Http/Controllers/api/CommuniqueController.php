<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\Chat;
use App\Models\ChatGroup;
use App\Repositories\CommuniqueRepository;
use App\Repositories\UserRepository;

use App\Lib\PusherFactory;
class CommuniqueController extends Controller
{
    protected $user;
 
    public function __construct(CommuniqueRepository $repository, UserRepository $user_repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->repository = $repository;
        $this->user_repository = $user_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function chats($search_keyword='')
    {
        $search_Array = array();
        $recentUsers = array();
        $userIDs = array();
        $users = $this->repository->getLastMessages($this->user, $search_keyword);
        foreach($users as $key => $user){
            if($user['from_user'] == $this->user['id']){
                array_push($userIDs,$user['to_user']);
            }
            else{
                array_push($userIDs,$user['from_user']);
            }
            array_push($recentUsers, $user);
            $users[$key]['unreadCount'] = Chat::select(DB::raw("COUNT('read_by') as unreadCount") )->whereRaw(' FIND_IN_SET("'.$this->user['id'].'",read_by) = 0 ')->where('group_id',$users[$key]->group_id)->get()[0]['unreadCount'];
        }
            array_push($userIDs,$this->user['id']);
        $allusers = $this->user_repository->getUsers('','',$userIDs,$search_keyword);
        if($search_keyword != ''){
            $search_Array = array_merge($recentUsers,(array)$allusers);
        }
        return response()->json([
            'success' => true,
            'staffProfileImageUrl' => url('images/users').'/',
            'recentUsers' => $recentUsers,
            'allUsers' => $allusers,
            'search_Array' => $search_Array,
        ], Response::HTTP_OK);
    }
    public function getLatestMessages(Request $request)
    {
        $User2Data = array();
        $users = $this->user_repository->getUsers();
        if(!$request->group_id) {
            return;
        }
        $data = array(
            'read_by' => DB::raw('CONCAT(read_by, ",'.$this->user['id'].'")')
        );
        Chat:: where('group_id', $request->group_id)->whereRaw(' FIND_IN_SET("'.$this->user['id'].'",read_by) = 0 ')->update($data);

        $messages = Chat::select('chats.*',DB::raw("CONCAT(sender.firstname,sender.lastname) AS fromUserName"),DB::raw("CONCAT(receiver.firstname,receiver.lastname) AS receiverName"),'sender.profile as fromUserImage','receiver.profile as receiverImage')->where('group_id', $request->group_id)
        ->join('users as sender','sender.id','from_user')->join('users as receiver','receiver.id','to_user')->orderBy('id','DESC')->latest()->get();
        // $messages = $chatMessage->reverse()->values();
        foreach($messages as $index => $message){
            $messages[$index]->dateTimeStr = date("Y-m-dTH:i", strtotime($message->created_at->toDateTimeString()));

            $messages[$index]->dateHumanReadable = $message->created_at->diffForHumans();
        }

        $groupObj = ChatGroup::findOrFail($request->group_id);
        $usersInGroup = explode(',',$groupObj->users);
        foreach($usersInGroup as  $key => $perUser){
            if($perUser != $this->user['id']){
                $User2Data = userData($perUser);
            }
        }
        return response()->json([
            'success' => true,
            'staffProfileImageUrl' => url('images/users').'/',
            'messages' => $messages,
            'User2Data' => $User2Data,
        ], Response::HTTP_OK);
    }
    public function sendMessage(Request $request)
    {
        if(!$request->id ||  (!$request->message && (!$request->type || $request->type =='text') )) {
            return;
        }
        $chatData = array(
            'from_user'     => $this->user['id'],
            'to_user'       => $request->id,
            'group_id'       => $request->group_id,
            'content'       => $request->message ? $request->message : '',
            'file'       => $request->file('file'),
            'type'       => (($request->type && $request->type != '') ? $request->type : 'text') ,
            'timestamp'       => strtotime('now'),
        );
        $message = $this->repository->saveChat($this->user,$chatData, $request->post('group_name'));


        return response()->json([
            'success' => true,
            'staffProfileImageUrl' => url('images/users').'/',
            'message' => $message,
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id='', Request $request)
    {
        $success=true;
        $message='';
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'title' => $request->post('title'),
                'desc' => $request->post('desc'),
                'privacy' => $request->post('privacy'),
                'posted_by' => $this->user['id'],
                'staff' => ($request->post('privacy') == 0) ? (($request->post('staff') != '')? $request->post('staff') :'') :'',
                'parent' => ($request->post('privacy') == 0) ? (($request->post('parent') != '')? $request->post('parent') :'') :'',
            );

            $feedSaved = $this->repository->addFeed($this->user,$condition,$Data,$request->file('link'));
            if($feedSaved){
                $message = 'Feed Saved Succesfully!!';
            }else{
                $success=false;
            }
        }
        $feedDetails = array();
        if($id != '')
            $feedDetails = $this->repository->fetchOne($condition);

        $allparents = $this->user_repository->getUsers('','3');
        $allstaff = $this->user_repository->getUsers('','2');
        $Allfields = array(
                      'feedDetails' => $feedDetails,
                      'allparents' => $allparents,
                      'allstaff'  => $allstaff
                    );
        return response()->json([
            'success' => $success,
            'message' => $message,
            'allFields' => $Allfields
        ], Response::HTTP_OK);
        // dd($user);

    }
}
