<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\DesignationsRepository;
use App\Repositories\ComponentResourcesRepository;
use App\Repositories\DocumentTypesRepository;
use App\Repositories\ActivityLogsRepository;
use App\Models\Calendars;

class ComponentController extends Controller
{
    protected $user;
 
    public function __construct(DocumentTypesRepository $documentTypes, DesignationsRepository $component , ComponentResourcesRepository $resources , ActivityLogsRepository $logs, UserRepository $user_repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->documentTypes = $documentTypes;
        $this->component = $component;
        $this->resources = $resources;
        $this->logs = $logs;
        $this->user_repository = $user_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $allFields = [];
        
        $component = $this->component->fetchOne(['code'=>$type]);
       
        if(empty($component))
        {
            $message = 'Component Not Found';
            return redirect(route('home'))->withErrors($message);
        }else{

            $allUsers = [];
            
            $logs = [];
        
            $conferences = [];
            
            $allUsers = $this->user_repository->getUsers($component->id);
            
            $documentTypes = $this->documentTypes->fetch();
            
            $resources = $this->resources->getResources( $component->id);

            if($type == 'education'){
                $conferencesQuery = Calendars::where('event_name','=','10');
                if($this->user['user_role'] == 3){
                    // $children = ChildRelation::where('rel_type','parent')->where('rel_from',auth()->user()['id'])->where('status',1)->get('rel_to');
                    // echo '<pre>';print_r($childrenArray);die;
                   $conferencesQuery->whereRaw("find_in_set(".$this->user['id'].",parent)");
                }else if($this->user['user_role'] == 2){
                    $conferencesQuery->whereRaw("find_in_set(".$this->user['id'].",staff)");
                }
                $conferences = $conferencesQuery->get()->toArray();
                $conferenceCount  =  0;
                foreach($conferences as $conference){
                    $recipients = array();
                    if($conference['staff']!='' && $conference['staff']!=NULL){
                        $recipients = explode(',',$conference['staff']);
                        foreach ($recipients as $recKey => $recVal) {
                            $recData = userData($recVal);
                            $thisStaff = array();
                            $thisStaff['name'] = $recData->firstname.' '.$recData->lastname;
                            $thisStaff['profile'] = $recData->profile;
                            $thisStaff['id'] = $recData->id;
                            $conferences[$conferenceCount]['staff']  = [];
                            // array_push($conference['staff'],$thisStaff);
                            // echo '<pre>';
                            // print_r($conference);
                            $conferences[$conferenceCount]['staff'][$recKey] = $thisStaff;
                        }
                    }
                    if($conference['parent']!='' && $conference['parent']!=NULL){
                        $recipients = explode(',',$conference['parent']);
                        foreach ($recipients as $recKey => $recVal) {
                            $recData = userData($recVal);
                            $thisParent = array();
                            $thisParent['name'] = $recData->firstname.' '.$recData->lastname;
                            $thisParent['profile'] = $recData->profile;
                            $thisParent['id'] = $recData->id;
                            $conferences[$conferenceCount]['parent']  = [];
                            $conferences[$conferenceCount]['parent'][$recKey] = $thisParent;
                        }
                    }
                    $conferenceCount++;
                }
            }

            if($type == 'internal-report'){
                $logs = $this->logs->getLogs();
            }

            $allFields = array(
                'allUsers' => $allUsers,
                'logs' => $logs,
                'documentTypes' => $documentTypes,
                'resources' => $resources,
                'component' => $component,
                'conferences' => $conferences,
            );
        }
        return response()->json([
            'success' => true,
            'fileUrl' => url('/resources/'.$type).'/',
            'staffProfileImageUrl' => url('images/users'),
            'allFields' => $allFields
        ], Response::HTTP_OK);
    }

}
