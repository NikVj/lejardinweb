<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Feed;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\FeedsRepository;
use App\Repositories\UserRepository;

class FeedsController extends Controller
{
    protected $user;
 
    public function __construct(FeedsRepository $repository, UserRepository $user_repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->repository = $repository;
        $this->user_repository = $user_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allFeeds = $this->repository->getFeeds($this->user);
        $i = 0;
        foreach($allFeeds as $perfeed){
            $allFeeds[$i]->posted_by_data = userData($perfeed->posted_by);
            $allFeeds[$i]->posted_by_data->user_profile_image_url = userProfile($perfeed->posted_by);
            $i++;
        }
        return response()->json([
            'success' => true,
            'allFeeds' => $allFeeds,
            'staffProfileImageUrl' => url('images/users').'/',
            'ImageUrl' => url('/images/feeds'),
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id='', Request $request)
    {
        $success=true;
        $message='';
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'title' => $request->post('title'),
                'desc' => $request->post('desc'),
                'privacy' => $request->post('privacy'),
                'posted_by' => $this->user['id'],
                'staff' => ($request->post('privacy') == 0) ? (($request->post('staff') != '')? $request->post('staff') :'') :'',
                'parent' => ($request->post('privacy') == 0) ? (($request->post('parent') != '')? $request->post('parent') :'') :'',
            );

            $feedSaved = $this->repository->addFeed($this->user,$condition,$Data,$request->file('link'));
            if($feedSaved){
                $message = 'Feed Saved Succesfully!!';
            }else{
                $success=false;
            }
        }
        $feedDetails = array();
        if($id != '')
            $feedDetails = $this->repository->fetchOne($condition);

        $allparents = $this->user_repository->getUsers('','3');
        $allstaff = $this->user_repository->getUsers('','2');
        $Allfields = array(
                      'feedDetails' => $feedDetails,
                      'allparents' => $allparents,
                      'allstaff'  => $allstaff
                    );
        return response()->json([
            'success' => $success,
            'message' => $message,
            'allFields' => $Allfields
        ], Response::HTTP_OK);
        // dd($user);

    }
    public function likeFeed(Request $request)
    {
        $status = $liked = false;
        $message = 'Something went wrong!!';
        if($request->id != ''){
            $liked = $this->repository->likeFeed($request->id, $this->user);
            if($liked){
                $message = 'Feed '. (($liked == 1) ? 'unliked' : 'liked').' Succesfully!!';
                $status = true;
            }
        }
        return response()->json([
            array(
                'status' => $status,
                'message' => $message,
                'like' => $liked,
            )
        ]);
    }
    public function addComment(Request $request)
    {
        $status = $added = false;
        $message = 'Something went wrong!!';
        if($request->id != ''){
            $added = $this->repository->addComment($request->id, $request->comment, $this->user);
            if($added){
                $message = 'Comment Added Succesfully!!';
                $status = true;
            }
        }
        return response()->json([
            array(
                'status' => $status,
                'message' => $message,
                'comment' => $added,
                'staffProfileImageUrl' => url('images/users').'/',
            )
        ]);
    }
    public function removeComment(Request $request)
    {
        $status = $removed = false;
        $comment =[];
        $message = 'Something went wrong!!';
        if($request->id != ''){
            $removed = $this->repository->removeComment($request->id, $this->user);
            if($removed){
                if($removed == 2){
                    $message = 'Access Denied!!';
                }else{
                    if(is_object(json_decode($removed))){
                        $comment = json_decode($removed)->comment;
                        $message = 'Comment removed Succesfully!!';
                        $status = true;
                    }
                }
            }else{
                $message = 'Comment Not Found!!';
            }
        }
        return response()->json([
            array(
                'status' => $status,
                'message' => $message,
                'comment' => $comment,
                'staffProfileImageUrl' => url('images/users').'/',
            )
        ]);
    }
}
