<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Feed;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\NewslettersRepository;
use App\Repositories\UserRepository;

class NewslettersController extends Controller
{
    protected $user;
 
    public function __construct(NewslettersRepository $repository, UserRepository $user_repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->repository = $repository;
        $this->user_repository = $user_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allNewsletter = $this->repository->getNewsletters($this->user);
        return response()->json([
            'success' => true,
            'allNewsletter' => $allNewsletter
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id='', Request $request)
    {
        $success=true;
        $message='';
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'title' => $request->post('title'),
                'desc' => $request->post('desc'),
                'posted_by' => $this->user['id'],
                'staff' => ($request->post('staff') != '') ? $request->post('staff') :'',
                'parent' => ($request->post('parent') != '') ? $request->post('parent') :'',
            );

            $newsletterSaved = $this->repository->addNewsletter($this->user,$condition,$Data,$request->file('link'));
            if($newsletterSaved){
                $message = 'Newsletters Sent Succesfully!!';
            }else{
                $success=false;
            }
        }
        $newsletterDetails = array();
        if($id != '')
            $newsletterDetails = $this->repository->getNewsletter($condition);

        $allparents = $this->user_repository->getUsers('','3');
        $allstaff = $this->user_repository->getUsers('','2');

        $Allfields = array(
                      'newsletterDetails' => $newsletterDetails,
                      'allparents' => $allparents,
                      'allstaff' => $allstaff,
                    );
        return response()->json([
            'success' => $success,
            'message' => $message,
            'allFields' => $Allfields
        ], Response::HTTP_OK);
        // dd($user);

    }
}
