<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\Calendars;
use App\Repositories\AttendanceRepository;
use App\Repositories\CalendarsRepository;

class CalendarController extends Controller
{
    protected $user;
 
    public function __construct(AttendanceRepository $attendance_repository,CalendarsRepository $repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->repository = $repository;
        $this->attendance_repository = $attendance_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function attendance(Request $request)
    {
        $success=true;
        $message = '';
        $data = array();
        if (!($this->user['user_role'] == 1 || $this->user['user_role'] == 2)) {
            $message = 'Access Denied!';
            $success = false;
        }else{
            $filters = array();
            if($request->post('year')){
                $filters = array_merge($filters,array('year'=>$request->post('year')));
            }
            if($request->post('month')){
                $filters = array_merge($filters,array('month'=>$request->post('month')));
            }
            $data = $this->attendance_repository->getAttendance($filters);
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], Response::HTTP_OK);
    }
    public function calData(Request $request)
    {
        $success=true;
        $message = '';
        $data = array();
        $data = $this->repository->getEvents($this->user);
        $data['calEventsData'] = json_decode($data['calEventsData']);
        $data['appointmentsEventsData'] = json_decode($data['appointmentsEventsData']);
        $data['alleventtypes'] = getalleventnames('0');
        $data['allappointmenttypes'] = getalleventnames('1');
        $EventsMarkedDates = array();
        $AppointmentMarkedDates = array();
        foreach($data['calEventsData'] as $calevent){
            $dateIndex = date('Y-m-d',strtotime($calevent->start));
            if(!isset($EventsMarkedDates[$dateIndex])){
                $EventsMarkedDates[$dateIndex] = array('selected' => true, 'marked' => true, 'selectedColor' => '#3fcabd');
            }
        }
        foreach($data['appointmentsEventsData'] as $appevent){
            $dateIndex = date('Y-m-d',strtotime($appevent->start));
            if(!isset($AppointmentMarkedDates[$dateIndex])){
                $AppointmentMarkedDates[$dateIndex] = array('selected' => true, 'marked' => true, 'selectedColor' => '#b91212');
            }
        }
        $data['EventsMarkedDates'] = ($EventsMarkedDates);
        $data['AppointmentMarkedDates'] = ($AppointmentMarkedDates);

        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], Response::HTTP_OK);
    }

    public function markAttendance($id='',Request $request)
    {
        $success=true;
        $message = '';
        $data = array();

        if (!($this->user['user_role'] == 1 || $this->user['user_role'] == 2)) {
            $message = 'Access Denied!';
        }else{
            $condition = array();
            if($id != ''){
                $condition = array('id'=>$id);
            }else if($request->get('date') != ''){
                $condition = array('date'=>$request->get('date'),'marked_by'=>$this->user['id']);            
            }
            if($request->isMethod('post')){
                $request->validate([
                    'date'  => 'required',
                    'children'    => 'required'
                ]);

                $date = $request->post('date');

                $attendanceData = array(
                    'marked_by'     => $this->user['id'],
                    'date'  => $date,
                    'day'  => date('d', strtotime($request->all()['date'])),
                    'month'  => date('m', strtotime($request->all()['date'])),
                    'year'  => date('Y', strtotime($request->all()['date'])),
                    'children'    => json_encode($request->post('children')),
                    'description' => isset($request->all()['description']) ? $request->all()['description'] : '',
                    'timestamp' => strtotime('now'),
                );
                $addAttendance = $this->attendance_repository->saveAttendance($condition,$attendanceData);

                if ($addAttendance) {
                    $message = 'attendance saved successfully!';
                }else{
                    $message = 'Something went wrong!';
                    $success=false;
                }
            }
            if($id != '' || $request->get('date') != ''){
                $data['attendanceData'] = $this->attendance_repository->getAttendance($condition)['attendanceData'];
                $data['attendanceData']->children = json_decode($data['attendanceData']->children);
                $data['date'] = $request->get('date');
            }

            $data['children'] = children();
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], Response::HTTP_OK);
    }
    
    function addCalDetails(Request $request)
    {
        $success=true;
        $message='';

        $request->validate([
            'title'       => 'required',
            'etype'       => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
            'event_name'    => 'required'
        ]);

        $startDate = date('Y-m-d'.'\T'.'H:i:s', strtotime($request->all()['start_date']));
        $end_date  = date('Y-m-d'.'\T'.'H:i:s', strtotime($request->all()['end_date']));
        if($startDate < $end_date){
            $calData = array(
                'user_id'     => $this->user['id'],
                'title'       => $request->all()['title'],
                'event_type'       => $request->all()['etype'],
                'event_name'       => $request->all()['event_name'],
                'start_date'  => $startDate,
                'end_date'    => $end_date,
                'staff'    => ($request->post('staff') != '') ? $request->post('staff') :'',
                'parent'    => ($request->post('parent') != '') ? $request->post('parent') :'',
                'description' => $request->all()['description'],
            );

            $addEvent = Calendars::create($calData);

            if ($addEvent) {
                $message = 'Event added successfully!';
            }else{
                $success=false;
                $message = 'Something went wrong!';
            }
        }else{
            $success=false;
            $message = 'Start date should be prior to end date!';
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ], Response::HTTP_OK);
    }


}
