<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Location;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\LocationsRepository;

class LocationController extends Controller
{
    protected $user;
 
    public function __construct(LocationsRepository $repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allLocation = $this->repository->fetch();
        return response()->json([
            'success' => true,
            'allLocation' => $allLocation
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id='', Request $request)
    {
        $success=true;
        $message='';
        $condition = array('id'=>$id);
        if($request->post()){
            $message = 'Something went wrong!!';
            $Data = array(
                'name' => $request->post('name'),
                'place' => $request->post('place'),
                'lat' => $request->post('lat'),
                'long' => $request->post('long'),
            );
            $locSaved = $this->repository->updateOrCreate($condition,$Data);
            if($locSaved){
                $message = 'Location Saved Succesfully!!';
            }else{
                $success=false;
            }
        }
        $locDetails = array();
        if($id != '')
            $locDetails = $this->repository->fetchOne($condition);

        $Allfields = array(
                      'locDetails' => $locDetails
                    );
        return response()->json([
            'success' => $success,
            'message' => $message,
            'allFields' => $Allfields
        ], Response::HTTP_OK);
        // dd($user);

    }
}
