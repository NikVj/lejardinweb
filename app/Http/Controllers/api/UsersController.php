<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use App\Models\Children;
use App\Models\ChildRelation;
use App\Repositories\UserRepository;
use App\Repositories\CalendarsRepository;

class UsersController extends Controller
{
    protected $user;
 
    public function __construct(UserRepository $repository, CalendarsRepository $cal_repository)
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
        $token =  JWTAuth::getToken();
        try {
            $this->user = JWTAuth::authenticate($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return;
        }
        $this->repository = $repository;
        $this->cal_repository = $cal_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userDetails = $this->repository->getUserDetails($this->user['id']);
        $returnarray = array(
            'success' => true,
            'userDetails' => $userDetails,
            'userProfileImageUrl' => url('images/users').'/',
        );
        if($this->user['user_role'] == 2) {
            $staffchild = $designations = array();
            $staffchild = array('staffchild'=> children($userDetails->user_id));
            $selfchild = children($userDetails->user_id,1);
            $i = 0;
            // echo '<pre>';print_r($staffchild['staffchild']);die;
            foreach ($staffchild['staffchild'] as $perchild){
                if(in_array($perchild,$selfchild)){
                    $staffchild['staffchild'][$i]->selected = 1;
                }else{
                    $staffchild['staffchild'][$i]->selected = 0;
                }
                $i++;
            }
            $designations = array('designations'=> userDesignations());
            $returnarray = array_merge($returnarray, $staffchild);
            $returnarray = array_merge($returnarray, $designations);
        }else{
            $child = array();
            $child = array('child'=> children($userDetails->user_id,1));
            $returnarray = array_merge($returnarray, $child);
        }
        return response()->json($returnarray, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $success = false;
        $data = $request->post();
        $saved = $this->repository->updateusers($this->user,$request,true);
        if($saved != true){
            $message = 'Something went wrong!';
        }else{
            if(is_object(json_decode($saved))){
                $message = json_decode($saved)->error;
            }else{
                $message = 'User updated successfully!';
                $success = true;
            }
        }
        $userDetails = $this->repository->getUserDetails($this->user['id']);
        return response()->json([
            'success' => $success,
            'message' => $message,
            'userDetails' => $userDetails
        ], Response::HTTP_OK);
        // dd($user);

    }
}
