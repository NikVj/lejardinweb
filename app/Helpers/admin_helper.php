<?php

use Illuminate\Support\Facades\Auth;
use App\Models\Designations;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use App\Models\ActivityLogs;

if (!function_exists('pr')) {
    function pr($data = array()){
        echo "<pre>";
        print_r($data);
        echo "<pre>";
    }
}

if (!function_exists('userDesignations')) {
    function userDesignations($id=''){
        if($id == ''){
            return Designations::all()->toArray();
        }else{
            return Designations::where('id',$id)->get()->first();
        }
    }
}

if (!function_exists('userData')) {
    function userData($id = ''){
        if($id == ''){
            $id = auth()->user()['id'];
        }
        return DB::table('users')
        ->select('users.*','user_details.*')
        ->join('user_details','user_details.user_id','=','users.id')
        ->where('users.id', $id)
        ->get()->first();
    }
}

if (!function_exists('getalleventnames')) {
    function getalleventnames($event_category = '0'){
        return DB::table('event_types')
        ->where('event_category','=', $event_category)
        ->get()->toArray();
    }
}

if (!function_exists('children')) {
    function children($id='',$onlyself=0){
        $children= DB::table('children');
        if($id != ''){
            $children->select('children.*','child_relations.status as child_status');
            if($onlyself == 1){
                $children->join('child_relations', function($join) use ($id)
                 {
                     $join->on('child_relations.rel_to','=','children.id');
                     $join->on('child_relations.rel_from','=',DB::raw("'".$id."'"));
                     $join->on('child_relations.status','=',DB::raw("1"));
                 });
            }else{
                $children->leftJoin('child_relations', function($join) use ($id)
                 {
                     $join->on('child_relations.rel_to','=','children.id');
                     $join->on('child_relations.rel_from','=',DB::raw("'".$id."'"));
                     $join->on('child_relations.status','=',DB::raw("1"));
                 });
            }
        }else
            $children->select('children.*');
        $children=$children->get()->toArray();
        return $children;
    }
}
if (!function_exists('userProfile')) {
    function userProfile(){
        $profile = userData()->profile;
        if (!empty($profile) && file_exists( public_path().'/images/users/'.$profile )) {
            $profilePath = '/images/users/'.$profile;
        }else{
            $profilePath = '/images/user.png';
        }
        return url($profilePath);
    }
}
if (!function_exists('getactionicons')) {
    function getactionicons($id, $url='',$action_string='EDSV'){
        $all_actions = str_split($action_string,1);
        $actionshtml =   "<ul class=\"list-inline d-flex m-0\">";
        if(in_array("E",$all_actions))
            $actionshtml.=    "<li class=\"list-inline-item\">
                    <a class=\"btn btn-success btn-sm rounded-5\" href=".url('edit'.$url.'/'.$id)." data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><i class=\"fa fa-edit\"></i></a>
                </li>";
        if(in_array("V",$all_actions))        
            $actionshtml.=    "<li class=\"list-inline-item\">
                    <a class=\"btn btn-primary btn-sm rounded-5\" href=".url('view'.$url.'/'.$id)." data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><i class=\"fa fa-eye\"></i></a>
                </li>";
        if(in_array("D",$all_actions))
             $actionshtml.=    "<li class=\"list-inline-item\">
                    <a class=\"btn btn-danger btn-sm rounded-5\" href=".url('delete'.$url.'/'.$id)." data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"><i class=\"fa fa-trash\"></i></a>
                </li>";
        if(in_array("S",$all_actions))
             $actionshtml.=    "<li class=\"list-inline-item\">
                    <a class=\"btn btn-warning btn-sm rounded-5\" href=".url('chstatus'.$url.'/'.$id)." data-toggle=\"tooltip\" data-placement=\"top\" title=\"Status\"><i class=\"fa fa-lock\"></i></a>
                </li>";
        $actionshtml.=     "</ul>";
        if(auth()->user()['is_admin'] != 1){
            echo 'N/A';
        }else
            echo $actionshtml;
    }
}

// if (!function_exists('convertToLocalTime')) {
// function convertToLocalTime($attr) {
    
//     if(Session::has('current_time_zone')){
//        $current_time_zone = Session::get('current_time_zone');
//        $utc = strtotime($attr)-date('Z'); // Convert the time zone to GMT 0. If the server time is what ever no problem.
     
//        $attr = $utc + $current_time_zone; // Convert the time to local time
//        $attr = date("F j, Y, g:i a", $attr);
//     }
//     return $attr;
// }
// }
if (!function_exists('get_local_time')) {
    function get_local_time($attr){
    
        $ip = file_get_contents("http://ipecho.net/plain");
        $url = 'http://ip-api.com/json/'.$ip;
        $tz = file_get_contents($url);
        $tz = json_decode($tz,true)['timezone'];
        // $dt =  date("d F Y H:i:s", $attr);
        $date = new DateTime("@".$attr);  
        $date->setTimezone(new DateTimeZone($tz)); 
        $date = $date->format('F j, Y, g:i a'); 
        
        return $date;

    }
}
if (!function_exists('save_activities')) {
    function save_activities($module, $module_id, $action, $description,$authUser,$user='admin'){
            $data = array(
                        'user_id' => $authUser['id'],
                        'user_type' => $user,
                        'module_id' => $module_id,
                        'description' => $description,
                        'action' => $action,
                        'timestamp' => strtotime('now'),
                    );
            $return = ActivityLogs::create($data);
        
        return true;

    }
}