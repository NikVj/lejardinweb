<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityLogs extends Model
{
    use HasFactory;

    protected $table="activity_logs";
    public $timestamps= true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
    	'user_type',
        'module_id',
        'action',
        'description',
        'timestamp'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }



}