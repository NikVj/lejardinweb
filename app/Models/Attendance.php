<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $table      ="attendance";
    public $timestamps    = true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'children',
        'marked_by',
		'date',
        'day',
        'month',
        'year',
		'timestamp',
        'description',
    ];
}
