<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';

    protected $fillable = [
        'from_user',
		'to_user',
		'content',
		'url_component',
		'section_id',
	];

}
