<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';

    protected $fillable = [
        'title',
		'desc',
		'link',
		'posted_by',
		'staff',
		'parent',
	];

    public function posted_by(){ 

        return $this->hasOne('App\Models\User','id');
    }
}
