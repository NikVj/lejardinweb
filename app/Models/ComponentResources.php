<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ComponentResources extends Model
{
    use HasFactory,Notifiable;

    protected $table="component_resources";
    public $timestamps= true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'designation_id',
        'document_type',
    	'topic',
    	'additional_info',
        'link',
        'file'
    ];

    // public function component()
    // {
    //     return $this->hasMany(Component::class,'id');
    // }

}