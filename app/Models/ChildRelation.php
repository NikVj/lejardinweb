<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChildRelation extends Model
{
    use HasFactory;
    // use SoftDeletes;
    protected $table      ="child_relations";
    public $timestamps    = true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'rel_type',
        'rel_to',
        'rel_from',
        'status',
    ];
}
