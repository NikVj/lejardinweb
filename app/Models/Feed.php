<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';

    protected $fillable = [
        'title',
		'desc',
		'link',
		'posted_by',
		'privacy',
		'staff',
		'parent',
	];

    public function comments(){ 

        return $this->hasMany('App\Models\FeedComment','feed_id');
    }
}
