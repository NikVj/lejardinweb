<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatGroup extends Model
{
    use HasFactory;
    protected $table      ="chat_groups";
    public $timestamps    = true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'group_name',
        'users',
        'icon',
        'is_group',
        'timestamp',
    ];
}
