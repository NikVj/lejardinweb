<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedComment extends Model
{
    use HasFactory;
    // use SoftDeletes;
    protected $appends = ['comment_added_by'];
    public $timestamps    = true;
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'feed_id',
        'comment',
        'timestamp',
    ];

    public function getCommentAddedByAttribute()
    {
        return  $this->belongsTo('App\Models\User','user_id')->first();
    }

}
