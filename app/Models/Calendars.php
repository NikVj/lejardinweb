<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Calendars extends Model
{
    use HasFactory, Notifiable;

    protected $table      ="calendars";
    public $timestamps    = true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'title',
		'calendar_type',
		'event_type',
		'event_name',
		'description',
		'is_public',
		'status',
		'start_date',
		'end_date',
		'staff',
		'parent',
		'priority',
		'color',
    ];

    protected $dates = ['deleted_at'];
}
