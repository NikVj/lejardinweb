<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use HasFactory, Notifiable;

    protected $table      ="user_details";
    public $timestamps    = true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'git_acc',
		'twitter_acc',
		'linkedin_acc',
		'facebook_acc',
		'outlook_acc',
		'instagram_acc',
		'slack_acc',
		'my_acc',
		'aadhar_no',
		'pan_no',
		'website',
		'other_acc',
		'phone_home',
		'city',
		'state',
		'country',
		'zip',
		'address1',
		'address2',
		'work',
		'education',
		'skill',
    ];
}
