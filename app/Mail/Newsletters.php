<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\EmailTemplate;
use App\Models\EmailType;

class Newsletters extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');

        // DB::enableQueryLog();
        $template       = EmailTemplate::where('email_type','newsletter')->first(); 
            // $query = DB::getQueryLog();
            // dd($query);
        $constants      = EmailType::where(['type'=>'newsletter'])->value('constants');
        $constants      = explode(',', $constants);
        $replace_with   = [ $this->data['TO_NAME'], $this->data['DESCRIPTION'], $this->data['TITLE'], $this->data['POSTED_BY']];
        $body           = str_replace($constants, $replace_with, $template->email_body);
        $mailview=$this->view('email.template')->subject($template->subject)->with(['body' => $body]);
        if(isset($this->data['link']) && $this->data['link']){
            if(is_array(($this->data['link']))){
                $files = ($this->data['link']);
                foreach ($files as $file) { 
                    $mailview->attach($file); 
                }
            }else{
                $mailview->attach($this->data['link']);
            }
        }
        // pr($body);die('build function');
        return $mailview;
    }
}
