<div class="container">
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}"> 
  @if (Route::has('login'))
    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block row">
        @auth
            <a href="{{ url('/home') }}" class="btn btn-primary btn-block text-sm text-gray-700 underline">Home</a>
        @else
            <a href="{{ route('login') }}" class="btn btn-success btn-block text-sm text-gray-700 underline">Log in</a>

            @if (Route::has('register'))
                <!-- <a href="{{ route('register') }}" class="btn btn-warning btn-block text-sm text-gray-700 underline">Register</a> -->
            @endif
        @endauth
    </div>
    @endif
</div>