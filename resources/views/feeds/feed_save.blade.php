@extends('layouts.app')
@section('content')
<?php
	$isEdit = (!empty($feedDetails))? 1 :0;
?>
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="card-body" id="addstaff">
	<form  method="post" data-parsley-validate enctype='multipart/form-data' >
		{{@csrf_field()}}
		@if ($errors->any())
		   <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
        @endif

		@if(!empty($feedDetails->firstname)) 
			<input type="hidden" name="id" value="{{$feedDetails->id}}">
		@endif

		<div class="row py-4">
			<div class="col-md-12 dis-flex">
				<h3><span class="az-content-title">@if(!empty($feedDetails)) Edit @else Add @endif Feed</span></h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="group">
					<input type="text" name="title" class="form-control" value="@if(old('title')) {{old('title')}} @elseif(!empty($feedDetails->title)) {{$feedDetails->title}} @endif" required>
					<span class="highlight"></span>
					<label>Name</label>
				</div>

				<div class="group">
					<input type="text" name="desc" id="desc" class="form-control" value="@if(old('desc')) {{old('desc')}} @elseif(!empty($feedDetails->desc)) {{$feedDetails->desc}} @endif" required>
					<span class="highlight"></span>
					<label>Description</label>
				</div>


        <div class=" form-group">
          <label>Image</label>
        </div>
        <div class="group">
          <input type="file" name="link" id="link" value="@if(old('link')) {{old('link')}} @elseif(!empty($feedDetails->link)){{$feedDetails->link}}@endif" >
          <span class="highlight"></span>
        </div>

        <div class=" form-group">
          <label class="label-radio">Privacy</label>
        </div>
        <div class="form-group d-flex align-items-center">
            <div class="custom-control custom-radio pmd-radio custom-control-inline">
              <input type="radio" id="privacy0" name="privacy" value="0" class="custom-control-input"  {{ $isEdit == 1 ? ($feedDetails->privacy == 0 ? 'checked':'') :'checked'}}  >
              <label class="custom-control-label pmd-radio-ripple-effect" for="privacy0">Visible to specific staff/parent</label>
            </div>
            <div class="custom-control custom-radio pmd-radio custom-control-inline">
              <input type="radio" id="privacy1" name="privacy" value="1" class="custom-control-input"  {{ $isEdit == 1 ? ($feedDetails->privacy == 1 ? 'checked':'') :''}}  >
              <label class="custom-control-label pmd-radio-ripple-effect" for="privacy1">Public (visible to all)</label>
            </div>
        </div><!-- form-group -->
        <div class="form-group row my-30 privacy_dropdowns @if($isEdit == 1 ) @if($feedDetails->privacy == 1) d-none @endif @endif ">
          <div class="col-md-6">
            <div class="children_list" >
                <select name="parent[]" class="form-control select2 select2parent" data-placeholder="Choose Parent" data-parsley-class-handler="#slWrapper" multiple="multiple" >
                  <option label="Choose Parent"></option>
                  @foreach($allparents as $parent)
                    <option value="{{$parent->id}}" {{isset($feedDetails->parent) ? (in_array($parent->id ,explode(',',$feedDetails->parent)) ? 'selected' :'') : ''}}>{{$parent->firstname}} &nbsp;&nbsp; {{$parent->lastname}}</option>
                  @endforeach
                </select>
              </div>
          </div>

          <div class="col-md-6">
            <div class="children_list" >
              <select name="staff[]" class="form-control select2 select2staff" data-placeholder="Choose Staff" data-parsley-class-handler="#slWrapper" multiple="multiple" >
                <option label="Choose Staff"></option>
                @foreach($allstaff as $staff)
                  <option value="{{$staff->id}}" {{isset($feedDetails->staff) ? (in_array($staff->id ,explode(',',$feedDetails->staff)) ? 'selected' :'') : ''}}>{{$staff->firstname}}&nbsp;&nbsp;{{$staff->lastname}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>

		</div>

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>



<script>
  $('input[type=radio][name=privacy]').on('change', function() {
    if($(this).val() == 0){    
      $('.privacy_dropdowns').removeClass('d-none');
    }else{
      $('.privacy_dropdowns').addClass('d-none');
    }
  });
  $(function(){
    'use strict'
    $(document).ready(function(){
      $('.select2staff').select2({
        placeholder: 'Choose Staff'
      });
      $('.select2parent').select2({
        placeholder: 'Choose Parent'
      });
    });
  });

  function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    });
// Create the search box and link it to the UI element.
const input = document.getElementById("place");
const searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
// Bias the SearchBox results towards current map's viewport.
map.addListener("bounds_changed", () => {
  searchBox.setBounds(map.getBounds());
});
let markers = [];
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener("places_changed", () => {
  const places = searchBox.getPlaces();


  if (places.length == 0) {
    return;
  }
// Clear out the old markers.
markers.forEach((marker) => {
  marker.setMap(null);
});
markers = [];
// For each place, get the icon, name and location.
const bounds = new google.maps.LatLngBounds();
places.forEach((place) => {
  if (!place.geometry || !place.geometry.location) {
    console.log("Returned place contains no geometry");
    return;
  }

  document.getElementById("lat").value = place.geometry.location.lat();
  document.getElementById("long").value = place.geometry.location.lng();

  const icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25),
  };
// Create a marker for each place.
markers.push(
  new google.maps.Marker({
    map,
    icon,
    title: place.name,
    position: place.geometry.location,
  })
  );

if (place.geometry.viewport) {
// Only geocodes have viewport.
bounds.union(place.geometry.viewport);
} else {
  bounds.extend(place.geometry.location);
}
});
map.fitBounds(bounds);
});
}

</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe2m3BWawVp5q4maj3Q1_PmH0cYrhvrvY&callback=initAutocomplete&libraries=places&v=weekly" async > </script>

@endsection