@extends('layouts.app')
@section('content')
<div class="container-fluid">
  <div class="row">
    @if(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger mb-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @endif
        <div id="feedslist" class="col-sm-12 col-md-12 col-lg-12">
          <div class="col sm-12 md-12 lg-12">
            <h5 class="breadcrumbs-title"><b>Feeds</b>
              <a href="{{ url('editFeed') }}" class=" medium " ><i class="fa fa-plus img-thumbnail"></i></a>
            </h5>
          </div>
          <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      Feeds
                    </div>
                    <div class="card-body">
                        @foreach($allFeeds as $feed)
                        @php
                          $userdata = userData($feed->posted_by);
                        @endphp
                      <div class="post" data-feedID = "{{$feed->id}}">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{'images/users/'.$userdata->profile}}" alt="user image">
                        <span class="username">
                          <a class="text-success" href="{{url('/viewUser/'.$userdata->id)}}">{{$userdata->firstname}}&nbsp;&nbsp;{{$userdata->lastname}}</a>
                          @if(auth()->user()['is_admin'] == 1 || $feed->posted_by == auth()->user()['id']) 
                          <a href="{{url('deleteFeed/'.$feed->id)}}" class="float-right img-circle btn btn-danger"><i class="fas fa-times"></i></a>
                          <a href="{{url('editFeed/'.$feed->id)}}" class="float-right img-circle btn btn-secondary mx-1"><i class="fa fa-edit"></i></a>
                          @endif
                        </span>
                        <span class="description">Shared @if($feed->privacy == '1') publicly @else privately @endif - {{date('Y-m-d'.' '.'H:i:s', strtotime($feed->created_at))}} </span>
                      </div>
                      <!-- /.user-block -->
                      <b>
                        {{$feed->title}}
                      </b>
                      <p>
                        {{$feed->desc}}
                      </p>
                      @if($feed->link != '' && $feed->link != NULL)
                      <div class="text-center feeds_bg" >
                        <img class="img-thumbnail my-4" style="max-height:400px;width:60%" src ="{{'images/feeds/'.$feed->link}}">
                      </div>
                      @endif
                      <p>
                        <!-- <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a> -->
                        <a href="JavaScript:;" class="like-btn btn btn-default mt-1 text-sm {{$feed->liked == 1 ? 'liked_already' : ''}}" data-feedID = "{{$feed->id}}"><i class="fa fa-thumbs-up mr-1"></i> Like  <span id="number_like_{{$feed->id}}" data-number_feed_like="{{$feed->likeCount}}"> {{$feed->likeCount > 0 ?( '('. $feed->likeCount  .')' ) : ''}} </span></a>
                        <span class="float-right">
                          <a href="JavaScript:;" class="text-sm btn btn-default mt-1" onCLick="$('#comment_section_{{$feed->id}}').toggle(200)">
                            <i class="far fa-comments mr-1"></i> Comments <span id="number_comment_{{$feed->id}}" data-number_feed_comment="{{count($feed->comments)}}"> {{count($feed->comments) > 0 ?( '('.count($feed->comments) .')' ): ''}} </span>
                          </a>
                        </span>
                      </p>
                      <div class="comment_section " id="comment_section_{{$feed->id}}" style="display: none;">
                        <input class="form-control form-control-sm add_comment" data-feedid = "{{$feed->id}}" type="text" placeholder="Type a comment....">
                        <div id="post_comments_wrapper_{{$feed->id}}" class="post_comments_wrapper mt-2">
                        @foreach($feed->comments as $comment)
                          <div class="panel-footer post-comment" id="comment_div_{{$comment->id}}">
                            <div class="comment" data-commentid="comment_{{$comment->id}}">
                              <div class="pull-left comment-image ">
                                <a href="JavaScript:;">
                                  <img src="{{userProfile($comment->comment_added_by['profile'])}}" class=" img-circle img-bordered-sm" alt="">
                                </a>
                              </div>
                              @if($comment->user_id == auth()->user()['id'] || auth()->user()['user_role'] == 1)
                              <span class="pull-right">
                                <a href="JavaScript:;" class="remove-post-comment" onclick="remove_post_comment({{$comment->id}}, {{$feed->id}});" data-commentid="{{$comment->id}}" data-feedid="{{$feed->id}}">
                                  <i class="fa fa-times bold">
                                  </i>
                                </a>
                              </span>
                              @endif
                              <div class="media-body">
                                <p class="no-margin comment-content">
                                  <a href="JavaScript:;">{{$comment->comment_added_by['name']}}</a> {{$comment->comment}}</p>
                                  <p class="no-margin">
                                    <!-- <a href="JavaScript:;" onclick="like_comment(comment_{{$comment->id}},5); return false;">
                                      <small>Like this </small>
                                    </a> -->
                                    <small> {{date('Y-m-dTH:i',$comment->timestamp)}}</small>
                                  </p>
                                </div>
                              </div>
                              <div class="clearfix">
                              </div>
                            </div>
                          @endforeach
                            
                          </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
  </div>
</div>
@endsection