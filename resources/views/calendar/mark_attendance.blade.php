@extends('layouts.app')
@section('content')
<?php
	$isEdit = (!empty($data['attendanceData']))? 1 :0;
?>
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>

<div class="card-body" id="addattendance">
	<form id="addAttendanceForm" method="post" data-parsley-validate enctype='multipart/form-data' >
		{{@csrf_field()}}
		@if ($errors->any())
		   <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
        @endif

		@if(!empty($data['attendanceData']->id)) 
			<input type="hidden" name="id" value="{{$data['attendanceData']->id}}">
		@endif

		<div class="row py-4">
			<div class="col-md-12 dis-flex">
				<h3><span class="az-content-title">@if(!empty($data['attendanceData'])) Edit @else Add @endif Attendance</span></h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
		        <div class="group mg-t-30">
		            <div class="row row-xs">
		              <div class="col-12">
		                <div class="input-group">
		                  <div class="input-group-prepend">
		                    <div class="input-group-text">
		                      <i class="fa fa-calendar"></i>
		                    </div>
		                  </div>
		                  <input type="hidden" name="page" id="page" value="attendance">
		                  <input type="text" disabled="disabled" value="{{date('Y-m-d')}}" class="form-control dtimepicker">
		                  <input type="hidden" name="date" value="{{(!empty($data['attendanceData'])) ?(($data['attendanceData']->date)) : (date('Y-m-d').'')}}" >
		                </div>
		              </div><!-- col-7 -->
		            </div><!-- row -->
		        </div><!-- form-group -->
				<div class="group">
					<input type="text" name="description" id="description" class="form-control" value="@if(old('description')) {{old('description')}} @elseif(!empty($data['attendanceData']->description)) {{$data['attendanceData']->description}} @endif">
					<span class="highlight"></span>
					<label>Description</label>
				</div>

		        <div class="form-group my-30">
		        	<label>Attendance</label>
	                @foreach($data['children'] as $child)
	                <div class="row mb-1">
		                <div class="col-md-6">
			                 {{$child->name}}
			             </div>
			             <div class="col-md-6">
	            			<input type="checkbox" name="children[]" value="{{$child->id}}" id="child_check_{{$child->id}}"  @if($isEdit == 1 && (isset($data['attendanceData']->childrenData['child_'.$child->id]) && $data['attendanceData']->childrenData['child_'.$child->id] == 1)) checked @endif  data-bootstrap-switch="" data-off-text="A" data-on-text="P" data-off-color="danger" data-on-color="success">
	            		</div>
	            	</div>
	                @endforeach
		        </div>
			</div>
		</div>

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>
<script type="text/javascript">
	
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

</script>

@endsection