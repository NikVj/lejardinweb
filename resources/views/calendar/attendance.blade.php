@extends('layouts.app')
@section('content')

  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <div class="az-content az-content-app az-content-contacts pd-b-0">

    @if(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger mb-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @endif
    @php
    if(isset($data['filters']['month'])) 
      $selectedMonth =  $data['filters']['month'] ;
    else
      $selectedMonth = date('m');
    if(isset($data['filters']['year'])) 
      $selectedYear =  $data['filters']['year'] ;
    else
      $selectedYear = date('Y');
    @endphp
        <form  method="post"  >
          <div class="row">
          {{@csrf_field()}}
            <div class="col-md-3 form-group">
              <select name="month" class="custom-select" required>
                <option>--Select Month--</option>
                @php
                $months = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                @endphp
                  @for($i=1;$i<=12;$i++)
                <option value="{{$i}}" @if(isset($data['filters']['month'])) @if( $data['filters']['month'] == $i) Selected @endif @elseif(date('m') == $i) selected @endif >{{$months[$i]}}
                </option>
                @endfor
              </select>
            </div>
            <div class="col-md-3 form-group">
              <select name="year" class="custom-select" required>
                <option>--Select Year--</option>
                @foreach($data['years'] as $eachyear)
                <option value="{{$eachyear->year}}"  @if(isset($data['filters']['year'])) @if( $data['filters']['year'] == $eachyear->year) Selected @endif @elseif(date('Y') == $eachyear->year) selected @endif >{{$eachyear->year}}
                </option>
                @endforeach
              </select>
            </div>
            <div class="col-md-3 form-group">
              <input type="submit" name="" class="btn btn-secondary" value="Filter">
            </div>
          </div>
        </form>

        <div id="stafflist">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title"><b>Attendance Sheet</b>
            @if(auth()->user()['is_admin'] == 1)
              <!-- <a href="{{ url('editAttendance') }}" class=" medium " ><i class="fa fa-user-plus"></i></a> -->
            @endif
            </h5>
          </div>
          @php
          $totalDays = 30;
          if($selectedMonth == 2){
            if(($selectedYear % 4 == 0 && $selectedYear % 100 != 0) || ($selectedYear % 400 == 0 && $selectedYear % 100 == 0)){
              $totalDays = 29;
            }else{
              $totalDays = 28;
            }
          }else if(in_array($selectedMonth,array(1,3,5,7,8,10,12))){
            $totalDays = 31;
          }
          @endphp
          <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="card" style="overflow: auto;">
                    <div class="card-body">
                      <table class="table table-bordered table-hover text-center" style="overflow: auto;max-height: 500px;display: block;">
                        <thead style="">
                            <tr>
                              <th></th>
                              @for($day = 01;$day <= $totalDays;$day++)
                              @php
                              $theDate = $selectedYear.'-'.substr("0000".$selectedMonth, -2).'-'.substr("0000".$day, -2);
                              @endphp
                              <th style="" class="verticalTableHeader"><p>{{$theDate}}</p></th>
                              @endfor
                            </tr>
                        </thead>
                        <tbody style="">
                        @foreach($data['children'] as $child)
                            <tr>
                              <td><b>{{$child->firstname.' '.$child->lastname}}</b></td>
                              @for($day = 01;$day <= $totalDays;$day++)
                              @php
                              if($selectedYear < date('Y') || ($selectedYear == date('Y') && (( $selectedMonth < date('m')) || ( $selectedMonth == date('m') && $day <= date('d'))))) {
                                $theDate = $selectedYear.'-'.substr("0000".$selectedMonth, -2).'-'.substr("0000".$day, -2);
                                $ChildAttendance = 'A';
                                if(isset($data['attendanceData'][$theDate])){
                                  foreach($data['attendanceData'][$theDate] as $dateKey=>$dateValue){
                                    if(isset($dateValue['childrenData']['child_'.$child->id])){
                                      if($dateValue['childrenData']['child_'.$child->id] == '1'){
                                        $ChildAttendance = 'P';
                                      }
                                    }
                                  }
                                }
                              }else{
                                $ChildAttendance = '--';
                              }
                              @endphp
                              <td style="" data-dateChild = "{{$theDate.'_'.$child->id}}"class="{{$ChildAttendance}}_attendanceClass">{{$ChildAttendance}}</td>
                              @endfor
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
  </div>
<style type="text/css">
  .P_attendanceClass{
    background-color: #72ea72;
    background: linear-gradient(140deg, rgba(255, 255, 255, 0) 0%, rgb(249, 249, 249) 10%, rgb(16 137 16) 41%);
    font-weight: 800;
  }
  .A_attendanceClass{
    background-color: #6b0505;
    background: linear-gradient(140deg, rgba(255, 255, 255, 0) 0%, rgb(249, 249, 249) 10%, rgb(107, 5, 5) 41%);
    color: #fff;
    font-weight: 800;
  }
  .verticalTableHeader {
    text-align:center;
    transform: rotate(270deg);

  }
  .verticalTableHeader p {
      margin:0 -100% ;
      display:inline-block;
  }
  .verticalTableHeader p:before{
      content:'';
      width:0;
      padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
      display:inline-block;
      vertical-align:middle;
  }
</style>
@endsection