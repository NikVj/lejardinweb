
<?php
	$isEdit = (!empty($data['attendanceData']->id))? 1 :0;
?>

<!-- Bootstrap Switch -->
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>


		@if(!empty($data['attendanceData']->id)) 
			<input type="hidden" name="id" value="{{$data['attendanceData']->id}}">
		@endif

			<div class="col-md-12">
		        <div class="group mg-t-30">
		            <div class="row row-xs">
		              <div class="col-12">
		                <div class="input-group">
		                  <div class="input-group-prepend">
		                    <div class="input-group-text">
		                      <i class="fa fa-calendar"></i>
		                    </div>
		                  </div>
		                  <input type="hidden" name="page" id="page" value="calendar">
		                  <input type="text" disabled="disabled" value="@if(!empty($data['attendanceData']->date)) {{$data['attendanceData']->date }} @else {{$data['date']}} @endif" class="form-control dtimepicker">
		                  <input type="hidden" name="date" value="@if(!empty($data['attendanceData']->date)) {{$data['attendanceData']->date }} @else {{$data['date']}} @endif" >
		                </div>
		              </div><!-- col-7 -->
		            </div><!-- row -->
		        </div><!-- form-group -->
		    </div>
		    <div class="col-md-12">
				<div class="group">
					<label>Description</label>
					<input type="text" name="description" id="description" class="form-control" value="@if(old('description')) {{old('description')}} @elseif(!empty($data['attendanceData']->description)) {{$data['attendanceData']->description}} @endif">
					<span class="highlight"></span>
				</div>
			</div>
			<div class="col-md-12">
		        <div class="form-group my-30">
		        	<label>Attendance</label>
		        	<div class="scroll_children container">
		                @foreach($data['children'] as $child)
		                <div class="row mb-1">
			                <div class="col-md-6">
				                 {{$child->name}}
				             </div>
				             <div class="col-md-6">
		            			<input type="checkbox" name="children[]" value="{{$child->id}}" id="child_check_{{$child->id}}" @if($isEdit == 1 ) @if((isset($data['attendanceData']->childrenData['child_'.$child->id]) && $data['attendanceData']->childrenData['child_'.$child->id] == 1))) checked @endif @endif data-bootstrap-switch="" data-off-text="A" data-on-text="P" data-off-color="danger" data-on-color="success">
		            		</div>
		            	</div>
		                @endforeach
	            	</div>
		        </div>
			</div>
		

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
<script type="text/javascript">
	
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

</script>