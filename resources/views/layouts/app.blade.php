<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Le Jardin</title>

  <!-- Favicons-->
  <link rel="icon" href="{{asset('images/favicon/favicon-32x32.png')}}" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}"> 
  <link rel="stylesheet" href="{{asset('css/custom.css')}}"> 
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link href="{{ asset('assets/lib/select2/css/select2.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css"/>
 
    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.10/css/perfect-scrollbar.css" />
    <script>
        var base_url = '{{ url("/") }}';
        var token = '{{csrf_token()}}';
        var userRole = '{{auth()->user() ? auth()->user()["user_role"] : 0}}';
    </script>
</head>

        @guest
        <body class="login-page">

            <main>
            <div>
                @yield('content')
            </div>
        @else

    <body class="@if(auth()->user()['is_admin'] != 1) non_admin_body @endif">

        <main>
            <x-header />
            <x-sidebar />
            <div class="wrapper">
                <!-- START MAIN -->
                <div id="main">
                    <!-- START WRAPPER -->
                    <div class="content-wrapper">
                        <!-- START CONTENT -->
                        <section id="content">
                            @yield('content')
                        </section>
                        <!-- END CONTENT -->
                        <x-footer />
                    </div>
                </div>
                <!-- Control Sidebar -->
                <aside class="control-sidebar control-sidebar-dark">
                    <!-- Control sidebar content goes here -->
                </aside>
                <!-- /.control-sidebar -->
            </div>
        @endguest
    </main>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('js/demo.js')}}"></script>

    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.10/js/perfect-scrollbar.jquery.js"></script>
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="{{ asset('assets/js/chat.js') }}"></script>
    <style type="text/css">
        #content{
                padding: 20px;;
        }
        .scroll_children{
          max-height: 150px;
          overflow-y: auto;
        }
    </style>
    <script type="text/javascript">
    $("#addAttendanceForm").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var page = $('input[name="page"]').val();
        var url = "editAttendance<?=((!empty($data['attendanceData']) && isset($data['attendanceData']->id) ) ?('/'.($data['attendanceData']->id)) : '')?>";
        var child_array = [];
        $('input[name="children[]"]').each(function() {
            var attendance = 0;
            if($(this).is(':checked'))
                attendance = 1;
                child_array.push({
                    child: this.value, 
                    attendance:  attendance
                });
        });
        data={
            'date':$('input[name="date"]').val(),
            'day':$('#clickedDay').val(),
            'month':$('#clickedMonth').val(),
            'year':$('#clickedYear').val(),
            'children':JSON.stringify(child_array),
            'description':$('input[name="description"]').val(),
            '_token':'{{csrf_token()}}',
        }
        $.ajax({
               type: "POST",
               url: url,
               data: data, // serializes the form's elements.
               success: function(data)
               {
                data = JSON.parse(data);

                   if(data.type == 'Success'){
                    toastr.success(data.message);
                    if(page == 'attendance'){
                        window.location.href = "attendance";
                    }else{
                        location.reload();
                    }
                   }else{
                    toastr.error(data.message);
                   } // show response from the php script.
               }
             });

        
    });
    </script>
</body>
    <audio id="chat-alert-sound" style="display: none">
        <source src="{{ asset('assets/sound/chat_sound.mp3') }}" />
    </audio>
    @yield('script')
</html>