<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Le Jardin</title>
</head>
<body style="padding: 0px 0px; margin: 0px 0px;">
    <table width="100%" bgcolor="#f4f4f4" cellpadding="0" cellspacing="0" border="0" style="padding: 15px;">
        <tr>
            <td>
                <table align="center" width="650" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #f3f3f3;">
                    <tr>
                        <td style="padding: 10px 15px; text-align: center"><img src="{{asset('images/logo.png')}}" alt="logo" width="190px"></td>
                    </tr>
                 <!---   <tr>
                        <td colspan="2"><img src="http://182.156.245.82:8080/mahesh_d/emailer-test/gotawseel/tgs_banner.jpg" alt="Go Tawseel"></td>
                    </tr>-->
                    <tr>
                        <td colspan="2" style="font-family: Arial; font-size: 15px; color: #667180; line-height: 25px; padding: 8px 20px;">
                            {!! $body !!}
                        </td>
                    </tr>
              
                </table>
            </td>
        </tr>
    </table>
</body>
</html>