@extends('layouts.app')

@section('content')


<style>
    .invalid-feedback {
        display: block;
    }
</style>

<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Le Jardin</b></a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
          <div class="input-field col s12 center">
            <h4>{{ __('Reset Password') }}</h4>
            <p class="center">You can reset your password</p>
          </div>
        @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
        @endif

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">
        <div class="row margin mb-3">
          <div class="input-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
            <label for="email" class="center-align"></label>
                <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                </div>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
          </div>
        </div>
        <div class="row margin mb-3">       
          <div class="input-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
              @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              @enderror
          </div>
        </div>
        <div class="row margin mb-3">            
          <div class="input-group">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
                @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <button type="submit" class="btn btn-primary btn-block">
                        {{ __('Reset Password') }}
                    </button>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            @if (Route::has('password.request'))
            <a class="float-right " href="{{ route('login') }}">
                {{ __("Sign In") }}
            </a>
            @endif
          </div>
        </div>
      </form>
    </div>
  </div>




@endsection
