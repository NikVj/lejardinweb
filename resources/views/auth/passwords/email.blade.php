@extends('layouts.app')

@section('content')

<div class="login-box">
  <div class="login-logo">
    <a href="/" class="outer_brand_logo">
      <img src="{{asset('images/logo.jpg')}}" alt="Le Jardin Logo" class="brand-image img-circle elevation-3" style="opacity: 1"><b>Le Jardin</b>
    </a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
        <div class="input-field col s12 center">
            <p class="login-box-msg">{{ __('Reset Password') }}</p>
        </div>

        @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="input-group mb-4">

                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  required autocomplete="off" autofocus placeholder="{{ __('E-Mail Address') }}">
                <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                </div>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div>
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
        </form>

        <div class="az-signin-footer mt-3 text-center">
            @if (Route::has('password.request'))
            <a class="" href="{{ route('login') }}">
                {{ __("Sign In") }}
            </a>
            @endif
            <!-- <a href="{{ route('register') }}" class="float-right">Sign up for an account</a> -->
        </div>
    </div>
</div>
</div>

@endsection