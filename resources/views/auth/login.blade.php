@extends('layouts.app')

@section('content')

<style>
    .invalid-feedback {
        display: block;
    }
</style>

<div class="login-box">
  <div class="login-logo">
    <a href="/" class="outer_brand_logo">
      <img src="{{asset('images/logo.jpg')}}" alt="Le Jardin Logo" class="brand-image img-circle elevation-3" style="opacity: 1"><b>Le Jardin</b>
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
            @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
            @endif
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="input-group mb-3">
              <input  id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="off" autofocus placeholder="{{ __('E-Mail Address') }}">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>

              @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="input-group mb-3">
              <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off" placeholder="{{ __('Password') }}">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
              @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>

            <div class="row">
              <!-- <div class="col-8">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember" name="remember"  {{ old('remember') ? 'checked' : '' }}>
                  <label for="remember">
                    Remember Me
                  </label>
                </div>
              </div> -->
              <!-- /.col -->
              <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">{{ __('Login') }}</button>
              </div>
              <!-- /.col -->
            </div>
          </form>


       <!--  @if (Route::has('password.request'))
          <p class="mb-1">
            <a href="{{ route('password.request') }}">{{ __("Can't log in?") }}</a>
          </p>
          @endif -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>


@endsection