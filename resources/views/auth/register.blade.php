@extends('layouts.app')

@section('content')

    <div class="login-logo">
        <a href="/"><b>Le Jardin</b></a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <div class="az-column-signup">
                <h3 class="az-logo">Super Admin</h3>
                <div class="az-signup-header">
                    <p>It's free to signup and only takes a minute.</p>

                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">

                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{ __('Name') }}">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">

                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                        </div>

                        <div class="form-group mb-0">
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="az-signup-footer">
                    <p class="mt-2">Already have an account?
                        <a href="{{ route('login') }}">
                            {{ __('Sign In') }}
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection