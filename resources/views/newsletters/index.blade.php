@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
    @if(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger mb-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @endif
        <div id="locationlist" class="col-sm-12 col-md-12 col-lg-12">
          <div class="col sm-12 md-12 lg-12">
            <h5 class="breadcrumbs-title"><b>Newsletters</b>
            @if(auth()->user()['is_admin'] == 1)
              <a href="{{ url('editNewsletter') }}" class=" medium " ><i class="fa fa-plus img-thumbnail"></i></a>
            @endif
            </h5>
          </div>
          <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="table table-bordered table-hover ListAll">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Attachment</th>
                                <th>Recipients</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($allNewsletter as $eachnewsletter)
                            <tr>
                                <td>{{$eachnewsletter->title}}</td>
                                <td>{{$eachnewsletter->desc}}</td>
                                <td>
                                  <a href="files/newsletter/{{$eachnewsletter->link}}">{{$eachnewsletter->link}}</a>
                                </td>
                                <td>
                                @if(auth()->user()['is_admin'] == 1||$eachnewsletter->posted_by == auth()->user()['id'])
                                  <div class=""> 
                                    @php 
                                    $recCount = 1;
                                    @endphp
                                    @foreach($eachnewsletter->recipients as $rec)
                                    <a href="{{url('viewUser/'.$rec['id'])}}"><img src="{{asset('images/users/'.$rec['profile'])}}" class="img-circle my-1 <?= (($recCount > 3) ? 'morerec' : '') ?> " style="width:38px; <?= (($recCount > 3) ? 'display: none' : '') ?> " title="{{$rec['name']}}"></a>
                                    @if($recCount == 4)
                                      <button href="JavaScript:;" class="btn btn-xs btn-outline btn-dark" onclick="$(this).parent().find('.morerec').toggle(200);$(this).toggle()">{{count($eachnewsletter->recipients) - 3}} MORE</button>
                                    @endif
                                    @php 
                                    $recCount++;
                                    @endphp
                                    @endforeach
                                  </div>
                                  @else
                                  No Recipients shown
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
	</div>
</div>
@endsection