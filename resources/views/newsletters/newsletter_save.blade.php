@extends('layouts.app')
@section('content')
<?php
	$isEdit = (!empty($newsletterDetails))? 1 :0;
?>
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="card-body" id="addnewsletter">
	<form  method="post" data-parsley-validate enctype='multipart/form-data' >
		{{@csrf_field()}}
		@if ($errors->any())
		   <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
        @endif

		@if(!empty($newsletterDetails->firstname)) 
			<input type="hidden" name="id" value="{{$newsletterDetails->id}}">
		@endif

		<div class="row py-4">
			<div class="col-md-12 dis-flex">
				<h3><span class="az-content-title">@if(!empty($newsletterDetails)) Edit @else Add @endif Newsletter</span></h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="group">
					<input type="text" name="title" class="form-control" value="@if(old('title')) {{old('title')}} @elseif(!empty($newsletterDetails->title)) {{$newsletterDetails->title}} @endif" required>
					<span class="highlight"></span>
					<label>Name</label>
				</div>

				<div class="group">
					<input type="text" name="desc" id="desc" class="form-control" value="@if(old('desc')) {{old('desc')}} @elseif(!empty($newsletterDetails->desc)) {{$newsletterDetails->desc}} @endif" required>
					<span class="highlight"></span>
					<label>Description</label>
				</div>


        <div class=" form-group">
          <label>Attachment</label>
        </div>
        <div class="group">
          <input type="file" name="link" id="link" value="@if(old('link')) {{old('link')}} @elseif(!empty($newsletterDetails->link)){{$newsletterDetails->link}}@endif" link>
          @if(!empty($newsletterDetails->link))
          <a href="@if(!empty($newsletterDetails->link)) {{'files/newsletter/'.$newsletterDetails->link}} @endif">{{$newsletterDetails->link}}</a>
          @endif
          <span class="highlight"></span>
        </div>
        <div class="form-group row my-30">
          <div class="col-md-6">
            <div class="children_list" >
                <select name="parent[]" class="form-control select2 select2parent" data-placeholder="Choose Parent" data-parsley-class-handler="#slWrapper" multiple="multiple" >
                  <option label="Choose Child"></option>
                  @foreach($allparents as $parent)
                    <option value="{{$parent->id}}" >{{$parent->firstname.' '.$parent->lastname}}</option>
                  @endforeach
                </select>
              </div>
          </div>

          <div class="col-md-6">
            <div class="children_list" >
              <select name="staff[]" class="form-control select2 select2staff" data-placeholder="Choose Staff" data-parsley-class-handler="#slWrapper" multiple="multiple" >
                <option label="Choose Staff"></option>
                @foreach($allstaff as $staff)
                  <option value="{{$staff->id}}">{{$staff->firstname.' '.$staff->lastname}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>

		</div>

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>



<script>
  $(function(){
    'use strict'
    $(document).ready(function(){
      $('.select2staff').select2({
        placeholder: 'Choose Staff'
      });
      $('.select2parent').select2({
        placeholder: 'Choose Parent'
      });
    });
  });

  function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    });
// Create the search box and link it to the UI element.
const input = document.getElementById("place");
const searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
// Bias the SearchBox results towards current map's viewport.
map.addListener("bounds_changed", () => {
  searchBox.setBounds(map.getBounds());
});
let markers = [];
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener("places_changed", () => {
  const places = searchBox.getPlaces();


  if (places.length == 0) {
    return;
  }
// Clear out the old markers.
markers.forEach((marker) => {
  marker.setMap(null);
});
markers = [];
// For each place, get the icon, name and location.
const bounds = new google.maps.LatLngBounds();
places.forEach((place) => {
  if (!place.geometry || !place.geometry.location) {
    console.log("Returned place contains no geometry");
    return;
  }

  document.getElementById("lat").value = place.geometry.location.lat();
  document.getElementById("long").value = place.geometry.location.lng();

  const icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25),
  };
// Create a marker for each place.
markers.push(
  new google.maps.Marker({
    map,
    icon,
    title: place.name,
    position: place.geometry.location,
  })
  );

if (place.geometry.viewport) {
// Only geocodes have viewport.
bounds.union(place.geometry.viewport);
} else {
  bounds.extend(place.geometry.location);
}
});
map.fitBounds(bounds);
});
}

</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe2m3BWawVp5q4maj3Q1_PmH0cYrhvrvY&callback=initAutocomplete&libraries=places&v=weekly" async > </script>

@endsection