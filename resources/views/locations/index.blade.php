@extends('layouts.app')
@section('content')

<!--  "DisplayText": "adcv", "ADDRESS": "Jamiya Nagar Kovaipudur Coimbatore-641042", "LatitudeLongitude": "10.9435131,76.9383790", "MarkerId": "Customer"  -->

@php
  $locArray = array();
@endphp
@foreach($allLocation as $locKey=>$loc)
  @php
    $locArray[$locKey]['DisplayText'] = $loc->name;
    $locArray[$locKey]['ADDRESS'] = $loc->place;
    $locArray[$locKey]['LatitudeLongitude'] = $loc->lat.','.$loc->long;
    $locArray[$locKey]['MarkerId'] = 'Customer';
  @endphp
@endforeach

<div class="container-fluid">
	<div class="row">
    @if(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger mb-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div id="locationlist" class="col-sm-12 col-md-12 col-lg-12">
      <div class="col sm-12 md-12 lg-12">
        <h5 class="breadcrumbs-title"><b>Locations</b>
        @if(auth()->user()['is_admin'] == 1)
          <a href="{{ url('editLocation') }}" class=" medium " ><i class="fa fa-plus img-thumbnail"></i></a>
        @endif
        </h5>
      </div>
      <ul class="nav nav-pills my-2">
        <li class="nav-item">
          <a class="nav-link active" href="#list" data-toggle="pill">List</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#mapView" data-toggle="pill">Map</a>
        </li>
      </ul>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <div class="tab-content">
                    <div id="list" class="tab-pane  active">
                      <table class="table table-bordered table-hover ListAll">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Place</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($allLocation as $loc)
                            <tr>
                                <td>{{$loc->name}}</td>
                                <td>{{$loc->place}}</td>
                                <td>
                                  @if($loc->status == 1)
                                  <label class="badge badge-success"> Active</label>
                                  @else
                                  <label class="badge badge-danger"> Deactivated</label>
                                  @endif
                                </td>
                                <td>{{getactionicons($loc->id,'Location','ED')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                    <div id="mapView" class="tab-pane fade">
                      <div id="map-canvas" style="width: 800px; height: 500px;">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe2m3BWawVp5q4maj3Q1_PmH0cYrhvrvY&sensor=true" type="text/javascript"></script>

<script type="text/javascript">
        var map;
        var geocoder;
        var marker;
        var people = new Array();
        var latlng;
        var latChanged=11.0168445;
        var longChanged=76.9558321;
        var infowindow;

        $(document).ready(function() {
          initGeolocation();
            // ViewCustInGoogleMap();
        });

        function ViewCustInGoogleMap() {

            var mapOptions = {
                // center: new google.maps.LatLng(11.0168445, 76.9558321),   // Coimbatore = (11.0168445, 76.9558321)
                center: new google.maps.LatLng(latChanged, longChanged),   // Coimbatore = (11.0168445, 76.9558321)
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            // Get data from database. It should be like below format or you can alter it.

            // var data = '[{ "DisplayText": "adcv", "ADDRESS": "Jamiya Nagar Kovaipudur Coimbatore-641042", "LatitudeLongitude": "10.9435131,76.9383790", "MarkerId": "Customer" },{ "DisplayText": "abcd", "ADDRESS": "Coimbatore-641042", "LatitudeLongitude": "11.0168445,76.9558321", "MarkerId": "Customer"}]';
            var data = '<?=json_encode($locArray)?>';;

            people = JSON.parse(data); 

            for (var i = 0; i < people.length; i++) {
                setMarker(people[i]);
            }

        }

        function setMarker(people) {
            geocoder = new google.maps.Geocoder();
            infowindow = new google.maps.InfoWindow();
            if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
                geocoder.geocode({ 'address': people["Address"] }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                        marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            draggable: false,
                            html: people["DisplayText"],
                            icon: "images/marker/" + people["MarkerId"] + ".png"
                        });
                        //marker.setPosition(latlng);
                        //map.setCenter(latlng);
                        google.maps.event.addListener(marker, 'click', function(event) {
                            infowindow.setContent(this.html);
                            infowindow.setPosition(event.latLng);
                            infowindow.open(map, this);
                        });
                    }
                    else {
                        alert(people["DisplayText"] + " -- " + people["Address"] + ". This address couldn't be found");
                    }
                });
            }
            else {
                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false,               // cant drag it
                    html: people["DisplayText"]    // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });
            }
        }
     function initGeolocation()
     {
        if( navigator.geolocation )
        {
           // Call getCurrentPosition with success and failure callbacks
           navigator.geolocation.getCurrentPosition( success, fail );
        }
        else
        {
           alert("Sorry, your browser does not support geolocation services.");
        }
     }

     function success(position)
     {
         longChanged = position.coords.longitude;
         latChanged = position.coords.latitude
         ViewCustInGoogleMap();
     }

     function fail()
     {
        // Could not obtain location
     }

</script>
@endsection