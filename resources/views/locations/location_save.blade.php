@extends('layouts.app')
@section('content')
<?php
	$isEdit = (!empty($locDetails))? 1 :0;
?>
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="card-body" id="addstaff">
	<form  method="post" data-parsley-validate enctype='multipart/form-data' >
		{{@csrf_field()}}
		@if ($errors->any())
		   <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
        @endif

		@if(!empty($locDetails->name)) 
			<input type="hidden" name="id" value="{{$locDetails->id}}">
		@endif

		<div class="row py-4">
			<div class="col-md-12 dis-flex">
				<h3><span class="az-content-title">@if(!empty($locDetails)) Edit @else Add @endif Location</span></h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="group">
					<input type="text" name="name" class="form-control" value="@if(old('name')) {{old('name')}} @elseif(!empty($locDetails->name)) {{$locDetails->name}} @endif" required>
					<span class="highlight"></span>
					<label>Name</label>
				</div>

				<div class="group">
					<input type="text" name="place" id="place" class="form-control" value="@if(old('place')) {{old('place')}} @elseif(!empty($locDetails->place)) {{$locDetails->place}} @endif" required>
					<span class="highlight"></span>
					<label>Place</label>
					<input type="hidden" name="lat" id="lat" value="@if(old('lat')) {{old('lat')}} @elseif(!empty($locDetails->lat)){{$locDetails->lat}}@endif" required>
					<input type="hidden" name="long" id="long" value="@if(old('long')) {{old('long')}} @elseif(!empty($locDetails->long)){{$locDetails->long}}@endif" required>

                    <div id="map"></div>
				</div>
			</div>

		</div>

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>



<script>

  function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    });
// Create the search box and link it to the UI element.
const input = document.getElementById("place");
const searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
// Bias the SearchBox results towards current map's viewport.
map.addListener("bounds_changed", () => {
  searchBox.setBounds(map.getBounds());
});
let markers = [];
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener("places_changed", () => {
  const places = searchBox.getPlaces();


  if (places.length == 0) {
    return;
  }
// Clear out the old markers.
markers.forEach((marker) => {
  marker.setMap(null);
});
markers = [];
// For each place, get the icon, name and location.
const bounds = new google.maps.LatLngBounds();
places.forEach((place) => {
  if (!place.geometry || !place.geometry.location) {
    console.log("Returned place contains no geometry");
    return;
  }

  document.getElementById("lat").value = place.geometry.location.lat();
  document.getElementById("long").value = place.geometry.location.lng();

  const icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25),
  };
// Create a marker for each place.
markers.push(
  new google.maps.Marker({
    map,
    icon,
    title: place.name,
    position: place.geometry.location,
  })
  );

if (place.geometry.viewport) {
// Only geocodes have viewport.
bounds.union(place.geometry.viewport);
} else {
  bounds.extend(place.geometry.location);
}
});
map.fitBounds(bounds);
});
}

</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe2m3BWawVp5q4maj3Q1_PmH0cYrhvrvY&callback=initAutocomplete&libraries=places&v=weekly" async > </script>

@endsection