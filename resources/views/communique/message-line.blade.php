<div class="direct-chat-primary">
 @if($message->from_user == \Auth::user()->id)

                      <div class="direct-chat-msg right msg_container base_sent" data-message-id="{{ $message->id }}">
                        <div class="direct-chat-infos clearfix">
                            <div class="messages msg_sent float-right">
                                <span class="direct-chat-name ">{{ $message->fromUserName }}</span>
                                <!-- <time datetime="{{ date("Y-m-dTH:i", strtotime($message->timestamp)) }}" class="direct-chat-timestamp float-left">{{ $message->fromUserName }} • {{ $message->timestamp }}</time> -->
                            </div>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img avatar" src="images/users/{{ $message->fromUserImage }}" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text float-right">
                            @if($message->type == 'image')
                                <img src="{!! $message->content !!}">
                            @else
                                {!! $message->content !!}
                            @endif
                        </div>
                        <!-- /.direct-chat-text -->
                      </div>
    

@else
                      <div class="direct-chat-msg msg_container base_receive" data-message-id="{{ $message->id }}">
                        <div class="direct-chat-infos clearfix">
                            <div class="messages msg_receive float-left">
                                <span class="direct-chat-name ">{{ $message->fromUserName }}</span>
                                <!-- <time class="direct-chat-timestamp float-right" datetime="{{ date("Y-m-dTH:i", strtotime($message->timestamp)) }}">{{ $message->fromUserName }} • {{ $message->timestamp }}</time> -->
                            </div>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <div class="avatar">
                            <img class="direct-chat-img img-responsive" src="images/users/{{ $message->fromUserImage }}" alt="message user image">
                        </div>
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text float-left">
                            @if($message->type == 'image')
                                <img src="{!! $message->content !!}">
                            @else
                                {!! $message->content !!}
                            @endif
                        </div>
                        <!-- /.direct-chat-text -->
                      </div>


@endif
</div>