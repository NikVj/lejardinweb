@extends('layouts.app')
@section('content')
              <div class="card">
                <div class="container-fluid" style="height: 560px;">
                  <div class="row">

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts col-md-3 col-sm-11">
                      <ul class="contacts-list" id="users">
                        Recent Chats
                        <chatusers class="recent_chats_head">
                        @if(count($users) > 0)
                        @foreach($users as $user)
                            <li id="user_li_{{ ($user->from_user != auth()->user()['id']) ? $user->from_user : $user->to_user }}" class="recent_chats">
                              <a href="javascript:void(0);" class="chat-toggle" data-group_id="{{ $user->group_id }}"  data-id="{{ ($user->from_user != auth()->user()['id']) ? $user->from_user : $user->to_user }}" data-user="{{ ($user->from_user != auth()->user()['id'] ) ? $user->sender_name : $user->receiver_name }}">
                              <img class="contacts-list-img" src="{{(($user->from_user != auth()->user()['id'] ) ? $user->sender_profile : $user->receiver_profile)}}" alt="User Avatar">
                                <div class="contacts-list-info">
                                  <span class="contacts-list-name">{{ ($user->from_user != auth()->user()['id'] ) ? $user->sender_name : $user->receiver_name }}
                                    <?= ($user->unreadCount > 0) ? '<small class="unread_count" id="unread_count_'.$user->group_id.'" data-unreadCount = '.$user->unreadCount.'>'. $user->unreadCount. '</small>' : '' ?>
                                    <small class="contacts-list-date float-right">{{ date('d/m/y',$user->timestamp) }}</small>
                                  </span> 
                                  <span class="contacts-list-msg">

                                    @if($user->type == 'image')
                                        <i class="fa fa-image"></i> &nbsp;&nbsp;Image 
                                    @else
                                    {{ $user->content }}
                                    @endif
                                  </span>
                                </div>
                              </a>
                            </li>
                        @endforeach
                        @else
                          <li class="norecents">No Recent Chats Found!!</li>
                        @endif
                        </chatusers>
                        Other Users
                        <chatusers class="new_chats_head">
                        @foreach($allusers as $eachuser)
                            <li id="user_li_{{ $eachuser->id }}" class="new_chats">
                              <a href="javascript:void(0);" class="chat-toggle" data-group_id=""  data-id="{{ $eachuser->id }}" data-user="{{ $eachuser->firstname.' '.$eachuser->lastname }}">
                              <img class="contacts-list-img" src="{{($eachuser->profile_image)}}" alt="User Avatar">
                                <div class="contacts-list-info">
                                  <span class="contacts-list-name">{{ $eachuser->firstname.' '.$eachuser->lastname }}
                                  </span> 
                                </div>
                                  <span class="contacts-list-msg">Say Hi to start Conversation</span>
                              </a>
                            </li>
                        @endforeach
                        </chatusers>
                    
                        <!-- End Contact Item -->
                      </ul>
                      <!-- /.contacts-list -->
                    </div>
                    <!-- /.direct-chat-pane -->
                    <div class="col-md-9 col-sm-12 chats_container">
                      <div id="chat-overlay" class="my-4 mx-1">
                        @include('communique/chat-box')
                      </div>
                    </div>
                    <input type="hidden" id="current_user" value="{{ \Auth::user()->id }}" />
                    <input type="hidden" id="pusher_app_key" value="{{ env('PUSHER_APP_KEY') ? env('PUSHER_APP_KEY'):'9cfa4d42c4a33e35be86' }}" />
                    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') ? env('PUSHER_APP_CLUSTER'):'ap2' }}" />
                  </div>
                </div>
              </div>


    

    <script src="{{ asset('assets/lib/lightslider/js/lightslider.min.js')}}"></script>

@endsection