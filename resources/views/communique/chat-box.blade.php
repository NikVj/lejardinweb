<div id="chat_box" class="chat_box pull-right container" style="display: none;height: 500px;">
    <div class="row">
        <div class="col-xs-12 col-md-12">
                <div class="panel panel-default" style="position:relative;">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <svg onclick="chatBackMobile(this)" data-toggle="tooltip" title="" class="chat_back_mobile" viewBox="0 0 24 24" data-original-title="Back">
                              <path d="M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z"></path>
                            </svg>
                            <span class="glyphicon glyphicon-comment"></span> <span>Chat with <i class="chat-user"></i> </span>
                        </h3>
                        <span class="glyphicon glyphicon-remove pull-right close-chat"></span>
                    </div>
                    <div class="panel-body chat-area">

                    </div>
                    <div class="panel-footer" style="position: relative;margin-top: 20px;/*bottom: 77px;width: 56%;*/">
                        <div class="input-group form-controls">
                            <textarea class="form-control input-sm chat_input" placeholder="Type here..."></textarea>
                            <span class="input-group-btn ">
                                    <button class="btn btn-danger btn-sm btn-file m-1" type="button" data-to-user="" onclick="$(this).parent().find('.chatFileInput').trigger('click');">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </button>
                                        <input type="file" name="chat_file" class="chatFileInput d-none" accept="image/*">
                                    <div class="clearfix"></div>
                                    <button class="btn btn-primary btn-sm btn-chat m-1" type="button" data-to-user="" disabled>
                                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                        </button>
                                </span>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <input type="hidden" id="to_user_id" value="" />
          {{@csrf_field()}}
</div>