@extends('layouts.app')

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (For parent & staff) -->
        <div class="card">
          <div class="card-header">
            <b>Check Your Components</b>
          </div>
          <div class="card-body bg-light">
            <div class="row">
              <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">

                    <p>Disability</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-wheelchair"></i>
                  </div>
                  <a href="{{url('disability')}}" class="small-box-footer">Go <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-secondary">
                  <div class="inner">

                    <p>Education</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-book"></i>
                  </div>
                  <a href="{{url('education')}}" class="small-box-footer">Go <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">

                    <p>Health/Nutrition</p>
                  </div>
                  <div class="icon">
                    <!-- <i class="fab fa-nutritionix"></i> -->
                    <i class="fas fa-ambulance"></i>
                  </div>
                  <a href="{{url('health-nutrition')}}" class="small-box-footer">Go <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">

                    <p>Family Services</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-child"></i>
                  </div>
                  <a href="{{url('family-services')}}" class="small-box-footer">Go <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <!-- ./col -->
              <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">

                    <p>Mental Health</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-brain"></i>
                  </div>
                  <a href="{{url('mental-health')}}" class="small-box-footer">Go <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <!-- ./col -->
              <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                  <div class="inner">

                    <p>Internal Report</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="{{url('internal-report')}}" class="small-box-footer">Go <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
            </div>
          </div>
        </div>
        <!-- /.row -->
                    <!--chart dashboard end-->
        <div class="card mt-3">
            <x-calendar />
        </div>

      </div><!-- /.container-fluid -->
    </section>
    
    <!-- chartist -->
    <script type="text/javascript" src="{{asset('js/plugins/chartist-js/chartist.min.js')}}"></script>   

    <!-- chartjs -->
    <script type="text/javascript" src="{{asset('js/plugins/chartjs/chart.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/chartjs/chart-script.js')}}"></script>   
@endsection