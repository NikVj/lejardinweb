@extends('layouts.app')
@section('content')

    <!-- Main content -->
    <section class="content">

    @if ($errors->any())
    <div class="alert alert-danger mg-b-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @elseif(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{(($userDetails->profile != '') ? (asset('images/users/'.$userDetails->profile)) : (asset('images/user.png')))}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$userDetails->firstname}}</h3>
                <h3 class="profile-username text-center">{{$userDetails->lastname}}</h3>


                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right" href="mailto:{{$userDetails->email}}">{{$userDetails->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right">{{$userDetails->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone (Home)</b> <a class="float-right">{{$userDetails->phone_home}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Current Address</b> <a class="float-right">{{$userDetails->address1}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Permanent Address</b> <a class="float-right">{{$userDetails->address2}}</a>
                  </li>
                </ul>

                <a href="{{url('editUser/'.$userDetails->user_id)}}" class="btn btn-primary btn-block"><b>Edit</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          <div class="col-md-9">
            @if($userDetails->user_role != '1')
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#staff_children" data-toggle="tab">Children</a></li>
                  <li class="nav-item"><a class="nav-link" href="#staff_events" data-toggle="tab">Events</a></li>
                  @if($userDetails->user_role == '2')
                  <li class="nav-item"><a class="nav-link" href="#staff_degnation" data-toggle="tab">Designations</a></li>
                  @endif
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="staff_children">
                    <div class="post">
                      @php
                      $childnum = 0;
                      $allChild = children($userDetails->id,1);
                      @endphp
                      @if(count($allChild) > 0)
                        @foreach($allChild as $perchild)
                        @php
                        $childnum++;
                        @endphp
                            <div><b>#{{$childnum}}</b></div>
                        <div class="user-block">
                          <span class="username">
                            <a href="#" class="{{$perchild->gender == 1 ? 'text-primary' : 'text-danger'}}">{{$perchild->firstname}} &nbsp;&nbsp; {{$perchild->lastname}}</a>
                            <p>{{$perchild->email}}</p>
                          </span>
                        </div>
                        @endforeach
                      @else
                        No Child Added Yet!!
                      @endif
                    </div>
                    <!-- /.post -->

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="staff_degnation">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse mb-2">
                      <!-- timeline time label -->
                      <div class="time-label">
                          <b>Designation Name</b>
                      </div>
                      <!-- /.timeline-label -->
                      <!-- END timeline item -->
                    </div>
                      <!-- timeline item -->
                      <div class="m-4">
                        @if(is_object(userDesignations($userDetails->designation)))
                        <div class="timeline-item">
                          <span href="#">{{ is_object(userDesignations($userDetails->designation)) ? (userDesignations($userDetails->designation)->designation_name) : '' }}</span>
                        </div>
                        @else
                          No Designation added Yet!
                        @endif
                      </div>
                  </div>
                  <!-- /.tab-pane -->
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="staff_events">
                    @if(!empty($events))
                      @if(!empty($events[1]))
                        @foreach($events[1] as $perevent)
                        <!-- The timeline -->
                        <div class="timeline timeline-inverse">
                          <!-- timeline time label -->
                          <div class="time-label">
                            <span class="bg-danger">
                              Event Name - {{$perevent['title']}}
                            </span>
                          </div>
                          <!-- /.timeline-label -->
                          <!-- timeline item -->
                          <div>
                            <i class="fas fa-calendar bg-primary"></i>

                            <div class="timeline-item">
                              <span href="#">
                              <b> {{$perevent['description']}} </b></span>
                              <hr/>
                              <p><b>Start Date :: </b>
                              {{date('Y-m-d'.'\T'.'H:i:s', strtotime($perevent['start']))}}</p>
                              <p><b>End Date :: </b>
                              {{date('Y-m-d'.'\T'.'H:i:s', strtotime($perevent['end']))}}</p>
                            </div>
                          </div>
                          <!-- END timeline item -->
                        </div>
                        @endforeach
                      @endif
                      @if(!empty($events[2]))
                        @foreach($events[2] as $perevent)
                          <!-- The timeline -->
                          <div class="timeline timeline-inverse">
                            <!-- timeline time label -->
                            <div class="time-label">
                              <span class="bg-danger">
                                Event Name - {{$perevent['title']}}
                              </span>
                            </div>
                            <!-- /.timeline-label -->
                            <!-- timeline item -->
                            <div>
                              <i class="fas fa-envelope bg-primary"></i>

                              <div class="timeline-item">
                                <span href="#">
                                <b> {{$perevent['description']}} </b></span>
                                <hr/>
                                <p>Start Date
                                {{$perevent['start']}}</p>
                                <p>End Date
                                {{$perevent['end']}}</p>
                              </div>
                            </div>
                            <!-- END timeline item -->
                          </div>
                        @endforeach
                      @endif
                    @else
                      No Events Found!!
                    @endif
                  <!-- /.tab-pane -->

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            @elseif($userDetails->user_role == '3')
            <div class="card">
              <div class="card-header p-2">
                Children
              </div>
              <div class="card-body">
                <div class="post">
                  @php
                  $childnum = 0;
                  @endphp
                  @foreach(children($userDetails->id,1) as $perchild)
                  @php
                  $childnum++;
                  @endphp
                      <div><b>#{{$childnum}}</b></div>
                  <div class="user-block">
                    <span class="username">
                      <a href="#" class="{{$perchild->gender == 1 ? 'text-primary' : 'text-danger'}}">{{$perchild->name}} &nbsp;&nbsp; {{$perchild->name}}</a>
                      <p>{{$perchild->email}}</p>
                    </span>
                  </div>
                  @endforeach
                </div>
              </div><!-- /.card-body -->
            </div>
            @endif
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection