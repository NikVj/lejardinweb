@extends('layouts.app')
@section('content')

  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <div class="az-content az-content-app az-content-contacts pd-b-0">


    @if ($errors->any())
    <div class="alert alert-danger mg-b-0">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <strong>Error!</strong> 
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @elseif(session('success'))
      <div class="alert alert-success mg-b-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <strong>{{session('success')}}</strong>
      </div>
    @endif
        <div id="stafflist">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title"><b>My Users</b>
            @if(auth()->user()['is_admin'] == 1)
              <a href="{{ url('editUser') }}" class=" medium " ><i class="fa fa-user-plus"></i></a>
            @endif
            </h5>
          </div>
          <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="table table-bordered table-hover ListAll">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Join Date</th>
                                <th>Photo</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($allUsers as $user)
                            <tr>
                                <td>{{$user->firstname}}&nbsp;&nbsp;{{$user->lastname}}</td>
                                <td>{{($user->user_role == 1)?'Admin':($user->user_role == 2 ? 'staff' : 'Parent')}}</td>
                                <td>{{$user->created_at}}</td>
                                @if ($user->profile != null)
                                <td><img class="img-thumbnail w-50px" style="width:0px" src = "{{asset('images/users/'.$user->profile)}}"/></td>
                                @else
                                <td><img class="img-thumbnail w-50px" style="width:0px" src = "{{asset('images/user.png')}}"/></td>
                                @endif
                                <td>{{$user->address1}}</td>
                                <td>
                                  @if($user->status == 1)
                                  <label class="badge badge-success"> Active</label>
                                  @else
                                  <label class="badge badge-danger"> Deactivated</label>
                                  @endif
                                </td>
                                <td>{{getactionicons($user->id,'User')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
  </div>
<!-- 
  <script>
    function loadPage(viewId,pageId,url){
      $('#loadedPage').load(url+'/'+pageId);
      $('#loadedPage').show();
      $('#stafflist').hide();
    }
  </script> -->
<!-- 
  <script>
    $(function(){
      'use strict'

      new PerfectScrollbar('#azContactList', {
        suppressScrollX: true
      });

      new PerfectScrollbar('.az-contact-info-body', {
        suppressScrollX: true
      });

      $('.az-contact-item').on('click touch', function() {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');

        $('body').addClass('az-content-body-show');
      })

    });
  </script> -->

<script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
@endsection