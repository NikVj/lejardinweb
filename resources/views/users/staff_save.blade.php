@extends('layouts.app')
@section('content')
<?php
	$isEdit = (!empty($userDetails))? 1 :0;
	if($isEdit == 1){
		$allchildren = children($userDetails->user_id);
		$selfchildren = children($userDetails->user_id,1);
	}else{
		$allchildren = children();
	}
?>
<link rel="stylesheet" href="{{ asset('assets/lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="card-body" id="addstaff">
	<form action="@if(!empty($userDetails)) {{url('updateUserDetails')}} @else {{url('saveUserDetails')}} @endif" method="post" data-parsley-validate enctype='multipart/form-data' >
		{{@csrf_field()}}
		@if ($errors->any())
		   <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
        @endif

		@if(!empty($userDetails->firstname)) 
			<input type="hidden" name="id" value="{{$userDetails->id}}">
			<input type="hidden" name="user_id" value="{{$userDetails->user_id}}">
		@endif

		<div class="row py-4">
			<div class="col-md-12 dis-flex">
				<h3><span class="az-content-title">@if(!empty($userDetails)) Edit @else Add @endif User</span></h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="group">
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="firstname" class="form-control" value="@if(old('firstname')) {{old('firstname')}}@elseif(!empty($userDetails->firstname)) {{$userDetails->firstname}} @endif" required>
							<span class="highlight"></span>
							<label>First Name</label>
						</div>
						<div class="col-md-6">
							<input type="text" name="lastname" class="form-control" value="@if(old('lastname')) {{old('lastname')}}@elseif(!empty($userDetails->lastname)) {{$userDetails->lastname}} @endif" required>
							<span class="highlight"></span>
							<label>Last Name</label>
						</div>
					</div>
				</div>

				<div class="group form-group">
					<input type="email" name="email" class="form-control" value="@if(old('email')) {{old('email')}}@elseif(!empty($userDetails->email)) {{$userDetails->email}} @endif" required>
					<span class="highlight"></span>
					<label>Email</label>
				</div>

				<div class="group form-group">
					<input type="password" name="password" class="form-control" @if(empty($userDetails)) required @endif>
					<span class="highlight"></span>
					<label>Password</label>
				</div>

				<div class="group form-group">
					<input type="password" name="confirm_password" class="form-control" @if(empty($userDetails)) required @endif>
					<span class="highlight"></span>
					<label>Confirm Password</label>
				</div>

				<div class=" form-group">
					<label class="label-radio">Role</label>
				</div>
				<input type="hidden" name="role" value="{{ $isEdit == 1 ? ($userDetails->user_role ) :''}}" >
				<div class="">
				    <div class="custom-control custom-radio pmd-radio custom-control-inline">
							<input type="radio" id="role3" name="user_role" value= "3" class="custom-control-input" {{ $isEdit == 1 ? 'disabled '.($userDetails->user_role == 3 ? 'checked':'') :''}} >
							<label class="custom-control-label pmd-radio-ripple-effect" for="role3">Parent</label>
				    </div>
				    <div class="custom-control custom-radio pmd-radio custom-control-inline">
							<input type="radio" id="role2" name="user_role" value= "2" class="custom-control-input" {{ $isEdit == 1 ? 'disabled '.($userDetails->user_role == 2 ? 'checked':'') :''}} >
							<label class="custom-control-label pmd-radio-ripple-effect" for="role2">Staff</label>
				    </div>
				    <div class="custom-control custom-radio pmd-radio custom-control-inline">
							<input type="radio" id="role1" name="user_role" value= "1" class="custom-control-input"  {{ $isEdit == 1 ? 'disabled '.($userDetails->user_role == 1 ? 'checked':'') : 'checked' }} >
							<label class="custom-control-label pmd-radio-ripple-effect" for="role1">Admin</label>
				    </div>
				</div>
				<div class="form-group py-4">
					
					<a href="javascript:;" onclick="$('#userProfileInput').trigger('click');">
		                <img src="{{ (($isEdit == 1 && $userDetails->profile != null ) ? (asset('images/users/'.$userDetails->profile) ) : asset('images/user.png') )}}" id="uploaded_image" class="img-circle" style="width:140px">
		            </a>
					<input type="file" name="profile" id="userProfileInput" class="d-none" accept="image/*">
				</div>
			</div>

			<div class="col-md-6">
				<div class="group form-group">
					<input type="text" name="phone" class="form-control phoneMask" value="@if(old('phone')) {{old('phone')}}@elseif(!empty($userDetails->phone)) {{$userDetails->phone}} @endif" required>
					<span class="highlight"></span>
					<label>Work Contact</label>
				</div>

				<div class="group form-group">
					<input type="text" name="phone_home" class="form-control phoneMask" value="@if(old('phone_home')) {{old('phone_home')}}@elseif(!empty($userDetails->phone_home)) {{$userDetails->phone_home}} @endif" required>
					<span class="highlight"></span>
					<label>Personal Contact</label>
				</div>

				<div class="group form-group">
					<label class="label-textarea">Current Address</label>
					<textarea name="address1" rows="3" class="form-control" placeholder="Type Here" required>@if(old('address1')) {{old('address1')}}@elseif(!empty($userDetails->address1)) {{$userDetails->address1}} @endif</textarea>
				</div>

				<div class="group form-group">
					<label class="label-textarea">Permanent Address</label>
					<textarea name="address2" rows="3" class="form-control" placeholder="Type Here" required>@if(old('address2')) {{old('address2')}}@elseif(!empty($userDetails->address2)) {{$userDetails->address2}} @endif</textarea>
				</div>
				<div class=" py-4">
					<div id="childinfo" class="card {{$isEdit ==1  && $userDetails->user_role == 3 && count($selfchildren) >=0 ? '' : 'd-none'}}">
						<div class="card-body childaddbox ">
							@if($isEdit ==1  && $userDetails->user_role == 3 && count($selfchildren) >0)
							@php
							$ind = 1;
							@endphp
							@foreach($selfchildren as $perselfchild)

								<div class="childinfobox" data-id="{{$ind}}">
									<div class="group form-group"><b class="text-danger">
	    								Child #<span class="child_reference">{{$ind}}</span></b>
									</div>
										<input name="child[{{$ind}}][id]" class="form-control childid" type="hidden" value="{{$perselfchild->id}}">
									<div class="group form-group">
										<input name="child[{{$ind}}][firstname]" class="form-control" value="{{$perselfchild->firstname}}">
										<label class="label-textarea label">first name</label>
									</div>
									<div class="group form-group">
										<input name="child[{{$ind}}][lastname]" class="form-control" value="{{$perselfchild->lastname}}">
										<label class="label-textarea label">last name</label>
									</div>
									<div class="group form-group">
										<input name="child[{{$ind}}][email]" type="email" class="form-control"  value="{{$perselfchild->email}}">
										<label class="label-textarea label">Email</label>
									</div>
									<div class=" form-group">
										<label class="label-radio">Gender</label>
									</div>
									<div class="">
									    <div class="custom-control custom-radio pmd-radio custom-control-inline">
												<input type="radio" id="gender1_{{$ind}}" name="child[{{$ind}}][gender]" value= "1" class="custom-control-input"  {{$perselfchild->gender == 1 ? "checked" :''}}>
												<label class="custom-control-label pmd-radio-ripple-effect" for="gender1_{{$ind}}">Male</label>
									    </div>
									    <div class="custom-control custom-radio pmd-radio custom-control-inline">
												<input type="radio" id="gender2_{{$ind}}" name="child[{{$ind}}][gender]" value= "2" class="custom-control-input" {{$perselfchild->gender == 2 ? "checked" :''}}>
												<label class="custom-control-label pmd-radio-ripple-effect" for="gender2_{{$ind}}">Female</label>
									    </div>
									    <div class="custom-control custom-radio pmd-radio custom-control-inline">
												<input type="radio" id="gender3_{{$ind}}" name="child[{{$ind}}][gender]" value= "3" class="custom-control-input" {{$perselfchild->gender == 3 ? "checked" :''}}>
												<label class="custom-control-label pmd-radio-ripple-effect" for="gender3_{{$ind}}">Other</label>
									    </div>
									</div>
								</div>
								@php
								$ind++;
								@endphp
							@endforeach
							@else
							<div class="childinfobox" data-id="1">
								<div class="group form-group"><b class="text-danger">
    								Child #<span class="child_reference">1</span></b>
								</div>
								<input name="child[1][id]" class="form-control childid" type="hidden" value="0">
								<div class="group form-group">
									<input name="child[1][firstname]" class="form-control" >
									<label class="label-textarea label">First Name</label>
								</div>
								<div class="group form-group">
									<input name="child[1][lastname]" class="form-control" >
									<label class="label-textarea label">Last Name</label>
								</div>
								<div class="group form-group">
									<input name="child[1][email]" type="email" class="form-control" >
									<label class="label-textarea label">Email</label>
								</div>
								<div class=" form-group">
									<label class="label-radio">Gender</label>
								</div>
								<div class="">
								    <div class="custom-control custom-radio pmd-radio custom-control-inline">
											<input type="radio" id="gender1_1" name="child[1][gender]" value= "1" class="custom-control-input" checked>
											<label class="custom-control-label pmd-radio-ripple-effect" for="gender1_1">Male</label>
								    </div>
								    <div class="custom-control custom-radio pmd-radio custom-control-inline">
											<input type="radio" id="gender2_1" name="child[1][gender]" value= "2" class="custom-control-input">
											<label class="custom-control-label pmd-radio-ripple-effect" for="gender2_1">Female</label>
								    </div>
								    <div class="custom-control custom-radio pmd-radio custom-control-inline">
											<input type="radio" id="gender3_1" name="child[1][gender]" value= "3" class="custom-control-input">
											<label class="custom-control-label pmd-radio-ripple-effect" for="gender3_1">Other</label>
								    </div>
								</div>
							</div>
							@endif
						</div>
							<a href="Javascript:;" class="btn btn-large btn-info btn-block p-2 addmorechild"><i class="fa fa-plus" ></i></a>
					</div>
					<div id="children_list" class=" {{ ($isEdit ==1 && $userDetails->user_role == 2) ? '' : 'd-none'}}">
						<div class="row">
							<div class="col-md-6">
								<select name="staffchild[]" class="form-control select2 select2child" data-placeholder="Choose Child" data-parsley-class-handler="#slWrapper" multiple="multiple" >
									<option label="Choose Child"></option>
									@foreach($allchildren as $child)
										<option value="{{$child->id}}" {{isset($child->child_status) ? ($child->child_status == 1 ? 'selected' :'') : ''}}>{{$child->firstname.' '.$child->lastname}}</option>
									@endforeach
								</select>
							</div>
							<div id="s2ErrorContainer"></div>
							<div class="col-md-6">
								<div id="slWrapper" class="parsley-select mg-l-10">
									<select name="designation" class="form-control select2 select2designation" data-placeholder="Choose Designation" data-parsley-class-handler="#slWrapper">
										<option label="Choose Designation"></option>
										@foreach(userDesignations() as $userDesignation)
											@if (!empty($userDetails->designation) && $userDesignation['id'] == $userDetails['designation'])
												<option value="{{$userDesignation['id']}}" selected>{{$userDesignation['designation_name']}}</option>
											@else
												<option value="{{$userDesignation['id']}}">{{$userDesignation['designation_name']}}</option>
											@endif
										@endforeach
									</select>
									<div id="slErrorContainer"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>

<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/lib/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

<script>
	$(function(){
		'use strict'
		$(document).ready(function(){
			$('.select2designation').select2({
				placeholder: 'Choose Designation'
			});
			$('.select2child').select2({
				placeholder: 'Choose Child'
			});

			$('.select2-no-search').select2({
				minimumResultsForSearch: Infinity,
				placeholder: 'Choose Designation'
			});
			$('.select2child .select2-no-search').select2({
				minimumResultsForSearch: Infinity,
				placeholder: 'Choose Child'
			});
		});
		$('#selectForm').parsley();
		$('#selectForm2').parsley();

		$('.phoneMask').mask('(999) 999-9999');
	});
	$('input[type=radio][name=user_role]').on('change', function() {
		var isEdit = '{{$isEdit}}';
		if(isEdit == 1){
			// alert('You are not allowed to change user role!!');
			return false;
		}
		else{
  			$('#children_list,#childinfo').addClass('d-none');
		  	var selected_role = $(this).val();
		  	if(selected_role == 2){
		  		$('#children_list').removeClass('d-none');
		  	}else if(selected_role == 3){
		  		$('#childinfo').removeClass('d-none');
		  	}
		}
	});
	$('.addmorechild').click(function(){
		var referenceIdprev=$('.childinfobox:last').data('id');
		var referenceId=referenceIdprev+1;

		var clonehtml	 = $('.childinfobox:last').clone();
		
		(clonehtml).attr('data-id',referenceId);
		(clonehtml).find('span').text(referenceId);
		(clonehtml).find('.childid').val(0);
		(clonehtml).find(`input[name="child[`+referenceIdprev+`][id]"]`).attr('name',"child["+referenceId+"][id]");
		(clonehtml).find(`input[name="child[`+referenceIdprev+`][firstname]"]`).attr('name',"child["+referenceId+"][firstname]");
		(clonehtml).find(`input[name="child[`+referenceIdprev+`][lastname]"]`).attr('name',"child["+referenceId+"][lastname]");
		(clonehtml).find(`input[name="child[`+referenceIdprev+`][email]"]`).attr('name',"child["+referenceId+"][email]");
		(clonehtml).find(`input[name="child[`+referenceIdprev+`][gender]"]`).attr('name',"child["+referenceId+"][gender]");
		(clonehtml).find('.custom-control-inline').find('input').each(function(index,item){
			$(item).attr('id','gender'+($(item).attr('value'))+'_'+referenceId);
			$(item).parent().find('label').attr('for','gender'+($(item).attr('value'))+'_'+referenceId);
		});
		clonehtml.appendTo('.childaddbox');
		$(`input[name="child[`+referenceId+`][firstname]"]`).val('');
		$(`input[name="child[`+referenceId+`][lastname]"]`).val('');
		$(`input[name="child[`+referenceId+`][email]"]`).val('');
		// $(`input[name="child[`+referenceId+`][gender]"]`).val(1);
		$('#gender1_'+referenceId).attr('checked','checked');
	});

	$(document).ready(function(){
        $(document).on('change', '[name="profile"]', function (e) { 
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
            var _this = $(this);
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                toastr.error( "Cover image has an invalid extension. Valid extension(s) : " + fileExtension.join(', '));
                _this.val('');
                return;
            } else if (this.files[0].size > (5 * 1024 * 1024)) {
                toastr.error( "Max 5MB of cover file size allowed");
                _this.val('');
                return;
            } else {
                var reader = new FileReader();
                var baseString;

                reader.onloadend = function () {
                    baseString = reader.result;
                    $('#uploaded_image').attr('src', baseString);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
	});
</script>
@endsection