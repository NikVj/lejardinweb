<link href="{{ asset('assets/lib/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/az_calendar.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('assets/lib/amazeui-datetimepicker/css/amazeui.datetimepicker.css') }}" rel="stylesheet"> -->

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<div class="az-content az-content-calendar" id="myCalendar">
  @if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
  @endif

  @if ($errors->any())
  <div class="alert alert-danger mb-0">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>Error!</strong> 
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <div class="az-content-left az-content-left-calendar pt-2">

          <div id="dateToday" class="az-content-label az-content-label-sm tx-medium lh-1 mg-b-10"></div>
          <h2 class="az-content-title mg-b-25 tx-24">My Calendar</h2>

          <div class="fc-datepicker az-datepicker mg-b-30"></div>

          <label class="az-content-label tx-13 tx-bold mg-b-10">Event List</label>
          <nav class="nav az-nav-column az-nav-calendar-event card">
              <a href="" class="d-flex text-info nav-link @if($calEventsData == '{}'){{'disabled text-secondary'}}@endif">
                <i class="icon ion-ios-calendar "></i>
                <div style="line-height: 30px;">Calendar Events</div></a>
              <a href="" class="d-flex text-danger nav-link @if($appointmentsEventsData == '{}'){{'disabled text-secondary'}}@endif">
                <i class="icon ion-ios-calendar tx-success"></i>
                <div style="line-height: 30px;">Appointment Events</div></a>
          </nav>
        </div>
      </div>
      <div class="col-md-9">
        <div class="az-content-body az-content-body-calendar">

          <div id="calendar" class="az-calendar"></div>
        </div><!-- az-content-body -->
      </div>
    </div>
  </div><!-- container -->
</div><!-- az-content -->

<div class="modal az-modal-calendar-schedule" id="modalSetSchedule" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title">Create New Event</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div><!-- modal-header -->
      <div class="modal-body">
        <a href="JavaScript:;" class="btn btn-outline-secondary mb-3 openAttendanceModel" onClick="$('#modalMarkAttendance').modal('show');$('#modalSetSchedule').modal('hide')">
          Mark Attendance
        </a>
        <form method="post" action="addCalDetails" id="addCalDetails">
          {{@csrf_field()}}

          <div class="form-group">
            <input type="text" name="title" class="form-control" placeholder="Add title" required>
          </div><!-- form-group -->
          <div class="form-group d-flex align-items-center">
              <div class="custom-control custom-radio pmd-radio custom-control-inline">
                <input type="radio" id="etype1" name="etype" value="1" class="custom-control-input" checked >
                <label class="custom-control-label pmd-radio-ripple-effect" for="etype1">Event</label>
              </div>
              <div class="custom-control custom-radio pmd-radio custom-control-inline">
                <input type="radio" id="etype2" name="etype" value="2" class="custom-control-input"  >
                <label class="custom-control-label pmd-radio-ripple-effect" for="etype2">Appointment</label>
              </div>
          </div><!-- form-group -->
          <div class="ename row form-group">
            <div class="col-md-6">
              <div id="slWrapper" class="parsley-select mg-l-10">
                <select name="event_name" class="form-control eventDD" required>
                  <option class="eventdropdown" label="Choose Event"></option>
                  @foreach(getalleventnames('0') as $event)
                      <option class="eventdropdown" value="{{$event->id}}" >{{$event->name}}</option>
                  @endforeach
                </select>
                <select name="appointment_name" class="form-control appointmentDD"  style="display: none">
                  <option class="appointmentdropdown" label="Choose Appointment"></option>
                  @foreach(getalleventnames('1') as $appointment)
                      <option class="appointmentdropdown" value="{{$appointment->id}}" >{{$appointment->name}}</option>
                  @endforeach
                </select>
                <!-- <div id="slErrorContainer"></div> -->
              </div>
            </div>
          </div>

          <div class="form-group row my-30">
            <div class="col-md-6">
              <div id="children_list" >
                  <select name="parent[]" class="form-control select2 select2parent" data-placeholder="Choose Parent" data-parsley-class-handler="#slWrapper" multiple="multiple" >
                    <option label="Choose Parent"></option>
                    @foreach($allparents as $parent)
                      <option value="{{$parent->id}}" >{{$parent->firstname.' '.$parent->lastname}}</option>
                    @endforeach
                  </select>
                </div>
            </div>

            <div class="col-md-6">
              <div id="children_list" >
                <select name="staff[]" class="form-control select2 select2staff" data-placeholder="Choose Staff" data-parsley-class-handler="#slWrapper" multiple="multiple" >
                  <option label="Choose Staff"></option>
                  @foreach($allstaff as $staff)
                    <option value="{{$staff->id}}">{{$staff->firstname.' '.$staff->lastname}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row d-none" id="selectBoxerror">Please select at least 1 staff or parent</div>
          </div>

          <div class="form-group mg-t-30">
            <label class="tx-13 mg-b-5 tx-gray-600">Start Event</label>
            <div class="row row-xs">
              <div class="col-12">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                  </div>
                  <input type="text" name="start_date" id="azEventStartDateOLD" value="{{date('Y-m-d')}} 12:00" class="form-control dtimepicker">
                </div>
              </div><!-- col-7 -->

            </div><!-- row -->
          </div><!-- form-group -->
          <div class="form-group">
            <label class="tx-13 mg-b-5 tx-gray-600">End Event</label>
            <div class="row row-xs">
              <div class="col-12">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                  </div>
                  <input type="text" name="end_date" id="azEventEndDateOLD" value="{{date('Y-m-d')}} 12:00" class="form-control dtimepicker">
                </div>
              </div>
            </div><!-- row -->
          </div><!-- form-group -->
          <div class="form-group">
            <textarea class="form-control" name="description" rows="2" placeholder="Write some description (optional)"></textarea>
          </div><!-- form-group -->

          <div class="d-flex mg-t-15 mg-lg-t-30">
            <button type="submit" class="btn btn-primary px-25 mr-5">Save</button>
            <a href="" class="btn btn-default" data-dismiss="modal">Discard</a>
          </div>
        </form>
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->
<div class="modal az-modal-calendar-schedule" id="modalMarkAttendance" role="dialog" aria-hidden="true">
  <input type="hidden" id="clickedDate" name="">
  <input type="hidden" id="clickedDay" name="">
  <input type="hidden" id="clickedMonth" name="">
  <input type="hidden" id="clickedYear" name="">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title">Mark Attendance</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div><!-- modal-header -->
      <div class="modal-body">
        <div class="container" id="addattendance">
          <form id="addAttendanceForm" method="post" data-parsley-validate enctype='multipart/form-data' >
            <div class="row attendanceView">
              {{@csrf_field()}}
            </div>
          </form>
        </div>
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->

<div class="modal az-modal-calendar-event" id="modalCalendarEvent" data-id="" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="event-title"></h6>
        <nav class="nav nav-modal-event">
          <!-- <a href="#" class="nav-link"><i class="fa fa-external-link"></i></a> -->
          @if(auth()->user()['is_admin'] == 1)<a href="javascript:;" class="nav-link" id="dltCalBtn"><i class="fa fa-trash"></i></a>@endif
          <a href="#" class="nav-link" data-dismiss="modal"><i class="fa fa-times"></i></a>
        </nav>
      </div><!-- modal-header -->
      <div class="modal-body">
        <div class="row row-sm">
          <div class="col-sm-6">
            <label class="tx-13 tx-gray-600 mg-b-2">Start Date</label>
            <p class="event-start-date"></p>
          </div>
          <div class="col-sm-6">
            <label class="tx-13 mg-b-2">End Date</label>
            <p class="event-end-date"></p>
          </div><!-- col-6 -->
        </div><!-- row -->

        <div class="row row-sm">
          <div class="col-sm-6">
            <label class="tx-13 tx-gray-600 mg-b-2">Staff Members</label>
            <p class="staff-members">
              No members assigned
            </p>
          </div>
          <div class="col-sm-6">
            <label class="tx-13 mg-b-2">Parents</label>
            <p class="parents-members">
              No Members assigned
            </p>
          </div><!-- col-6 -->
        </div><!-- row -->

        <label class="tx-13 tx-gray-600 mg-b-2">Description</label>
        <p class="event-desc tx-gray-900 mg-b-30"></p>

        <a href="" class="btn btn-secondary wd-80" data-dismiss="modal">Close</a>
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->
<script src="{{ asset('assets/lib/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<!-- <script src="{{ asset('assets/lib/fullcalendar/fullcalendar.min.js') }}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js" integrity="sha512-o0rWIsZigOfRAgBxl4puyd0t6YKzeAw9em/29Ag7lhCQfaaua/mDwnpE2PVzwqJ08N7/wqrgdjc2E0mwdSY2Tg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('assets/js/app-calendar.js') }}"></script>
<script src="{{ asset('assets/lib/amazeui-datetimepicker/js/amazeui.datetimepicker.min.js') }}"></script>

<script>
    $(document).ready(function(){
      $('.select2parent').select2({
        placeholder: 'Choose Parent'
      });

      $('.select2parent .select2-no-search').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'Choose Parent'
      });
      $('.select2staff').select2({
        placeholder: 'Choose Staff'
      });

      $('.select2staff .select2-no-search').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'Choose Staff'
      });
      $('.select2child').select2({
        placeholder: 'Choose Child'
      });

      $('.select2child .select2-no-search').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'Choose Child'
      });
    });
  $(document).ready(function(){
      $('#etype1').attr('checked', 'checked');
      $('input[type=radio][name=etype]').on('change', function() {
          var selected_type = $(this).val();
          if(selected_type == '1'){
            // $('.eventdropdown').removeClass('d-none');
            // $('.eventdropdown').removeClass('d-none');
            $('.eventDD').show().prop('required',true);
            $('.appointmentDD').hide().prop('required',false);
          }else {
            // $('.appointmentdropdown').removeClass('d-none');
            $('.eventDD').hide().prop('required',false);
            $('.appointmentDD').show().prop('required',true);
          }

    });

  })
  $(function(){
    'use strict'

    $('.select2-modal').select2({
      minimumResultsForSearch: Infinity,
      dropdownCssClass: 'az-select2-dropdown-modal',
    });

    $('#dateToday').text(moment().format('ddd, MMMM DD YYYY'));

    // AmazeUI Datetimepicker
    $('.dtimepicker').datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      autoclose: true
    });

  });
</script>

<script type="text/javascript">
  'use strict'

  var cEvents     = <?= $calEventsData ?>;
  var apmntEvents  = <?= $appointmentsEventsData ?>;

  var curYear  = moment().format('YYYY');
  var curMonth = moment().format('MM');

  var azCalendarEvents = {
    id: 1,
    backgroundColor: '#3fcabd',
    borderColor: '#638478',
    events: cEvents
  };

  var azAppointmentEvents = {
    id: 2,
    backgroundColor: '#b91212',
    borderColor: '#6b3e3a',
    events: apmntEvents
  };

</script>

<script type="text/javascript">
  $('#dltCalBtn').click(function(){
    var ID = $('#modalCalendarEvent').attr('data-id');

    $.ajax({
      url: "{{'deleteCalendar'}}" +'/'+ ID,
      success: function (data){
        if (data.class_name == 'alert-success') {
          $('[data-act=removed]').slideToggle();
        }
        $('.alert').hide(200);
        var html='<div class="alert '+ data.class_name +' mg-b-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+ data.message +'</strong></div>';
        $( ".az-content-calendar" ).prepend($(html));
        $('#modalCalendarEvent').modal('toggle');
      }
    });
  });
  $('.openAttendanceModel').click(function(){

        $.ajax({
               type: "GET",
               url: 'editAttendance?date='+$('#clickedDate').val(),
               success: function(data)
               {
                $('#modalMarkAttendance').find('.attendanceView').html(data) ;                  
               }
             });

  })
  $( "#addCalDetails" ).submit(function( event ) {
    $('#selectBoxerror').addClass('d-none');
    var staffSelected= $('.select2staff').val();
    var parentSelected= $('.select2parent').val();
    console.log("staffSelected.length",staffSelected.length);
    console.log("parentSelected.length",parentSelected.length);
    if( staffSelected.length == 0 && parentSelected.length == 0 ){
      $('#selectBoxerror').removeClass('d-none');
      event.preventDefault();
    }
});
</script>