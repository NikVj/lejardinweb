
<?php
	// pr(userProfile());
?>

  <!-- START HEADER -->
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('home')}}" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block" id="main-header-search">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li> -->

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge notification_count d-none " > </span>
        </a>
        <div id="notifications_chart" class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><span class="notification_count"></span> Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span> -->
            No New Notifications
          </a>
         <!--  <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a> -->
          <div class="dropdown-divider"></div>
          <a href="notifications" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <script>
     $( document ).ready(function() {
        $.ajax({
           type:'GET',
           url:'notifications',
           data:'_token = <?php echo csrf_token() ?>',
           success:function(data) {
            var result = JSON.parse(data);
            var notifications = result.data;
            if(result.count != 0){
              $(".notification_count").html(result.count);
              $(".notification_count").removeClass('d-none');
            }else
              $(".notification_count").addClass('d-none');
              var notification_html = '<span class="dropdown-item dropdown-header"><span class="notification_count"></span> Notifications</span>';
              $.each( notifications, function( key, value ) {
                notification_html+= '<div class="dropdown-divider"></div><a href="#" class="dropdown-item"> '+value.content+' <span class="float-right text-muted text-sm"></span></a>';
              });
              notification_html+= '<div class="dropdown-divider"></div><a href="notifications" class="dropdown-item dropdown-footer">See All Notifications</a>';
              $('#notifications_chart').html(notification_html);

           }
        });
     });
  </script>
  <!-- END HEADER -->