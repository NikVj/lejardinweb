
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar @if(auth()->user()['is_admin'] == 1) sidebar-dark-primary @else sidebar-navy-warning @endif  elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('home')}}" class="brand-link">
      <img src="{{asset('images/logo.jpg')}}" alt="Le Jardin Logo" class="brand-image img-circle elevation-3" style="opacity: 1">
      <span class="brand-text font-weight-light">Le Jardin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{userProfile()}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <div class="dropdown">
            <a href="#" class="d-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{userData()->firstname}}&nbsp;&nbsp;{{userData()->lastname}}</a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="background: #e3e6ce">
              <a class="dropdown-item" href="{{url('viewUser/'.auth()->user()['id'])}}">Profile</a>
              <!-- <a class="dropdown-item" href="JavaScript::Void();">Settings</a> -->
                  <form method="POST" action="{{url('logout')}}">
                    @csrf           
                      <a class="dropdown-item" href="#" onclick="event.preventDefault();this.closest('form').submit();">Logout</a>
                  </form>
            </div>
          </div>
        </div>
      </div>

      <!-- SidebarSearch Form -->
     <!--  <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{url('home')}}" class="nav-link {{ (request()->is('home')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('messaging')}}" class="nav-link {{ (request()->is('messaging')) ? 'active' : '' }}">
              <i class="nav-icon far fa-comments"></i>
              <p>
                Chat
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="{{url('app/mail')}}" class="nav-link {{ (request()->is('app/mail')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Mailbox
                <span class="right badge badge-danger">4 New</span>
              </p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="{{url('calendar')}}" class="nav-link {{ (request()->is('calendar')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar "></i>
              <p>
                Calendar
              </p>
            </a>
          </li>
          @if(auth()->user()['is_admin'])
          <li class="nav-item">
            <a href="{{url('users')}}" class="nav-link {{ (request()->is('users')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{url('locations')}}" class="nav-link {{ (request()->is('locations')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-map"></i>
              <p>
                Locations
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('feeds')}}" class="nav-link {{ (request()->is('feeds')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-rss"></i>
              <p>
                Feed
              </p>
            </a>
          </li>
          @if ((auth()->user()['user_role'] == 1 || auth()->user()['user_role'] == 2)) 
          <li class="nav-item">
            <a href="{{url('attendance')}}" class="nav-link {{ (request()->is('attendance')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-th-list"></i>
              <p>
                Attendance
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{url('newsletters')}}" class="nav-link {{ (request()->is('newsletters')) ? 'active' : '' }}">
              <i class="nav-icon fa fa-newspaper-o"></i>
              <p>
                Newsletter
              </p>
            </a>
          </li>
          <li class="nav-item  {{ ((request()->is('disability')) || (request()->is('education')) || (request()->is('health-nutrition')) || (request()->is('family-services')) || (request()->is('mental-health')) || (request()->is('internal-report')) ) ? 'menu-open' : '' }}">
            <a href="javascript:;" class="nav-link {{ ((request()->is('disability')) || (request()->is('education')) || (request()->is('health-nutrition')) || (request()->is('family-services')) || (request()->is('mental-health')) || (request()->is('internal-report')) ) ? 'active' : '' }}">
              <i class="nav-icon fas fa-search"></i>
              <p>
                Components
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('disability')}}" class="nav-link {{ (request()->is('disability')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Disability</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('education')}}" class="nav-link {{ (request()->is('education')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Education</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('health-nutrition')}}" class="nav-link {{ (request()->is('health-nutrition')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Health/Nutrition</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('family-services')}}" class="nav-link {{ (request()->is('family-services')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Family Services</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('mental-health')}}" class="nav-link {{ (request()->is('mental-health')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Mental Health</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('internal-report')}}" class="nav-link {{ (request()->is('internal-report')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Internal Report</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- <li class="nav-item {{ (request()->is('component resources')) ? 'active' : '' }}">
            <a href="{{url('component-resources')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Componenets Resources
              </p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <style type="text/css">
    .user-panel, .user-panel .info {
        overflow: unset;
    }
  </style>