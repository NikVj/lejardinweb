@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="{{ asset('assets/lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="az-content az-content-app az-content-contacts pd-b-0">

@if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
@endif

@if ($errors->any())
<div class="alert alert-danger mb-0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Error!</strong> 
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

</div>
<div class="az-content-body az-content-body-contacts">
<!-- <form method="POST" action={{route('resources.save')}} data-target= "{{route('component.view',['type' => $component->name])}}" id="resource_form"> -->
<form action="{{route('education.documents.save')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
	  	{{@csrf_field()}}

 
		<input type="hidden" name="document_type_id" value="{{$document_id}}">
		@if(!empty($resource)) 
			<input type="hidden" name="id" value="{{$resource->id}}">
		@endif
		@if(!empty($component)) 
			<input type="hidden" name="type" value="{{$component->code}}">
		@endif
        
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Topic</label>
					<input type="text" name="topic" class="form-control" value="@if(!empty($resource->topic)) {{$resource->topic}} @endif" required>
					
				</div>
				<!-- <div class="form-group">
                        <label>Date</label>
                        <input type="date" class="form-control datePicker" name="date" value="@if(!empty($resource->date)) {{ date('Y-m-d', strtotime($resource->date)) }} @endif">
                </div> -->

				<div class="form-group" >
        		 <label>Choose From files </label>
					<input type="file" name="document" class="form-control" > 
					@if(!empty($resource->file))
					<div id="uploaded">
					<p> Uploaded File : 
					<a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
					</p>
					<input type="hidden" name="uploaded_file" value ="{{$resource->file}}" >
					</div>
					@endif
				</div>
				</div>
			</div>

		<button type="submit" id="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>

<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/lib/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

<script>
$(document).ready(function() {
	$('.datePicker').datepicker({
		format: 'dd-mm-yyyy',
		orientation: "bottom",
		 todayHighlight: true,
		 autoclose: true
	});
});	



</script>
@endsection