@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="{{ asset('assets/lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="az-content az-content-app az-content-contacts pd-b-0">

@if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
@endif

@if ($errors->any())
<div class="alert alert-danger mb-0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Error!</strong> 
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

</div>
<div class="az-content-body az-content-body-contacts">

<form action="{{route('education.documents.save')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
		{{@csrf_field()}}

		@if(!empty($resource)) 
			<input type="hidden" name="id" value="{{$resource->id}}">
		@endif
		@if(!empty($component)) 
			<input type="hidden" name="type" value="{{$component->code}}">
		@endif
		@if(!empty($documentType))
			 <input type="hidden" name="document_id" value="{{$documentType->id}}">
			 <input type="hidden" name="document_type" value="{{$documentType->type}}">
		@endif
		
        <div class="row">
        <span class="az-content-title">@if(!empty($resource)) Edit @else Add @endif</span>
		</div>
        <br>
        <div class="row">
			<div class="col-md-6">
                <div class="form-group">
				<label>Component type</label>
					<input type="text" name="component_type" id="component_type" class="form-control" value="@if(!empty($component)) {{$component->designation_name}} @endif" readonly>
				</div>


				</div>
			</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Topic</label>
					<input type="text" name="topic" class="form-control" value="@if(!empty($resource->topic)) {{$resource->topic}} @endif" required>
					
				</div>
				<!-- <div class="form-group">
                        <label>Date</label>
                        <input type="date" class="form-control" name="date">
                </div> -->

				<div class="form-group">
					<label>Additional text</label>
					<input type="text" name="additional_info" class="form-control" value="@if(!empty($resource->additional_info)) {{$resource->additional_info}} @endif">
					
				</div>
			

				<div class="form-group">
                                <label>Select a Resource Type</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="resource_type" value="link" {{!empty($resource->link) ? 'checked' : ''}} checked > Link
                                    </label>
									&nbsp;
                                    <label class="radio-inline">
                                        <input type="radio" name="resource_type" value="document" {{!empty($resource->file) ? 'checked' : ''}}> Upload File
                                    </label>
                            	</div>
                </div>
				<div id="link" style="display: none;">
				<div class="group form-group">
					<input type="text" name="link" class="form-control" value="@if(!empty($resource->link)) {{$resource->link}} @endif">
					<span class="highlight"></span>
					<label>Link</label>
				</div>
				</div>
				<div id="document" style="display: none;">
				<div class="group form-group" >
					<input type="file" name="document" class="form-control" >
					<span class="highlight"></span>
					<label>Choose From files </label> 
					@if(!empty($resource->file))
					<div id="uploaded">
					<p> Uploaded File : 
					<a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
					<!-- <button type="button" id="remove" class="btn-xs btn-danger pd-x-10">Remove</button> -->
					</p>
					<input type="hidden" name="uploaded_file" value ="{{$resource->file}}" >
					</div>
					@endif
				</div>
				</div>
			</div>
			</div>
		</div>
		<button type="submit" id="submit" class="btn btn-dark pd-x-20">Submit</button>
	</form>
</div>

<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/lib/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

<script>
// $(document).ready(function(){
//    var fileName = "";
//    var fileExtension = fileName.split('.').pop(); //"pdf"
//    if(fileExtension == 'pdf')
//    {

//    }
//   });

$(document).ready(function() {
    $("input[name$='resource_type']").click(function() {
        var type = $(this).val();
		if(type == 'link'){
		$('#link').show();
		$('#document').hide();
		}

		if(type == 'document')
		{
		$('#link').hide();
		$('#document').show();

		}
		
    });

	var type = $('input[name="resource_type"]:checked').val();
	if(type == 'link'){
		$('#link').show();
		$('#document').hide();
		}

		if(type == 'document')
		{
		$('#link').hide();
		$('#document').show();

		}
});

// $('#remove').on('click' , function(){

// 	$('#file').val('');
// 	$('#uploaded').hide();
// })
// $(document).ready(function(){
// 	$('#submit').on('click', function() {
//     var form = $('#resource_form').closest('form');
//     var method = form.data('method');
//     var url = form.data('action');
//     console.log(url);
//     var target = form.data('target');
//     var data = form.serialize();
//     console.log(data);
//     var type = $('#component_type').val();
//     var buttonText = $(this).text();
//     $(this).attr('disabled', true).html('<i class="fa fa-spinner fa-spin"></i>');
//     $.ajax({
//         url: url,
//         type: method,
//         data: {
//                 'id': $('#bbq_city').val(),
//                 '_token': $('input[name=_token]').val()
//             },
//         success: function(data) {
//             location.href = target;
//         },
//         error: function(data) {
//             showError(data,form);
//             $('#submit').attr('disabled', false).html(buttonText);
//         }
//     });
// });
// });
</script>
@endsection