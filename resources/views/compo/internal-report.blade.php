@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">
<style>
ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: -7px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 20px 0;
    padding-left: 20px;
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: -15px;
    width: 20px;
    height: 20px;
    z-index: 400;
}
</style>

<div class="container">
  <h2>Internal Report</h2>
  <div class="az-content az-content-app az-content-contacts pd-b-0">

@if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
@endif

@if ($errors->any())
<div class="alert alert-danger mb-0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Error!</strong> 
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

@if($current_time_zone=Session::get('current_time_zone'))@endif
<input type="hidden" id="hd_current_time_zone" value="{{$current_time_zone}}">
  <ul class="nav nav-pills my-2">
    <li class="nav-item">
      <a class="nav-link active" href="#menu1" data-toggle="pill">Parent/Staff Activity Log</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu2" data-toggle="pill">Reset Password</a>
    </li>
  </ul>
  <div class="tab-content">
    <div id="menu1" class="tab-pane active">
      <div class="container mt-5 mb-5">
       <div class="row mb-2" >
            <!-- <div class="col-sm-12 col-md-8">
                <div id="DataTables_Table_4_filter" class="dataTables_filter">
                    <form  action="{{route('component.view')}}/internal-report?limit={{$paginate}}" method="GET">
                    <label class="col-md-6">Search Keyword :<input class="form-control" @if( isset($_GET['search_keyword']) )  value="{!! $_GET['search_keyword'] !!}" @endif name="search_keyword"  placeholder="Type Name, Booking ID" aria-controls="DataTables_Table_4" type="search">
                    </label>
                    <label>
                        <button class="btn btn-primary">Filter</button>
                         &nbsp; &nbsp;
                        <a href="{{route('component.view')}}/internal-report" class="btn btn-default">Reset</a>
                    </label> 
                </form>
                </div>
            </div> -->  

            <div class="col-sm-12 col-md-4">
                    <div class="float-left">
                        <label>Show <select onchange="location = '{!! route('component.view') !!}/internal-report'+'?limit='+this.options[this.selectedIndex].value;" 
                            name="limit"  class="form-control form-control-sm">
                            <option value="10" @if($paginate == 10) selected  @endif>10</option>
                            <option value="25" @if( $paginate == 25  ) selected  @endif>25</option>
                            <option value="50" @if( $paginate ==  50  ) selected  @endif >50</option>
                            <option value="100" @if( $paginate ==  100  ) selected  @endif >100</option>
                        </select> </label>
                    </div>
            </div>
        </div>
      <div class="row">
        <div class="col-md-10">
          <h4>Activity Logs</h4>
          <ul class="timeline">
          @foreach($logs as $key => $log)
            <li>
              <a href="#">{{$log->user->name}}</a>
              <a href="JavaScript:;" class="float-right">{{get_local_time($log->timestamp)}} </a>
              <p>{{$log->description}}</p>
            </li>
          @endforeach
          </ul>
        </div>
                                {!! $logs->appends(['limit' => (isset($_GET['limit']))?$_GET['limit']:10 ])->links('pagination::bootstrap-4') !!}
	</div>
</div>
    </div>
    <div id="menu2" class="tab-pane fade">
    <h3>Rest password </h3>
    <div class=" py-4">
      <div class="container">
        <form action=" {{url('updatePassword')}} " method="post" >
          {{ csrf_field() }}
          <div class="row ">
            <div class="group form-group col-md-6">
              <input type="password" name="password" class="form-control" required>
              <span class="highlight"></span>
              <label>Password</label>
            </div>
          </div>

          <div class="row ">
            <div class="group form-group col-md-6">
              <input type="password" name="confirm_password" class="form-control" required>
              <span class="highlight"></span>
              <label>Confirm Password</label>
            </div>
          </div>

        <button type="submit" class="btn btn-dark pd-x-20">Submit</button>
        </form>
      </div>
    </div>

    </div>
  </div>
</div>
<script>
    // $(function () {
    //     // get user timezone
    //     var Zone = Intl.DateTimeFormat().resolvedOptions().timeZone ;
    //     console.log(Zone);
    // })

    // $(document).ready(function(){
    //   var timezone_offset_minutes = new Date().getTimezoneOffset();
    //   timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

// Timezone difference in minutes such as 330 or -360 or 0
// console.log(timezone_offset_minutes); 
//       if($('#hd_current_time_zone').val() ==""){ // Check for hidden field is empty. if is it empty only execute the post function
//           var current_date = new Date();
//           curent_zone = -current_date.getTimezoneOffset() * 60;
//           var token = "{{csrf_token()}}";
//           $.ajax({
//             method: "POST",
//             url: "{{URL::to('ajax/set_current_time_zone/')}}",
//             data: {  '_token':token, curent_zone: curent_zone } 
//           }).done(function( data ){
//         });   
//       }       
// });
</script>


@endsection