@extends('layouts.app')
@section('content')


<div class="container">
  <h2>Health/Nutrition</h2>
  <div class="az-content az-content-app az-content-contacts pd-b-0">

@if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
@endif

@if ($errors->any())
<div class="alert alert-danger mb-0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Error!</strong> 
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

  
  <ul class="nav nav-pills my-2">
    <li class="nav-item">
      <a class="nav-link active" href="#menu1" data-toggle="pill">EHS - Health</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu2" data-toggle="pill">HS - Health</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu3" data-toggle="pill"> Nutrition</a>
    </li>

  </ul>

  <div class="tab-content">
    <div id="menu1" class="tab-pane active">
    <div id="Documentlist">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">EHS Attachments
            @if(auth()->user()['is_admin'] == 1)
                  <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'health-ehs'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            @endif
            </h5>
          </div>
          @php 
          $documentType = $documentTypes->where('type','health-ehs')->pluck('id')->first();
           if(!empty($documentType))
           $education = $resources->where('document_type',$documentType);
           else
           $education = [];
          @endphp
          @if($education->isNotEmpty())
          <table class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Additional info</th>
                    <th>Link/File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
           
            @foreach($education as $resource)
                <tr>
                    <td>{{$resource->topic}}</td>
                    <td>{{$resource->additional_info}}</td>
                    <td>@if(!empty($resource->link))
                        <a href="{{$resource->link}}" target="_blank">{{$resource->link}}</a>
                        @else
                        <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                        @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'])
                    <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=> 'health-ehs' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                    @else
                    NA
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
        </div>
    </div>
    <div id="menu2" class="tab-pane fade">
    <div id="Documentlist1">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title"> HS Attachments
            @if(auth()->user()['is_admin'] == 1)
                  <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'health-hs'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            @endif
            </h5>
          </div>
          @php 
          $documentType = $documentTypes->where('type','health-hs')->pluck('id')->first();
           if(!empty($documentType))
           $education = $resources->where('document_type',$documentType);
           else
           $education = [];
          @endphp
          @if($education->isNotEmpty())
          <table class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Additional info</th>
                    <th>File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
           
            @foreach($education as $resource)
                <tr>
                    <td>{{$resource->topic}}</td>
                    <td>{{$resource->additional_info}}</td>
                    <td>@if(!empty($resource->link))
                        <a href="{{$resource->link}}" target="_blank">{{$resource->link}}</a>
                        @else
                        <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                        @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'])
                    <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=> 'health-hs' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    @else
                    NA
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
        </div>
    </div>
    <div id="menu3" class="tab-pane fade">
    <div id="Resourceslist">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Nutrition Attachments
            @if(auth()->user()['is_admin'] == 1)
            <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'health-nutrition'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            @endif
            </h5>
          </div>
          @php 
          $documentType = $documentTypes->where('type','health-nutrition')->pluck('id')->first();
           if(!empty($documentType))
           $education = $resources->where('document_type',$documentType);
           else
           $education = [];
          @endphp
          @if($education->isNotEmpty())
          <table  class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Additional Info</th>
                    <th>Link/File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
           
            @foreach($education as $resource)
                <tr>
                    <td>{{$resource->topic}}</td>
                    <td>{{$resource->additional_info}}</td>
                    <td>@if(!empty($resource->link))
                        <a href="{{$resource->link}}" target="_blank">{{$resource->link}}</a>
                        @else
                        <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                        @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin']) 
                    <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=> 'health-nutrition' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    @else
                    NA
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
        </div>
    </div>
    

  </div>
</div>

</div>
  <script>
    function loadPage(viewId,pageId,url){
      $('#loadedPage').load(url+'/'+pageId);
      $('#loadedPage').show();
      $('#stafflist').hide();
    }

  </script>

@endsection