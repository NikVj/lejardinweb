@extends('layouts.app')
@section('content')
<div class="az-content az-content-app az-content-contacts pd-b-0">

@if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
@endif

@if ($errors->any())
<div class="alert alert-danger mb-0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Error!</strong> 
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div id="loadedPage" style="display:none"></div>

</div>

<div class="container">
  <h2>Disability</h2>
  <ul class="nav nav-pills my-2">
    <li class="nav-item">
      <a class="nav-link active" href="#home" data-toggle="pill">Staff</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu1" data-toggle="pill">Resources</a>
    </li>
  </ul>
 
  
  <div class="tab-content">
    <div id="home" class="tab-pane active">
    <div id="stafflist">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Disability Staff
            @if(auth()->user()['is_admin'] == 1)
              <a href="{{ url('editUser') }}" class=" medium " ><i class="fa fa-user-plus"></i></a>
            @endif
            </h5>
          </div>

          <table  class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Join Date</th>
                    <th>Photo</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($allUsers as $user)
                <tr>
                    <td>{{$user->firstname.' '.$user->lastname}}</td>
                    <td>{{$user->designation_name}}</td>
                    <td>{{$user->created_at}}</td>
                    <td><img class="img-thumbnail w-50px" src = "{{asset('images/users/'.$user->profile)}}"></td>
                    <td>{{$user->address1}}</td>
                    <td>
                      @if($user->status == 1)
                      <label class="badge badge-success"> Active</label>
                      @else
                      <label class="badge badge-danger"> Deactivated</label>
                      @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'] == 1)
                      {{getactionicons($user->id,'User')}}
                    @else
                    {{getactionicons($user->id,'User','V')}}
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
    <div id="menu1" class="tab-pane fade">
    <div id="Resourceslist">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Resources
            @if(auth()->user()['is_admin'] == 1)
            <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'disability-resource'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            @endif
            </h5>
          </div>
          @if($resources->isNotEmpty())
          <table  class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Additional Info</th>
                    <th>Link/File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
           @php
           $documentType = $documentTypes->where('type','disability-resource')->pluck('id')->first();
           if(!empty($documentType))
           $resources = $resources->where('document_type',$documentType);
           @endphp
            @foreach($resources as $resource)
                <tr>
                    <td>{{$resource->topic}}</td>
                    <td>{{$resource->additional_info}}</td>
                    <td>@if(!empty($resource->link))
                        <a href="{{$resource->link}}" target="_blank">{{$resource->link}}</a>
                        @else
                        <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                        @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'] == 1)
                    <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=> 'disability-resource' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    @else
                    NA
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
        </div>
    </div>
   
  </div>
</div>

  <script>
    function loadPage(viewId,pageId,url){
      $('#loadedPage').load(url+'/'+pageId);
      $('#loadedPage').show();
      $('#stafflist').hide();
    }

  </script>

 
@endsection