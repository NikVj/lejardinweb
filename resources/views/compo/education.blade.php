@extends('layouts.app')
@section('content')

<div class="az-content az-content-app az-content-contacts pd-b-0">

@if(session('success'))
  <div class="alert alert-success mg-b-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <strong>{{session('success')}}</strong>
  </div>
@endif

@if ($errors->any())
<div class="alert alert-danger mb-0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Error!</strong> 
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div id="loadedPage" style="display:none"></div>

</div>

<div class="container">
  <h2>Education</h2>

  <ul class="nav nav-pills my-2">
    <li class="nav-item">
      <a class="nav-link active" href="#home" data-toggle="pill">My Teacher</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu1" data-toggle="pill">Resources/Links</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu2" data-toggle="pill">Education Docs File</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu3" data-toggle="pill">School Rediness/VPK Assessment Result</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#menu4" data-toggle="pill">Teacher Conference</a>
    </li>
  </ul>
  
  <div class="tab-content">
    <div id="home" class="tab-pane  active">
    <div id="stafflist">
          <div class="col s12 m12 l12">
          @if(auth()->user()['is_admin'] == 1)
            <h5 class="breadcrumbs-title">My Teachers
              <a href="{{ url('editUser') }}" class=" medium " ><i class="fa fa-user-plus"></i></a>
            </h5>
          @endif
          </div>

          <table class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Email</th>
                    <th>Photo</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($allUsers as $user)
                <tr>
                    <td>{{$user->firstname.' '.$user->lastname}}</td>
                    <td>{{$user->designation_name}}</td>
                    <td>{{$user->email}}</td>
                    <td><img class="img-thumbnail w-50px" src = "{{asset('images/users/'.$user->profile)}}"></td>
                    <td>{{$user->address1}}</td>
                    <td>
                      @if($user->status == 1)
                      <label class="badge badge-success"> Active</label>
                      @else
                      <label class="badge badge-danger"> Deactivated</label>
                      @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'] == 1)
                      {{getactionicons($user->id,'User')}}
                    @else
                    {{getactionicons($user->id,'User','V')}}
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
    <div id="menu1" class="tab-pane fade">
    <div id="Resourceslist">
          <div class="col s12 m12 l12">
          @if(auth()->user()['is_admin'] == 1)
             <h5 class="breadcrumbs-title">Resources
              <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'education-resource'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            </h5>
            @endif
          </div>
          @if($resources->isNotEmpty())
          <table  class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Additional Info</th>
                    <th>Link/File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
           @php 
             $documentType = $documentTypes->where('type','education-resource')->pluck('id')->first();
             if(!empty($documentType))
             $reso = $resources->where('document_type',$documentType);
             else
             $reso = [];
           @endphp
            @foreach($reso as $resource)
                <tr>
                    <td>{{$resource->topic}}</td>
                    <td>{{$resource->additional_info}}</td>
                    <td>@if(!empty($resource->link))
                        <a href="{{$resource->link}}" target="_blank">{{$resource->link}} </a>
                        @else
                        <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                        @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'] == 1)
                    <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=> 'education-resource' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    @else
                    NA
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
        </div>
    </div>
    <div id="menu2" class="tab-pane fade">
      <div id="Documentlist">
          <div class="col s12 m12 l12">
          @if(auth()->user()['is_admin'] == 1)
            <h5 class="breadcrumbs-title">
                <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'education-docs'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            </h5>
           @endif
          </div>
          @php 
           $documentType = $documentTypes->where('type','education-docs')->pluck('id')->first();
           if(!empty($documentType))
           $education = $resources->where('document_type',$documentType);
           else
           $education = [];
          @endphp
          @if($education->isNotEmpty())
          <table  class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Topic</th>
                    <th>Additional Info</th>
                    <th>Link/File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
           
            @foreach($education as $resource)
                <tr>
                    <td>{{$resource->topic}}</td>
                    <td>{{$resource->additional_info}}</td>
                    <td>@if(!empty($resource->link))
                        <a href="{{$resource->link}}" target="_blank">{{$resource->link}}</a>
                        @else
                        <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                        @endif
                    </td>
                    <td>
                    @if(auth()->user()['is_admin'] == 1)
                    <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=> 'education-docs' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    @else
                    NA
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
        </div>
              
 </div>
    <div id="menu3" class="tab-pane fade">
      <div id="Resultlist">
          <div class="col s12 m12 l12">
            @if(auth()->user()['is_admin'] == 1)
                      <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.add', ['type' => $component->code,'document_id' => 'education-result-rediness'] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></a>
            @endif
           
          </div>
          @php 
          $documentType = $documentTypes->where('type','education-result-rediness')->pluck('id')->first();
           if(!empty($documentType))
           $education = $resources->where('document_type',$documentType);
           else
           $education = [];
          @endphp
          @if($education->isNotEmpty())
            <table class="table table-bordered table-hover ListAll">
              <thead>
                  <tr>
                      <th>Topic</th>
                      <th>Additional Info 
                      <th>Link/File</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
             
              @foreach($education as $resource)
                  <tr>
                      <td>{{$resource->topic}}</td>
                      <td>{{$resource->additional_info}}</td>
                      <td>@if(!empty($resource->link))
                          <a href="{{$resource->link}}" target="_blank">{{$resource->link}}</a>
                          @else
                          <a href="{{ asset('resources/'.$component->code.'/'.$resource->file) }}" target="_blank" >{{$resource->file}}</a>
                          @endif
                      </td>
                      <td>
                      @if(auth()->user()['is_admin'] == 1)
                      <a class="btn btn-success btn-sm rounded-5" href="{{ route('document.type.edit', ['type' => $component->code,'document_id'=>'education-result-rediness' ,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-danger btn-sm rounded-5" href="{{ route('resource.delete', ['type' => $component->code,'id' => $resource->id] ) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                      @else
                      NA
                      @endif
                      </td>
                  </tr>
              @endforeach
              </tbody>
            </table>
          @else
              <p> No Record Found </p>
          @endif
        </div>
    </div>
    <div id="menu4" class="tab-pane fade">
      <h3>Conference</h3>
          @if(!empty($conferences))
          <table class="table table-bordered table-hover ListAll">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Staff Members</th>
                    <th>Parents</th>
                </tr>
            </thead>
            <tbody>
           
            @foreach($conferences as $conference)
                <tr>
                    <td><p>{{$conference['title']}}</p></td>
                    <td>{{($conference['start_date'])}} </td>
                    <td>{{($conference['end_date'])}} </td>
                    <td>
                      <div class=""> 
                        @php 
                        $recCount = 1;
                        @endphp
                        @foreach($conference['staff'] as $rec)
                          <a href="{{url('viewUser/'.$rec['id'])}}"><img src="{{asset('images/users/'.$rec['profile'])}}" class="img-circle my-1 <?= (($recCount > 3) ? 'morerec' : '') ?> " style="width:38px; <?= (($recCount > 3) ? 'display: none' : '') ?> " title="{{$rec['name']}}"></a>
                          @if($recCount == 4)
                            <button href="JavaScript:;" class="btn btn-xs btn-outline btn-dark" onclick="$(this).parent().find('.morerec').toggle(200);$(this).toggle()">{{count($conference->staff) - 3}} MORE</button>
                          @endif
                          @php 
                            $recCount++;
                          @endphp
                        @endforeach
                      </div>
                    </td>
                    <td>
                      <div class=""> 
                        @php 
                        $recCount = 1;
                        @endphp
                        @foreach($conference['parent'] as $rec)
                          <a href="{{url('viewUser/'.$rec['id'])}}"><img src="{{asset('images/users/'.$rec['profile'])}}" class="img-circle my-1 <?= (($recCount > 3) ? 'morerec' : '') ?> " style="width:38px; <?= (($recCount > 3) ? 'display: none' : '') ?> " title="{{$rec['name']}}"></a>
                          @if($recCount == 4)
                            <button href="JavaScript:;" class="btn btn-xs btn-outline btn-dark" onclick="$(this).parent().find('.morerec').toggle(200);$(this).toggle()">{{count($conference->parent) - 3}} MORE</button>
                          @endif
                          @php 
                            $recCount++;
                          @endphp
                        @endforeach
                      </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p> No Record Found </p>
        @endif
      <!-- <p>- VPK Parent Teacher Conference</p>
      <p>- School Readiness Parent Teacher Conference</p> -->
    </div>
  </div>
</div>

  <script>
    function loadPage(viewId,pageId,url){
      $('#loadedPage').load(url+'/'+pageId);
      $('#loadedPage').show();
      $('#stafflist').hide();
    }

  </script>

@endsection