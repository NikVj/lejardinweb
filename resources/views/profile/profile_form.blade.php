<link rel="stylesheet" href="{{ asset('assets/lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets//css/form_elements.css') }}">

<div class="az-content-body az-content-profile-form">
	<?php
		// pr(userData());
	?>

			<form action="updateUserProfile" method="post" data-parsley-validate>
				<div class="row">
					{{@csrf_field()}}
					<input type="hidden" name="userid" value="{{userData()->id}}">
					<div class="col-md-12">
						<div class="dis-flex">
							<span class="az-content-title">Edit</span>
							<div id="slWrapper" class="parsley-select mg-l-10">
								<select name="designation" class="form-control select2" data-placeholder="Choose Designation" data-parsley-class-handler="#slWrapper" data-parsley-errors-container="#slErrorContainer">
									<option label="Choose Designation"></option>
									@foreach(userDesignations() as $userDesignation)
										@if (!empty(userData()->designation) && $userDesignation['designation_name'] == userData()->designation)
											<option value="{{$userDesignation['designation_name']}}" selected>{{$userDesignation['designation_name']}}</option>
										@else
											<option value="{{$userDesignation['designation_name']}}">{{$userDesignation['designation_name']}}</option>
										@endif
									@endforeach
								</select>
								<div id="slErrorContainer"></div>
							</div>
						</div>

						<div class="btn-action tx-20">
					        <a href="javascript:;" class="text-info mr-3" onclick="loadPage('loadedPage','0','{{ url('profileform') }}');$('.az-content-profile').hide();">
					        	<i class="fas fa-sync"></i>
					        </a>
					        <a href="javascript:;" class="text-secondary mr-3" onclick="$('.az-content-profile,.az-content-profile-form').toggle();">
					        	<i class="fa fa-times"></i>
					        </a>
					     </div>
					</div>

					<div class="col-md-6">

						<div class="group">
							<input type="text" name="name" class="form-control" value="@if(!empty(userData()->name)) {{userData()->name}} @endif" required>
							<span class="highlight"></span>
							<label>Name</label>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="git_acc" class="form-control" value="@if(!empty(userData()->git_acc)) {{userData()->git_acc}} @endif">
									<span class="highlight"></span>
									<label>GIT</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="twitter_acc" class="form-control" value="@if(!empty(userData()->twitter_acc)) {{userData()->twitter_acc}} @endif">
									<span class="highlight"></span>
									<label>Twitter</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="linkedin_acc" class="form-control" value="@if(!empty(userData()->linkedin_acc)) {{userData()->linkedin_acc}} @endif">
									<span class="highlight"></span>
									<label>Linkedin</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="facebook_acc" class="form-control" value="@if(!empty(userData()->facebook_acc)) {{userData()->facebook_acc}} @endif">
									<span class="highlight"></span>
									<label>Facebook</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="outlook_acc" class="form-control" value="@if(!empty(userData()->outlook_acc)) {{userData()->outlook_acc}} @endif">
									<span class="highlight"></span>
									<label>Outlook</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="instagram_acc" class="form-control" value="@if(!empty(userData()->instagram_acc)) {{userData()->instagram_acc}} @endif">
									<span class="highlight"></span>
									<label>Instagram</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="slack_acc" class="form-control" value="@if(!empty(userData()->slack_acc)) {{userData()->slack_acc}} @endif">
									<span class="highlight"></span>
									<label>Slack</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="my_acc" class="form-control" value="@if(!empty(userData()->my_acc)) {{userData()->my_acc}} @endif">
									<span class="highlight"></span>
									<label>My Account</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="aadhar_no" class="form-control" value="@if(!empty(userData()->aadhar_no)) {{userData()->aadhar_no}} @endif">
									<span class="highlight"></span>
									<label>Aadhar Number</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="pan_no" class="form-control" value="@if(!empty(userData()->pan_no)) {{userData()->pan_no}} @endif">
									<span class="highlight"></span>
									<label>PAN Number</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="website" class="form-control" value="@if(!empty(userData()->website)) {{userData()->website}} @endif">
									<span class="highlight"></span>
									<label>Website</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="other_acc" class="form-control" value="@if(!empty(userData()->other_acc)) {{userData()->other_acc}} @endif">
									<span class="highlight"></span>
									<label>Other Account</label>
								</div>
							</div>
						</div>
					
					</div>

					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="phone" class="form-control phoneMask" value="@if(!empty(userData()->phone)) {{userData()->phone}} @endif" required>
									<span class="highlight"></span>
									<label>Work Contact</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="phone_home" class="form-control phoneMask" value="@if(!empty(userData()->phone_home)) {{userData()->phone_home}} @endif">
									<span class="highlight"></span>
									<label>Personal Contact</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="city" class="form-control" value="@if(!empty(userData()->city)) {{userData()->city}} @endif">
									<span class="highlight"></span>
									<label>City</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="state" class="form-control" value="@if(!empty(userData()->state)) {{userData()->state}} @endif">
									<span class="highlight"></span>
									<label>State</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="country" class="form-control" value="@if(!empty(userData()->country)) {{userData()->country}} @endif">
									<span class="highlight"></span>
									<label>Country</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="group form-group">
									<input type="text" name="zip" class="form-control" value="@if(!empty(userData()->zip)) {{userData()->zip}} @endif">
									<span class="highlight"></span>
									<label>ZIP</label>
								</div>
							</div>
						</div>

						<div class="group form-group">
							<label class="label-textarea">Current Address</label>
							<textarea name="address1" rows="3" class="form-control" placeholder="Type Here" required>@if(!empty(userData()->address1)) {{userData()->address1}} @endif</textarea>
						</div>

						<div class="group form-group">
							<label class="label-textarea">Permanent Address</label>
							<textarea name="address2" rows="3" class="form-control" placeholder="Type Here">@if(!empty(userData()->address2)) {{userData()->address2}} @endif</textarea>
						</div>
					</div>

				</div>

				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
				</div>
			</form>

</div>

<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/lib/parsleyjs/parsley.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

<script>
	$(function(){
		'use strict'
		$(document).ready(function(){
			$('.select2').select2({
				placeholder: 'Choose Designation'
			});

			$('.select2-no-search').select2({
				minimumResultsForSearch: Infinity,
				placeholder: 'Choose Designation'
			});
		});
		$('#selectForm').parsley();
		$('#selectForm2').parsley();

		$('.phoneMask').mask('(999) 999-9999');
	});
</script>