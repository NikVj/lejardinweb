<div class="az-content-body az-content-profile-form">
	<form action="updateUserSkills" method="post" data-parsley-validate>
		<div class="row">
			{{@csrf_field()}}
			<input type="hidden" name="userid" value="{{userData()->id}}">

			<div class="col-md-12">
				<div class="float-right tx-20">
					<a href="javascript:;" class="text-info mr-3" onclick="loadPage('loadedPage','0','{{ url('profileskillsform') }}');$('.az-content-profile').hide();">
						<i class="fas fa-sync"></i>
					</a>
					<a href="javascript:;" class="text-secondary mr-3" onclick="$('.az-content-profile,.az-content-profile-form').toggle();">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>

			<div class="col-md-12 mb-4">
				<p class="az-content-title mb-3">Skill Sets</p>
				<table class="w-100">
					<thead>
						<tr>
							<th>Name</th>
							<th>Knowledge(%)</th>
							<th>Color</th>
							<th style="min-width: 30px"></th>
						</tr>
					</thead>
					<tbody id="tableSkills">
						@if (!empty(userData()->education))
						@foreach (json_decode(userData()->skill, true) as $skill)
						<tr>
							<td>
								<input type="text" class="form-control td_recip" name="skill_name[]" value="{{$skill['name']}}" placeholder="Name">
							</td>

							<td>
								<input type="number" name="skill_number[]" value="{{$skill['number']}}" class="form-control" min="0" max="100" placeholder="Knowledge(%)">
							</td>

							<td>
								<input type="text" name="skill_bgcolor[]" value="{{$skill['bgcolor']}}" class="showPaletteOnly" style="display: none;">

								<!-- <input type="text" name="skill_bgcolor[]" value="{{$skill['bgcolor']}}" class="form-control" placeholder="Color"> -->
							</td>

							<td class="text-center">
								<a href="javascript:void(0);" onclick="delete_proposal_type(this)" class="deleteRecip" style="color: red; padding-left: 5px;">
									<span class="badge bg-danger">
										<i class="fa fa-times text-white"></i>
									</span>
								</a>
							</td>
						</tr>
						@endforeach
						@endif

						<tr>
							<td>
								<input type="text" class="form-control td_recip" name="skill_name[]" placeholder="Name" onfocus="addmore_recipient(this, 'tableSkills')">
							</td>

							<td>
								<input type="number" name="skill_number[]" class="form-control" min="0" max="100" placeholder="Knowledge(%)">
							</td>

							<td>
								
								<input type="text" name="skill_bgcolor[]" class="showPaletteOnly" style="display: none;">

								<!-- <input type="text" name="skill_bgcolor[]" class="form-control" placeholder="Color"> -->
							</td>

							<td class="text-center">
								<a href="javascript:void(0);" onclick="delete_proposal_type(this)" class="deleteRecip" style="color: red; padding-left: 5px; display: none;">
									<span class="badge bg-danger">
										<i class="fa fa-times text-white"></i>
									</span>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12 mb-4">
				<p class="az-content-title mb-3">Education</p>
				<table class="w-100">
					<thead>
						<tr>
							<th>Name</th>
							<th>University</th>
							<th>From(Year)</th>
							<th>To(Year)</th>
							<th style="min-width: 30px"></th>
						</tr>
					</thead>
					<tbody id="tableEducations">
						@if (!empty(userData()->education))
						@foreach (json_decode(userData()->education, true) as $educ)
						<tr>
							<td>
								<input type="text" class="form-control td_recip" name="edu_name[]" value="{{$educ['name']}}" placeholder="Name">
							</td>

							<td>
								<input type="text" name="edu_university[]" value="{{$educ['university']}}" class="form-control" placeholder="University">
							</td>

							<td>
								<input type="number" name="edu_from[]" value="{{$educ['from']}}" class="form-control" min="1901" max="2050" placeholder="From Year">
							</td>

							<td>
								<input type="number" name="edu_to[]" value="{{$educ['to']}}" class="form-control" min="1901" max="2050" placeholder="To Year">
							</td>

							<td class="text-center">
								<a href="javascript:void(0);" onclick="delete_proposal_type(this)" class="deleteRecip" style="color: red; padding-left: 5px;">
									<span class="badge bg-danger">
										<i class="fa fa-times text-white"></i>
									</span>
								</a>
							</td>
						</tr>
						@endforeach
						@endif

						<tr>
							<td>
								<input type="text" class="form-control td_recip" name="edu_name[]" placeholder="Name" onfocus="addmore_recipient(this, 'tableEducations')">
							</td>

							<td>
								<input type="text" name="edu_university[]" class="form-control" placeholder="University">
							</td>

							<td>
								<input type="number" name="edu_from[]" class="form-control" min="1901" max="2050" placeholder="From Year">
							</td>

							<td>
								<input type="number" name="edu_to[]" class="form-control" min="1901" max="2050" placeholder="To Year">
							</td>

							<td class="text-center">
								<a href="javascript:void(0);" onclick="delete_proposal_type(this)" class="deleteRecip" style="color: red; padding-left: 5px; display: none;">
									<span class="badge bg-danger">
										<i class="fa fa-times text-white"></i>
									</span>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12 mb-4">
				<p class="az-content-title mb-3">Work Experience</p>
				<table class="w-100">
					<thead>
						<tr>
							<th>Organization</th>
							<th>Designation</th>
							<th>From(Year)</th>
							<th>To(Year)</th>
							<th>Website</th>
							<th style="min-width: 30px"></th>
						</tr>
					</thead>
					<tbody id="tableWork">
						@if (!empty(userData()->education))
						@foreach (json_decode(userData()->work, true) as $work)
						<tr>
							<td>
								<input type="text" class="form-control td_recip" name="work_organization[]" value="{{$work['organization']}}" placeholder="Organization">
							</td>

							<td>
								<input type="text" name="work_designation[]" value="{{$work['designation']}}" class="form-control" placeholder="Designation">
							</td>

							<td>
								<input type="number" name="work_from[]" value="{{$work['from']}}" class="form-control" min="1901" max="2050" placeholder="From Year">
							</td>

							<td>
								<input type="number" name="work_to[]" value="{{$work['to']}}" class="form-control" min="1901" max="2050" placeholder="To Year">
							</td>

							<td>
								<input type="text" name="work_website[]" value="{{$work['website']}}" class="form-control" placeholder="Website">
							</td>

							<td class="text-center">
								<a href="javascript:void(0);" onclick="delete_proposal_type(this)" class="deleteRecip" style="color: red; padding-left: 5px;">
									<span class="badge bg-danger">
										<i class="fa fa-times text-white"></i>
									</span>
								</a>
							</td>
						</tr>
						@endforeach
						@endif

						<tr>
							<td>
								<input type="text" class="form-control td_recip" name="work_organization[]" placeholder="Organization" onfocus="addmore_recipient(this, 'tableWork')">
							</td>

							<td>
								<input type="text" name="work_designation[]" class="form-control" placeholder="Designation">
							</td>

							<td>
								<input type="number" name="work_from[]" class="form-control" min="1901" max="2050" placeholder="From Year">
							</td>

							<td>
								<input type="number" name="work_to[]" class="form-control" min="1901" max="2050" placeholder="To Year">
							</td>

							<td>
								<input type="text" name="work_website[]" class="form-control" placeholder="Website">
							</td>

							<td class="text-center">
								<a href="javascript:void(0);" onclick="delete_proposal_type(this)" class="deleteRecip" style="color: red; padding-left: 5px; display: none;">
									<span class="badge bg-danger">
										<i class="fa fa-times text-white"></i>
									</span>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-dark pd-x-20">Submit</button>
		</div>
	</form>
</div>


<script>
	$(function(){
		'use strict'
		$('#colorpicker').spectrum({
			color: '#17A2B8'
		});

		$('#showAlpha').spectrum({
			color: 'rgba(23,162,184,0.5)',
			showAlpha: true
		});

		$('.showPaletteOnly').spectrum({
			showPaletteOnly: true,
			showPalette:true,
			color: '#DC3545',
			palette: [
			['#1D2939', '#fff', '#0866C6','#23BF08', '#F49917'],
			['#DC3545', '#17A2B8', '#6610F2', '#fa1e81', '#72e7a6']
			]
		});

	});
</script>

<script src="{{ asset('assets/lib/spectrum-colorpicker/spectrum.js') }}"></script>
<link href="{{ asset('assets/lib/spectrum-colorpicker/spectrum.css') }}" rel="stylesheet">


<script type="text/javascript">
	function addmore_recipient(position, table){
		$(position).removeAttr("onfocus");
		$('#'+table+' .deleteRecip').show();
		var item = $('#'+table+" tr:last");
		var cloneItem = item.clone();

		cloneItem.find('a').hide();
		$('#'+table).append(cloneItem);
		$('#'+table+" .td_recip:last").attr('onfocus', 'addmore_recipient(this, "'+table+'")');
	}
	function delete_proposal_type(obj){
		obj.closest("tr").remove();
	}
</script>