
      $(document).ready( function () {
        $('.ListAll').DataTable();
        $('.select2').select2();
    } );
    // $('.chats_container,.direct-chat-contacts').perfectScrollbar({
    $('.direct-chat-contacts').perfectScrollbar({
        useBothWheelAxes: false,
        suppressScrollX: true
    });

function changedate(unix_timestamp,datepattern = ''){
  var date = new Date(unix_timestamp * 1000);
  // Hours part from the timestamp
  var day = "0" + date.getDate();
  // Minutes part from the timestamp
  var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
       
  var monthNumber ="0" + ( date.getMonth() );

  var month = months[monthNumber];
  // Seconds part from the timestamp
  var year = "0" + date.getFullYear();
  // Hours part from the timestamp
  var hours = date.getHours();
  // Minutes part from the timestamp
  var minutes = "0" + date.getMinutes();
  // Seconds part from the timestamp
  var seconds = "0" + date.getSeconds();
  monthNumber++;
  var monthNumber ="0" +monthNumber;
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  
  // Will display time in 10:30:23 format
  if(datepattern == ''){
    var formattedTime =  day.substr(-2) + ' ' + month.substr(0,4) + ' ' +year.substr(-2)+ ', '+ hours + ':' + minutes.substr(-2) + ' ' +ampm ;
  }else{
    if(datepattern == 'dmy'){
      var formattedTime =  day.substr(-2) + ' ' + month.substr(0,4) + ', ' +year.substr(-4);
    }
    if(datepattern == 'd/m/y'){
      var formattedTime =  day.substr(-2) + '/' + monthNumber.substr(-2) + '/' +year.substr(-4);
    }
  }

  return(formattedTime);
}

    function chatBackMobile(e) {
        if (window.matchMedia("only screen and (max-width: 767px)").matches) {
            $('.direct-chat-contacts').fadeIn(50);
            $('#chat_box_'+$(e).data('userid')).animate({
                "left": '+=100%',
                'opacity': '0'
            }, 200, 'linear', function() {
                $(e).hide();
            });
        }
    }

    $('.like-btn').click(function(){
        var ele = $(this);
        var feedID = $(this).data('feedid');
        $.ajax({
            url: base_url + "/likeFeed" ,
            data: {id: feedID, _token: $("meta[name='csrf-token']").attr("content")},
            method:"GET",
            dataType: "json",
            success:function(data){
                if(data.status == true){
                    if(data.like == '2'){
                        ele.addClass('liked_already');
                        var numberFeedLike=$('#number_like_'+feedID).data('number_feed_like')+1;
                    }
                    else{
                        ele.removeClass('liked_already');
                        var numberFeedLike=$('#number_like_'+feedID).data('number_feed_like')-1;
                    }
                        $('#number_like_'+feedID).data('number_feed_like',numberFeedLike);
                        if(numberFeedLike > 0){
                            $('#number_like_'+feedID).text('('+ numberFeedLike +')');
                        }else{
                            $('#number_like_'+feedID).text('');
                        }
                    toastr.success(data.message);
                }
            }
        });
    });
    // $('.add_comment').keydown(function(e){
    //     var id = $(e).data('feedid');
    //     var post_id = $(e).parent('div').parent('div').data('feedid');
    //     if(id != post_id){
    //         return false;
    //     }
    // });
    $('.add_comment').keyup(function(e){
        var id = $(this).data('feedid');
        var comment = $(this).val();
        var ele = $(this);
        // var token = $("meta[name='csrf-token']").attr("content");
        if ( e.which == 13 ) {
            $.ajax({
                url: base_url + "/addComment" ,
                data: {id: id,comment: comment, _token: token},
                method:"POST",
                dataType: "json",
                success:function(data){
                    if(data.status == true){
                        var comment = data.comment;
                        var newHtml  = '<div class="panel-footer post-comment" id="comment_div_'+comment.id+'"><div class="comment" data-commentid="comment_"'+comment.id+'><div class="pull-left comment-image "><a href="JavaScript:;"><img src="'+base_url+'/images/users/'+comment.comment_added_by['profile']+'" class=" img-circle img-bordered-sm" alt=""></a></div><span class="pull-right"><a href="Javascript:;" data-commentid="'+comment.id+'"  onclick="remove_post_comment('+comment.id+','+comment.feed_id+');" class="remove-post-comment"><i class="fa fa-times bold"></i></a></span><div class="media-body"><p class="no-margin comment-content"><a href="JavaScript:;">'+comment.comment_added_by['firstname']+((comment.comment_added_by['lastname'] != null && comment.comment_added_by['lastname'] != '') ? (' '+comment.comment_added_by['lastname']) : '')+'</a> '+comment.comment+'</p><p class="no-margin"></a><small> just now</small></p></div></div><div class="clearfix"></div></div>';
                        // console.log(newHtml);
                        $('#post_comments_wrapper_'+id).prepend(newHtml);
                        ele.val('');
                        var numberFeedComment=$('#number_comment_'+id).data('number_feed_comment')+1;
                        $('#number_comment_'+id).data('number_feed_comment',numberFeedComment);
                        if(numberFeedComment > 0){
                            $('#number_comment_'+id).text('('+numberFeedComment+')');
                        }else{
                            $('#number_comment_'+id).text('');
                        }
                    }
                }
            });

        }
    });
    function remove_post_comment(id, feed_id){
        // var id = $(this).data('commentid');
        $.ajax({
            url: base_url + "/removeComment" ,
            data: {id: id, _token: token},
            method:"POST",
            dataType: "json",
            success:function(data){
                if(data.status == true){
                    $('#comment_div_'+id).remove();
                    var numberFeedComment=$('#number_comment_'+feed_id).data('number_feed_comment')-1;
                    $('#number_comment_'+feed_id).data('number_feed_comment',numberFeedComment);
                    if(numberFeedComment > 0){
                        $('#number_comment_'+feed_id).text('('+numberFeedComment+')');
                    }else{
                        $('#number_comment_'+feed_id).text('');
                    }

                }
            }
        });
    };