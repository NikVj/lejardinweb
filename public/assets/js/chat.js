$(function () {
   let pusher = new Pusher($("#pusher_app_key").val(), {
        cluster: $("#pusher_cluster").val(),
        encrypted: true
    });

    let channel = pusher.subscribe('chat');


   // on close chat close the chat box but don't remove it from the dom
   $(".close-chat").on("click", function (e) {

       $(this).parents("div.chat-opened").removeClass("chat-opened").slideUp("fast");
   });


    let lastScrollTop = 0;
    // $(".chats_container").on("scroll", function (e) {
    $(".chat-area").on("scroll", function (e) {
       let st = $(this).scrollTop();
       // let currentChat = $(this).find('.chat-opened');
       let currentChat = $(this).parent('.chat-opened');
       if(st < lastScrollTop) {
           fetchOldMessages((currentChat).find("#to_user_id").val(), (currentChat).find(".chat-area").find('.direct-chat-msg').first().attr("data-message-id"));
           // fetchOldMessages($(this).parents(".chat-opened").find("#to_user_id").val(), $(this).find(".msg_container:first-child").attr("data-message-id"));
       }
       lastScrollTop = st;
       // console.log('lastScrollTop'+lastScrollTop);
       // console.log('st'+st);
    });
    // listen for the oldMsgs event, this event will be triggered on scroll top
    channel.bind('oldMsgs', function(data) {
        displayOldMessages(data);
    });

    $(document).on('change', '[name="chat_file"]', function (e) { 
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        var _this = $(this);
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {console.log("Cover image has an invalid extension. Valid extension(s) ");
            toastr.error( "Cover image has an invalid extension. Valid extension(s) : " + fileExtension.join(', '));
            _this.val('');
            return;
        } else if (this.files[0].size > (5 * 1024 * 1024)) {
            toastr.error( "Max 5MB of cover file size allowed");
            _this.val('');
            return;
        } else {
            var reader = new FileReader();
            var baseString;

            reader.onloadend = function () {
                baseString = reader.result;console.log(this);
                send(_this.attr('data-to-user'), $("#chat_box_" + _this.attr('data-to-user')).find(".chat_input").val(), $("#chat_box_" + _this.attr('data-to-user')).attr("data-group_id"));
                // $('#uploaded_image').attr('src', baseString);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
});
    /**
     * fetchOldMessages
     *
     * this function load the old messages if scroll up triggerd
     *
     * @param to_user
     * @param old_message_id
     */
    function fetchOldMessages(to_user, old_message_id)
    {
        let chat_box = $("#chat_box_" + to_user);
        let chat_area = chat_box.find(".chat-area");
        $.ajax({
            url: base_url + "/fetch-old-messages",
            data: {to_user: to_user, old_message_id: old_message_id, _token: $("meta[name='csrf-token']").attr("content")},
            method: "GET",
            dataType: "json",
            beforeSend: function () {
                if(chat_area.find(".loader").length  == 0) {
                    chat_area.prepend(loaderHtml());
                }
            },
            success: function (response) {
                displayOldMessages(response,to_user);
            },
            complete: function () {
                chat_area.find(".loader").remove();
            }
        });
    }
    function displayOldMessages(data,to_user='')
    {
        if(data.data.length > 0) {
            data.data.map(function (val, index) {
                if(to_user == '')
                    $("#chat_box_" + data.to_user).find(".chat-area").prepend(val);
                else
                    $("#chat_box_" + to_user).find(".chat-area").prepend(val);
            });
        }
    }


/**
 * loaderHtml
 *
 * @returns {string}
 */
function loaderHtml() {
    return '<i class="fa fa-refresh"></i>';
}

/**
 * getMessageSenderHtml
 *
 * this is the message template for the sender
 *
 * @param message
 * @returns {string}
 */
function getMessageSenderHtml(message)
{
          return `            <div class="direct-chat-primary"><div class="direct-chat-msg right msg_container base_sent" data-message-id="${message.id}">
                        <div class="direct-chat-infos clearfix">
                            <div class="messages msg_sent float-right">
                                <span class="direct-chat-name ">${message.fromUserName }</span>
                                <!-- <time datetime="${message.dateTimeStr}" class="direct-chat-timestamp float-left">${message.fromUserName} • ${message.dateHumanReadable}</time> -->
                            </div>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img avatar" src="images/users/${message.fromUserImage }" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text float-right">
                          ${message.content != 'text' ? message.content : ('<img src='+message.content+'>')}
                        </div>
                        <!-- /.direct-chat-text -->
                      </div></div>`;
    // return `
    //        <div class="row msg_container base_sent" data-message-id="${message.id}">
    //     <div class="col-md-10 col-xs-10">
    //         <div class="messages msg_sent text-right">
    //             <p>${message.content}</p>
    //             <time datetime="${message.dateTimeStr}"> ${message.fromUserName} • ${message.dateHumanReadable} </time>
    //         </div>
    //     </div>
    //     <div class="col-md-2 col-xs-2 avatar">
    //         <img src="` + base_url +  '/images/user-avatar.png' + `" width="50" height="50" class="img-responsive">
    //     </div>
    // </div>
    // `;
}

/**
 * getMessageReceiverHtml
 *
 * this is the message template for the receiver
 *
 * @param message
 * @returns {string}
 */
function getMessageReceiverHtml(message)
{
          return `          <div class="direct-chat-primary">  <div class="direct-chat-msg left msg_container base_receive" data-message-id="${message.id}">
                        <div class="direct-chat-infos clearfix">
                            <div class="messages msg_receive float-left">
                                <span class="direct-chat-name ">${message.fromUserName }</span>
                                <!-- <time datetime="${message.dateTimeStr}" class="direct-chat-timestamp float-left">${message.fromUserName} • ${message.dateHumanReadable}</time> -->
                            </div>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img avatar" src="images/users/${message.fromUserImage }" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text float-left">
                          ${message.content}
                        </div>
                        <!-- /.direct-chat-text -->
                      </div></div>`;
    // return `
    //        <div class="row msg_container base_receive" data-message-id="${message.id}">
    //        <div class="col-md-2 col-xs-2 avatar">
    //          <img src="` + base_url +  '/images/user-avatar.png' + `" width="50" height="50" class="img-responsive">
    //        </div>
    //     <div class="col-md-10 col-xs-10">
    //         <div class="messages msg_receive text-left">
    //             <p>${message.content}</p>
    //             <time datetime="${message.dateTimeStr}"> ${message.fromUserName}  • ${message.dateHumanReadable} </time>
    //         </div>
    //     </div>
    // </div>
    // `;
}




$(function () {
   let pusher = new Pusher($("#pusher_app_key").val(), {
        cluster: $("#pusher_cluster").val(),
        encrypted: true
    });

    let channel = pusher.subscribe('chat');
    // console.log(pusher);

    // on click on any chat btn render the chat box
   $(document).on('click', ".chat-toggle", function (e) {
       e.preventDefault();
       // $('.chats_container').scrollTop(0);
        if (window.matchMedia("only screen and (max-width: 767px)").matches) {
            $('.direct-chat-contacts').hide();
        }

       let ele = $(this);

       let user_id = ele.attr("data-id");

       let group_id = ele.attr("data-group_id");

       $('#unread_count_'+group_id).remove();

       let username = ele.attr("data-user");
        $('.chat-opened').remove();
       cloneChatBox(user_id, group_id, username, function () {

           let chatBox = $("#chat_box_" + user_id);

           if(!chatBox.hasClass("chat-opened")) {

               chatBox.addClass("chat-opened").slideDown("fast");

               loadLatestMessages(chatBox, user_id, group_id);
                setTimeout(function(){ 
                    chatBox.find(".chat-area").animate({scrollTop: chatBox.find(".chat-area").get(0).scrollHeight}, 800, 'swing');
                }, 500);
               // chatBox.find(".chat-area").animate({scrollTop: chatBox.find(".chat-area").offset().top + chatBox.find(".chat-area").outerHeight(true)}, 800, 'swing');
                // chatBox.perfectScrollbar('update');
           }
       });
   });

   // on close chat close the chat box but don't remove it from the dom
   $(".close-chat").on("click", function (e) {

       $(this).parents("div.chat-opened").removeClass("chat-opened").slideUp("fast");
   });


   
    
    
    // on change chat input text toggle the chat btn disabled state
    $(".chat_input").on("change keyup", function (e) {
       if($(this).val() != "") {
           $(this).parents(".form-controls").find(".btn-chat").prop("disabled", false);
       } else {
           $(this).parents(".form-controls").find(".btn-chat").prop("disabled", true);
       }
    });


    // on click the btn send the message
   $(".btn-chat").on("click", function (e) {
        $(this).attr('disabled','disabled');
       send($(this).attr('data-to-user'), $("#chat_box_" + $(this).attr('data-to-user')).find(".chat_input").val(), $("#chat_box_" + $(this).attr('data-to-user')).attr("data-group_id"));
   });

   // listen for the send event, this event will be triggered on click the send btn
    channel.bind('send', function(data) {
        displayMessage(data.data);
    });
});


/**
 * loaderHtml
 *
 * @returns {string}
 */
function loaderHtml() {
    return '<i class="glyphicon glyphicon-refresh loader"></i>';
}

/**
 * getMessageSenderHtml
 *
 * this is the message template for the sender
 *
 * @param message
 * @returns {string}
 */
function getMessageSenderHtml(message)
{
    return `            <div class="direct-chat-primary"><div class="direct-chat-msg right msg_container base_sent" data-message-id="${message.id}">
                        <div class="direct-chat-infos clearfix">
                            <div class="messages msg_sent float-right">
                                <span class="direct-chat-name ">${message.fromUserName }</span>
                                <!-- <time datetime="${message.dateTimeStr}" class="direct-chat-timestamp float-left">${message.fromUserName} • ${message.dateHumanReadable}</time> -->
                            </div>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img avatar" src="images/users/${message.fromUserImage }" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text float-right">
                            ${message.type == 'text' ? message.content : ('<img src='+message.content+'>')}

                        </div>
                        <!-- /.direct-chat-text -->
                      </div></div>`;
    // return `
    //        <div class="row msg_container base_sent" data-message-id="${message.id}">
    //     <div class="col-md-10 col-xs-10">
    //         <div class="messages msg_sent text-right">
    //             <p>${message.content}</p>
    //             <time datetime="${message.dateTimeStr}"> ${message.fromUserName} • ${message.dateHumanReadable} </time>
    //         </div>
    //     </div>
    //     <div class="col-md-2 col-xs-2 avatar">
    //         <img src="` + base_url +  '/images/user-avatar.png' + `" width="50" height="50" class="img-responsive">
    //     </div>
    // </div>
    // `;
}

/**
 * getMessageReceiverHtml
 *
 * this is the message template for the receiver
 *
 * @param message
 * @returns {string}
 */
function getMessageReceiverHtml(message)
{
          return `          <div class="direct-chat-primary">  <div class="direct-chat-msg left msg_container base_receive" data-message-id="${message.id}">
                        <div class="direct-chat-infos clearfix">
                            <div class="messages msg_receive float-left">
                                <span class="direct-chat-name ">${message.fromUserName }</span>
                                <!-- <time datetime="${message.dateTimeStr}" class="direct-chat-timestamp float-left">${message.fromUserName} • ${message.dateHumanReadable}</time> -->
                            </div>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img avatar" src="images/users/${message.fromUserImage }" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text float-left">
                          ${message.type == 'text' ? message.content : ('<img src='+message.content+'>')}
                        </div>
                        <!-- /.direct-chat-text -->
                      </div></div>`;
    // return `
    //        <div class="row msg_container base_receive" data-message-id="${message.id}">
    //        <div class="col-md-2 col-xs-2 avatar">
    //          <img src="` + base_url +  '/images/user-avatar.png' + `" width="50" height="50" class="img-responsive">
    //        </div>
    //     <div class="col-md-10 col-xs-10">
    //         <div class="messages msg_receive text-left">
    //             <p>${message.content}</p>
    //             <time datetime="${message.dateTimeStr}"> ${message.fromUserName}  • ${message.dateHumanReadable} </time>
    //         </div>
    //     </div>
    // </div>
    // `;
}


/**
 * cloneChatBox
 *
 * this helper function make a copy of the html chat box depending on receiver user
 * then append it to 'chat-overlay' div
 *
 * @param user_id
 * @param username
 * @param callback
 */
function cloneChatBox(user_id,group_id, username, callback)
{
    if($("#chat_box_" + user_id).length == 0) {

        let cloned = $("#chat_box").clone(true);

        // change cloned box id
        cloned.attr("id", "chat_box_" + user_id);

        cloned.attr("data-group_id", group_id);

        cloned.find(".chat-user").text(username);

        cloned.find(".btn-chat, .chatFileInput").attr("data-to-user", user_id);

        cloned.find(".panel-title").find("svg").attr("data-userid", user_id);

        cloned.find("#to_user_id").val(user_id);

        $("#chat-overlay").append(cloned);
    }

    callback();
}

/**
 * loadLatestMessages
 *
 * this function called on load to fetch the latest messages
 *
 * @param container
 * @param user_id
 */
function loadLatestMessages(container, user_id, group_id)
{
    let chat_area = container.find(".chat-area");

    chat_area.html("");

    $.ajax({
        url: base_url + "/load-latest-messages",
        data: {user_id: user_id,group_id: group_id, _token:token},
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            if(chat_area.find(".loader").length  == 0) {
                chat_area.html(loaderHtml());
            }
        },
        success: function (response) {
            if(response.state == 1) {
                if(response.messages.length== 0){
                    $('.chat-area').html("<div class='col-md-12 text-center no_messages'><img class='w-100' src = '"+base_url+"/images/no-chats.png'></div>");
                }
                response.messages.map(function (val, index) {
                    $(val).appendTo(chat_area);
                });
                chat_area.perfectScrollbar({
                    useBothWheelAxes: false,
                    suppressScrollX: true
                });
               // chat_area.scrollTop(chat_area.prop( "scrollHeight" ) );
               // chat_area.perfectScrollbar('update');
                
            }
        },
        complete: function () {
            chat_area.find(".loader").remove();
            // console.log(chat_area.find(".direct-chat-primary").last().offset() );
            // console.log(chat_area.offset().top );
            var scrolled_height = chat_area.find(".direct-chat-primary").last().offset().top  ;
            // console.log(scrolled_height );
            // chat_area.scrollTop( scrolled_height);
            // chat_area.scrollTop(chat_area.get(0).scrollHeight);
            // chat_area.find(".direct-chat-primary").last().append('NIKITA');
        }
    });
}

/**
 * send
 *
 * this function is the main function of chat as it send the message
 *
 * @param to_user
 * @param message
 */
function send(to_user, message,group_id='')
{
    let chat_box = $("#chat_box_" + to_user);
    let chat_area = chat_box.find(".chat-area");

    var data = new FormData();

    data.append('to_user',to_user);
    data.append('group_id',group_id);
    data.append('_token',token);

    // {to_user: to_user,group_id: group_id, _token:token};

    var files = chat_box.find('.chatFileInput')[0].files;
    
    // Check file selected or not
    if(files.length > 0 ){
       data.append('file',files[0]);
       data.append('type','image');
    }else{
       data.append('message',message);
    }

    $.ajax({
        url: base_url + "/send",
        data: data,
        method: "POST",
        contentType: false,
        processData: false,
        beforeSend: function () {
            if(chat_area.find(".loader").length  == 0) {
                chat_area.append(loaderHtml());
            }
        },
        success: function (response) {
        },
        complete: function () {
            chat_area.find(".loader").remove();
            chat_box.find(".btn-chat").prop("disabled", true);
            chat_box.find(".chat_input").val("");
            chat_box.find(".chatFileInput").val("");
            chat_area.animate({scrollTop: chat_area.get(0).scrollHeight}, 800, 'swing');
            // chat_area.animate({scrollTop: chat_area.offset().top + chat_area.outerHeight(true)}, 800, 'swing');
            // chat_area.scrollTop(chat_area.get(0).scrollHeight)
        }
    });
}

/**
 * This function called by the send event triggered from pusher to display the message
 *
 * @param message
 */
function displayMessage(message)
{
    if($('.no_messages').length >0){
        $('.no_messages').remove();
    }
    let alert_sound = document.getElementById("chat-alert-sound");

    if($("#current_user").val() == message.from_user) {

        var otherUser = message.to_user;

        let messageLine = getMessageSenderHtml(message);

        $("#chat_box_" + message.to_user).find(".chat-area").append(messageLine);

    } else if($("#current_user").val() == message.to_user) {

        var otherUser = message.from_user;

        alert_sound.play();

        // for the receiver user check if the chat box is already opened otherwise open it
        cloneChatBox(message.from_user,message.group_id, message.fromUserName, function () {

            let chatBox = $("#chat_box_" + message.from_user);

            if(!chatBox.hasClass("chat-opened")) {

                chatBox.addClass("chat-opened").slideDown("fast");

                loadLatestMessages(chatBox, message.from_user,message.group_id);

                // chatBox.find(".chat-area").animate({scrollTop: chatBox.find(".chat-area").offset().top + chatBox.find(".chat-area").outerHeight(true)}, 800, 'swing');
                setTimeout(function(){ 
                    console.log(chatBox.find(".chat-area").get(0).scrollHeight);
                    chatBox.find(".chat-area").animate({scrollTop: chatBox.find(".chat-area").get(0).scrollHeight}, 800, 'swing');
                }, 500);
            } else {

                let messageLine = getMessageReceiverHtml(message);

                // append the message for the receiver user
                $("#chat_box_" + message.from_user).find(".chat-area").append(messageLine);
            }
        });
    }
    if($("#current_user").val() == message.from_user || $("#current_user").val() == message.to_user){
        let newchatli = '';
        let username = $('#user_li_'+otherUser).find('a').data('user');
        let imagesrc = $('#user_li_'+otherUser).find('.contacts-list-img').attr('src');
        if($('#user_li_'+otherUser).hasClass('new_chats') && (!($('#user_li_'+otherUser).hasClass('recent_chats')))){
            $('.norecents').remove();
        }
        $('#user_li_'+otherUser).remove();
        $('.recent_chats_head').find('.recent_chats').find('.contacts-list-date').each(function(){
            if($(this).text() == 'Just Now'){
                $(this).text(($(this).data('date')));
            }
        });
        var date = changedate(message.timestamp,'d/m/y');
        $('.recent_chats_head').prepend('<li id="user_li_'+otherUser+'" class="recent_chats"><a href="javascript:void(0);" class="chat-toggle" data-group_id="'+ message.group_id +'"  data-id="'+otherUser+'" data-user="'+username+'"><img class="contacts-list-img" src="'+imagesrc+'" alt="User Avatar"><div class="contacts-list-info"><span class="contacts-list-name">'+username+'<small class="contacts-list-date float-right" data-date="'+ date +'">Just Now</small></span> <span class="contacts-list-msg">'+(message.type == 'image' ? ('<i class="fa fa-image"></i> &nbsp;&nbsp;Image') : (message.content))+'</span></div></a></li>');
        if($('#chat_box_'+otherUser).length != 0){
            $('#chat_box_'+otherUser).attr('data-group_id',message.group_id);
        }
    }
}